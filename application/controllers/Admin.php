<?php
class Admin extends CI_Controller{
    public function __construct()
    {
      parent::__construct();
      $this->load->model('admin_model');
    }


    public function index(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }
        
        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['total_users'] = $this->admin_model->get_total_users();
        $data['total_jobs'] = $this->admin_model->get_total_jobs();
        $data['total_company'] = $this->admin_model->get_total_company();
        $data['total_blog'] = $this->admin_model->get_total_blog();

               
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;


        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/index.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function resume($userid)
	{
	    if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }
        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);

		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
	    $data['admin'] = $admin;


		$data['educations'] = $this->user_model->get_education($userid);
		$data['projects'] = $this->user_model->get_projects($userid); 
		$data['experiences'] = $this->user_model->get_experiences($userid); 

		//$data = [];
		//load the view and saved it into $html variable
		// $this->load->view('templates/resume/resume.php');
       	//$this->load->view('templates/resume/resume-temp-1.php', $data, true);


		// 		echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// exit;
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->SetDisplayMode('fullwidth');
		
		$this->load->view('templates/resume/resume1.php' , $data);
        
		// Get output html
		$html = $this->output->get_output();

	
        //$html = $this->load->view('templates/resume/resume.php',[],true);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
	}


    public function login(){

        if($this->session->admindata('alogged_in')){
            redirect('admin/');
        }

        $data['title'] = 'Sign In';
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() === FALSE){
            $this->load->view('templates/admin/login.php', $data);
        }else{
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            //$user_id = $this->user_model->login($username, $password);
            $admin_id = $this->admin_model->login($username, $password);

            if($admin_id){
                $admin_data = array(
                    'admin_id' => $admin_id,
                    'username' => $username,
                    'alogged_in' => true
                );
                $this->session->set_admindata($admin_data);
				
                redirect('admin/');
			} 
	
            else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('admin/login');
            }	

        }

    }

    public function logout(){

        $this->session->unset_userdata('alogged_in');
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('username');
		
        $this->session->set_flashdata('user_loggedout', 'You are now logged out');
        redirect('admin/login');
    }
    
    function do_upload() {
		
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
			);
	
		$this->load->library('upload', $config);

		if($this->upload->do_upload('userfile'))
		{
			$imgdata = array('upload_data' => $this->upload->data());
	
			$imgname = $imgdata['upload_data']['file_name'];
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

        return $imgname;
    }


    public function users(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['users'] = $this->admin_model->get_users(); 
        $data['categories'] = $this->jobs_model->getcategory(); 
        $data['countries'] = $this->jobs_model->getcountry();
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/users.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    

    public function settings(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $data['settings'] = $this->admin_model->get_settings();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/settings.php', $data);
        $this->load->view('templates/admin/footer.php');
    
    

    }

    public function savesettings(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
    
        $this->admin_model->savesettings();

        
        redirect('admin/settings');
    

    }

    public function profile(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['users'] = $this->admin_model->get_users(); 
        $data['categories'] = $this->jobs_model->getcategory(); 
        $data['countries'] = $this->jobs_model->getcountry();
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/profile.php', $data);
        $this->load->view('templates/admin/footer.php');

    }



    public function companies(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['users'] = $this->admin_model->get_companies(); 
        $data['categories'] = $this->jobs_model->getcategory(); 
        $data['countries'] = $this->jobs_model->getcountry();
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/companies.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function ajax_edit_usermodal($userid){
        $currUser = $this->user_model->get_userinfo($userid);
        $data['user'] = $currUser;

        $this->load->view('templates/ajax/edituser.php', $data);
    }

    public function adduser()
    {

        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }
		$data['title'] = 'Add User';
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('fname', 'Father Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        //$this->form_validation->set_rules('country', 'Country', 'required');
        //$this->form_validation->set_rules('city', 'City', 'required');

		if($this->form_validation->run() === FALSE)
		{
    
            $this->session->set_flashdata('user_empty', 'Fields Empty.');
			redirect('admin/users');
            
        }
        else
		{
			
            $config = array(
                'upload_path' => "assets/uploads/",
                'allowed_types' => "jpg|png|jpeg",
                'overwrite' => false,
                'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
                );
        
            $this->load->library('upload', $config);
            if($this->upload->do_upload('userfile'))
            {
                $imgdata = array('upload_data' => $this->upload->data());
        
                $imgname = $imgdata['upload_data']['file_name'];
                
                $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                //$enc_password = md5($this->input->post('password'));
                
                $this->admin_model->add_user($imgname, $enc_password);

                $this->session->set_flashdata('user_added', 'User Added Successfully.');
                redirect('admin/users');
                //$this->load->view('upload_success',$data);
            }
            else
            {
                $error = array('error' => $this->upload->display_errors());
                echo '<pre>';
                print_r($error);
                echo '<pre>';
                exit;
            }

        }
        
    }


    public function category(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $data['categories'] = $this->admin_model->get_categories();

        $this->form_validation->set_rules('catname', 'Name', 'required');
        $this->form_validation->set_rules('catdesc', 'Name', 'required');
        $this->form_validation->set_rules('catkeyword', 'Email', 'required');

        if($this->form_validation->run() === FALSE)
		{
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/category.php', $data);
            $this->load->view('templates/admin/footer.php');
        }
        else
		{
            $this->admin_model->add_category();
            redirect('admin/category');
        }
    

    }

    public function delcategory($catid)
	{
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

		$this->admin_model->delcategory($catid);

			
		redirect('admin/category');
    }


    public function delorder($orderid)
	{
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

		$this->admin_model->delorder($orderid);

		redirect('admin/allorders');
    }

    public function updateuser()
    {

        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }
		$data['title'] = 'Edit User Account';
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('fname', 'Father Name', 'required');
		$this->form_validation->set_rules('phone', 'Phone No', 'required');

		if($this->form_validation->run() === FALSE)
		{
            redirect('admin/users/1');
        }
        else
		{
			if(!file_exists($_FILES["userfile"]["tmp_name"])){
                $user_id = $this->input->post('userid');
				$currUser = $this->user_model->get_userinfo($user_id);
			
				$imgname = $currUser["picture"];
				$this->admin_model->update_user($user_id ,$imgname);
			}
			else{
                $user_id = $this->input->post('userid');
                $imgname = $this->do_upload();
				$this->admin_model->update_user($user_id ,$imgname);
				}
            $this->session->set_flashdata('user_updated', 'User account is now updated.');
            redirect('admin/users');
            
        }
        
    }

    public function deluser($userid)
	{
        if(!$this->session->admindata('admin_id'))
		{
         
			redirect('admin/login');
        }
        $this->session->set_flashdata('user_deleted', 'User Deleted Successfully.');
		$this->admin_model->del_user($userid);

		
		redirect('admin/users');
    }

 

    
    public function delcompany($companyid)
	{
        if(!$this->session->admindata('admin_id'))
		{
         
			redirect('admin/login');
        }
        $this->session->set_flashdata('user_deleted', 'User Deleted Successfully.');
		$this->admin_model->del_company($companyid);

		
		redirect('admin/companies');
    }

    public function enbcompany($companyid)
	{
        if(!$this->session->admindata('admin_id'))
		{
         
			redirect('admin/login');
        }
        $this->session->set_flashdata('user_deleted', 'User Deleted Successfully.');
		$this->admin_model->enb_company($companyid);

		
		redirect('admin/companies');
    }
	
	
    public function administrators(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['admins'] = $this->admin_model->get_admins(); 


        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/admins.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    
    public function ajax_edit_adminmodal($adminid){

        $admin = $this->admin_model->get_admininfo($adminid);
        $data['admin'] = $admin;

        $this->load->view('templates/ajax/editadmin.php', $data);
    }

    public function addadmin()
    {

        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }
		$data['title'] = 'Add Administrators';
		$this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run() === FALSE)
		{
    
            $this->session->set_flashdata('admin_empty', 'Fields Empty.');
			redirect('admin/administrators');
            
        }
        else
		{

            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            
            $this->admin_model->add_admin($enc_password);

            $this->session->set_flashdata('admin_added', 'Administrators added successfully.');
            redirect('admin/administrators');
            //$this->load->view('upload_success',$data);

        }
        
    }

    public function check_administator($username)
	{
        $this->db->where('username', $username);
        $result = $this->db->get('admin');
        
        if($result->row(0))
        {
            $this->form_validation->set_message('check_username_exists_or_unique', 'This username is already exists');
            return FALSE;
        }
        return TRUE;

	}
	

    public function updateadmin()
    {

        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }
		$data['title'] = 'Edit Administrator Account';
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run() === FALSE)
		{
            redirect('admin/administrators');
        }
        else
		{
            $admin_id = $this->input->post('adminid');
            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			$this->admin_model->update_admin($admin_id ,$enc_password);
            $this->session->set_flashdata('user_updated', 'User account is now updated.');
            redirect('admin/administrators');
            
        }
        
    }

    public function deladmin($adminid)
	{
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/administrators');
        }
        $this->session->set_flashdata('admin_deleted', 'Administrator Deleted Successfully.');
		$this->admin_model->del_admin($adminid);

		
		redirect('admin/administrators');
    }



    //Bloggers

    public function bloggers(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['bloggers'] = $this->admin_model->get_bloggers();
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/bloggers.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function ajax_edit_bloggermodal($bloggerid){

        $blogger = $this->admin_model->get_bloggerinfo($bloggerid);
        $data['blogger'] = $blogger;

        $this->load->view('templates/ajax/editblogger.php', $data);
    }

    public function addblogger()
    {

        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }
		$data['title'] = 'Add Administrators';
		$this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run() === FALSE)
		{
            $this->session->set_flashdata('admin_empty', 'Fields Empty.');
			redirect('admin/bloggers'); 
        }
        else
		{
            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            
            $this->admin_model->add_blogger($enc_password);

            $this->session->set_flashdata('blogger_added', 'Bloggers added successfully.');
            redirect('admin/bloggers');
            //$this->load->view('upload_success',$data);

        }
        
    }

    public function updateblogger()
    {

        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }
		$data['title'] = 'Edit Administrator Account';
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run() === FALSE)
		{
            redirect('admin/bloggers');
        }
        else
		{
            $blogger_id = $this->input->post('bloggerid');
            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			$this->admin_model->update_blogger($blogger_id ,$enc_password);
            $this->session->set_flashdata('bloggers_updated', 'Blooggers account is now updated.');
            redirect('admin/bloggers');
            
        }
        
    }

    public function delblogger($bloggerid)
	{
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }
        echo 'asdsad';
        $this->session->set_flashdata('admin_deleted', 'Bloggers Deleted Successfully.');
		$this->admin_model->del_bloggers($bloggerid);

			
		redirect('admin/bloggers');
    }

    public function countries(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['countries'] = $this->admin_model->get_countries();
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/countries.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function ajax_cities($country_name){

        $data['cities'] = $this->admin_model->get_cities($country_name); 
         // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;
        $data['country_name'] = $country_name;

        $this->load->view('templates/ajax/ajaxcities.php', $data);
    }

    public function addcity()
    {

        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }
		$data['title'] = 'Add Cities';
		$this->form_validation->set_rules('geoname_id', 'geoname_id', 'required');
        $this->form_validation->set_rules('country_iso_code', 'country_iso_code', 'required');
        $this->form_validation->set_rules('time_zone', 'time_zone', 'required');
        $this->form_validation->set_rules('city_name', 'city_name', 'required');
        $this->form_validation->set_rules('country_name', 'country_name', 'required');

		if($this->form_validation->run() === FALSE)
		{
            $this->session->set_flashdata('city_empty', 'Fields Empty.');
			redirect('admin/countries'); 
        }
        else
		{
            $this->admin_model->add_city();

            $this->session->set_flashdata('city_added', 'Cities added successfully.');
            redirect('admin/countries');

        }
        
    }

    public function delcity($city_name)
	{
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }
        echo 'asdsad';
        $this->session->set_flashdata('city_deleted', 'Bloggers Deleted Successfully.');
		$this->admin_model->del_city($city_name);

			
		redirect('admin/countries');
    }

    public function postedjobs(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['postedjobs'] = $this->admin_model->get_jobs(); 
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/postedjob.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function approvejobs(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['postedjobs'] = $this->admin_model->get_approvejobs(); 
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/approvejobs.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function pendingjobs(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['postedjobs'] = $this->admin_model->get_pendingjobs(); 
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/pendingjobs.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function appliedjobs(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['applications'] = $this->admin_model->get_appliedjobs(); 
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/appliedjob.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function alljobs(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['alljobs'] = $this->admin_model->get_jobs(); 
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/alljobs.php', $data);
        $this->load->view('templates/admin/footer.php');

    }



    public function allorders(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['allorders'] = $this->admin_model->get_orders(); 
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/allorders.php', $data);
        $this->load->view('templates/admin/footer.php');

    }
    
    public function pendingorders(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['allorders'] = $this->admin_model->get_orders(); 
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/pendingorders.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function invoices(){
        if(!$this->session->admindata('admin_id'))
		{
			redirect('admin/login');
        }

        $admin_id= $this->session->admindata('admin_id');
        $admin = $this->admin_model->get_admininfo($admin_id);
        $data['admin'] = $admin;
        $data['invoices'] = $this->admin_model->get_invoices(); 
        
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/invoices.php', $data);
        $this->load->view('templates/admin/footer.php');

    }
}