<?php
class Blogger extends CI_Controller{
    public function __construct()
    {
      parent::__construct();
      $this->load->model('admin_model');
    }




    public function index(){
        $data['settings'] = $this->jobs_model->get_settings();
        if(!$this->session->bloggerdata('ideal_blogger_id'))
		{
			redirect('blogger/login');
        }
        
        $blogger_id= $this->session->bloggerdata('ideal_blogger_id');

        $data['blog_all'] = $this->user_model->get_blog_all();
               
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;


        
        $this->load->view('templates/blogger/header.php');
        $this->load->view('templates/blogger/navbar.php');
        $this->load->view('templates/blogger/aside.php');
        $this->load->view('templates/blogger/index.php', $data);
        $this->load->view('templates/blogger/footer.php');

    }

	function do_upload() {
		
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "jpg|png|jpeg",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
			);
	
		$this->load->library('upload', $config);

		if($this->upload->do_upload('userfile'))
		{
			$imgdata = array('upload_data' => $this->upload->data());
	
			$imgname = $imgdata['upload_data']['file_name'];
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

        return $imgname;
    }
	

    public function postblog()
    {
        $data['settings'] = $this->jobs_model->get_settings();

        if(!$this->session->bloggerdata('ideal_blogger_id'))
		{
			redirect('blogger/login');
        }

        $blogger_id= $this->session->bloggerdata('ideal_blogger_id');
        $currUser = $this->admin_model->get_bloggerinfo($blogger_id);

        $aurthorname = $currUser['name'];
        $avt = $currUser['picture'];

		    // echo '<pre>';
			// print_r($currUser);
			// echo '<pre>';
			// exit;

        
		$this->form_validation->set_rules('tittle', 'Tittle', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');


		if($this->form_validation->run() === FALSE)
		{
	
            $this->load->view('templates/blogger/header.php');
            $this->load->view('templates/blogger/navbar.php');
            $this->load->view('templates/blogger/aside.php');
            $this->load->view('templates/blogger/postblog.php');
            $this->load->view('templates/blogger/footer.php');
            
        }
        else
		{
            $thumbnail = $this->do_upload();
            $this->admin_model->addpost($aurthorname , $avt , $thumbnail);

            $this->session->set_flashdata('user_updated', 'New Blog Sucessfully Added');
            redirect('blogger/postblog');
            
        }
    }



    public function login(){

        if($this->session->bloggerdata('ideal_blogger_id')){
            redirect('blogger/index');
        }

        $data['title'] = 'Sign In';
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() === FALSE){
            $this->load->view('templates/blogger/login.php', $data);
        }else{
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            //$user_id = $this->user_model->login($username, $password);
            $blogger_id = $this->admin_model->bloggerlogin($username, $password);

            if($blogger_id){
                $blogger_data = array(
                    'ideal_blogger_id' => $blogger_id,
                    'ideal_blogger_username' => $username
                );
                $this->session->set_bloggerdata($blogger_data);
				
                redirect('blogger/');
			} 
	
            else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('blogger/login');
            }	

        }

    }
	public function delblog($blogid)
	{
		if(!$this->session->userdata('ideal_blogger_id')){
            redirect('blogger/login');
        }
		
		$this->admin_model->delblog($blogid);

		
		redirect('blogger');
	}


    public function logout(){

        $this->session->unset_userdata('ideal_blogger_id');
        $this->session->unset_userdata('ideal_blogger_username');
		
        $this->session->set_flashdata('user_loggedout', 'You are now logged out');
        redirect('blogger/login');
    }




}