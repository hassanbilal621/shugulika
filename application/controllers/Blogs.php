<?php
class Blogs extends CI_Controller{

    public function index()
    {
			$data['settings'] = $this->jobs_model->get_settings();
		
		$data['blog_all'] = $this->user_model->get_blog_all();
		$data['categories'] = $this->jobs_model->getcategory(); 

		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('templates/home/header.php');
			$this->load->view('templates/auth/navbar.php', $data);
			$this->load->view('templates/blog/blog.php', $data);
			$this->load->view('templates/home/footer.php');
		}
		else{
		$userid= $this->session->userdata('user_id');
		$currUser = $this->user_model->get_userinfo($userid);
		

		$data['user'] = $currUser;
		$data['title'] = "User Account";

				$this->load->view('templates/home/header.php');
				$this->load->view('templates/home/navbar.php', $data);
        $this->load->view('templates/blog/blog.php', $data);
        $this->load->view('templates/home/footer.php');
		}
	}

	public function test()
	{	
		echo date('Y-m-d H:i:s');
		// $something = $this->input->post('comment');
		// $something1 = $this->input->post('user');
		// echo $something1;

		//echo "Values  : " . post('comment') . post('blogid') . post('user');
		exit;


	}


	public function view($blogid)
    {
			$data['settings'] = $this->jobs_model->get_settings();
		$data['title'] = 'Blog #'.$blogid;
        
		$blogs = $this->user_model->get_blogs($blogid);
		$blogcomments = $this->user_model->get_blog_comments($blogid);
		$data['categories'] = $this->jobs_model->getcategory(); 
		$data['blogs'] = $blogs;
		$data['blogcomments'] = $blogcomments;
	

		// echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// die;

		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('templates/home/header.php');
			$this->load->view('templates/auth/navbar.php', $data);
			$this->load->view('templates/blog/singleblogs', $data);
			$this->load->view('templates/home/footer.php');
		}
		else{
			$userid= $this->session->userdata('user_id');
			$currUser = $this->user_model->get_userinfo($userid);

			$data['user'] = $currUser;
			$data['title'] = "User Account";
			$this->form_validation->set_rules('comment', 'Comment', 'required');
			$this->form_validation->set_rules('blogid', 'Blogid', 'required');
			$this->form_validation->set_rules('user', 'User', 'required');

			if($this->form_validation->run() === FALSE){
				
				$this->load->view('templates/home/header.php');
				$this->load->view('templates/home/navbar.php', $data);
				$this->load->view('templates/blog/singleblogs.php', $data);
				$this->load->view('templates/home/footer.php');
			}
			else{
			
				$this->user_model->add_blog_comments($userid);

				redirect('blogs/view/'. $this->input->post('blogid'));
				
			}
		}
    }


}

?>