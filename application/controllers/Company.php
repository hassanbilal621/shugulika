<?php
class Company extends CI_Controller{

	public function __construct()
    {
      parent::__construct();
      $this->load->model('Employee_model');
    }
	
	public function index()
	{
		$data['settings'] = $this->jobs_model->get_settings();
		if(!$this->session->companyuserdata('company_user_id'))
		{
			redirect('users/login');
		}

		$company_user_id = $this->session->companyuserdata('company_user_id');
		$companydata = $this->Employee_model->get_companyinfo($company_user_id);
		// print_r($companydata);
		// exit;
		$data['activejob'] = $this->Employee_model->get_activejob($company_user_id);
		$data['expiredjob'] = $this->Employee_model->get_expiredjob($company_user_id);

	
		$data['company'] = $companydata;
		$data['title'] = "Company Account";

		$this->load->view('templates/company/header.php');
		$this->load->view('templates/company/navbar.php', $data);
		$this->load->view('templates/company/home.php', $data);
        $this->load->view('templates/home/footer.php');
	}

	public function edit()
    {
		$data['settings'] = $this->jobs_model->get_settings();
        if(!$this->session->companyuserdata('company_user_id'))
		{
			redirect('users/login');
		}
		$company_user_id = $this->session->companyuserdata('company_user_id');
		$companydata = $this->Employee_model->get_companyinfo($company_user_id);
	
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('company_desc', 'Description', 'required');
		$this->form_validation->set_rules('companyname', 'Company Name', 'required');
		$this->form_validation->set_rules('company_title', 'Company Type', 'required');


		if($this->form_validation->run() === FALSE)
		{

			redirect('company/');
            
        }
        else
		{

			if(!file_exists($_FILES["userfile"]["tmp_name"])){

		
				$imgname = $companydata["logo"];
				$this->Employee_model->update($company_user_id, $imgname);
			}
			else{
				$imgname = $this->do_upload();
            	$this->Employee_model->update($company_user_id, $imgname);
				}
	

            $this->session->set_flashdata('user_updated', 'Your account is now updated.');
            redirect('company/');
            
        }
        
    }


	public function userpdf($userid)
	{
		$data['settings'] = $this->jobs_model->get_settings();
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		//$data = [];
		//load the view and saved it into $html variable
		// $this->load->view('templates/resume/resume.php');
       	//$this->load->view('templates/resume/resume-temp-1.php', $data, true);

		$mpdf = new \Mpdf\Mpdf();


		$mpdf->SetDisplayMode('fullwidth');
        $html = $this->load->view('templates/resume/resume.php',[],true);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
	}

			
	public function view($companyid)
	{	
		$data['settings'] = $this->jobs_model->get_settings();
		$companydata = $this->Employee_model->get_companyinfo($companyid);
		// print_r($companydata);
		// exit;

		$data['company'] = $companydata;
		$data['title'] = "Company Account";
		if(!$this->session->userdata('user_id'))
		{

			$this->load->view('templates/home/header.php');
			$this->load->view('templates/auth/navbar.php', $data);
			$this->load->view('templates/home/companyview.php', $data);
			$this->load->view('templates/index/footer.php');
			
		}
		$userid= $this->session->userdata('user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$this->load->view('templates/home/header.php');
		$this->load->view('templates/home/navbar.php', $data);
		$this->load->view('templates/home/companyview.php', $data);
        $this->load->view('templates/home/footer.php');
	}

	function do_upload() {
		
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
			);
	
		$this->load->library('upload', $config);

		if($this->upload->do_upload('userfile'))
		{
			$imgdata = array('upload_data' => $this->upload->data());
	
			$imgname = $imgdata['upload_data']['file_name'];
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

        return $imgname;
    }

	public function postjob()
	{
		$data['settings'] = $this->jobs_model->get_settings();
		if(!$this->session->companyuserdata('company_user_id'))
		{
			redirect('users/login');
		}

		$company_user_id = $this->session->companyuserdata('company_user_id');
		$companydata = $this->Employee_model->get_companyinfo($company_user_id);
		$data['categories'] = $this->jobs_model->getcategory(); 
		$data['countries'] = $this->jobs_model->getcountry();
		
		$data['company'] = $companydata;

		$this->form_validation->set_rules('jobtitile', 'Jobtitle', 'required');
		
		$this->form_validation->set_rules('country', 'Country', 'required');
		
		$this->form_validation->set_rules('city', 'City', 'required');
		
		$this->form_validation->set_rules('category', 'Category', 'required');
		
		$this->form_validation->set_rules('startdate', 'Start Daate', 'required');
		
		$this->form_validation->set_rules('enddate', 'End Date', 'required');

		$this->form_validation->set_rules('jobdesc', 'Job Description', 'required');

		
		$this->form_validation->set_rules('minqualification', 'Minimum qualifications', 'required');

		
		$this->form_validation->set_rules('howtoapply', 'How To Apply', 'required');
				
		$this->form_validation->set_rules('salary', 'Salary', 'required');
		$this->form_validation->set_rules('keyword', 'KeyWord', 'required');
		$this->form_validation->set_rules('jobaddress', 'Job Address', 'required');

		//$this->form_validation->set_rules('phone', 'Phone', 'required|regex_match[/^\d{3}-\d{3}-\d{4}$/]');
		// $this->form_validation->set_rules('pcode', 'Postal Code', 'required|regex_match[/^[A-Z][0-9][A-Z]\s[0-9][A-Z][0-9]$/]');
        // $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]|is_unique[company.email]');
        //$this->form_validation->set_rules('password', 'Password', 'required');
		//$this->form_validation->set_rules('password2', 'Confirm Password', 'required|matches[password]');
	
		
        if($this->form_validation->run() === FALSE){

			$this->load->view('templates/company/header.php');
			$this->load->view('templates/company/navbar.php', $data);
			$this->load->view('templates/company/addjob.php', $data);
			$this->load->view('templates/home/footer.php');
        }
        else{
			$imgname = $this->do_upload();
			$this->jobs_model->postjob($company_user_id ,$imgname);
			redirect('company/activejobs');
		}

	}


	public function activejobs(){
		$data['settings'] = $this->jobs_model->get_settings();
        $config['base_url'] = base_url().'jobs/index';
        
		if(!$this->session->companyuserdata('company_user_id'))
		{
			redirect('users/login');
		}

		$company_user_id = $this->session->companyuserdata('company_user_id');
		$companydata = $this->Employee_model->get_companyinfo($company_user_id);

		$data['company'] = $companydata;
        $data['activejobs'] = $this->jobs_model->get_activejobs($company_user_id);


        // echo '<pre>';
        // print_r($data);
        // echo 'pre';
        // die;
        $data['categories'] = $this->jobs_model->getcategory(); 
        $data['countries'] = $this->jobs_model->getcountry();

		$this->load->view('templates/company/header.php');
		$this->load->view('templates/company/navbar.php', $data);
        $this->load->view('templates/company/activejobs.php', $data);
        $this->load->view('templates/home/footer.php');
	}


	
	public function product(){
		$data['settings'] = $this->jobs_model->get_settings();
        $config['base_url'] = base_url().'jobs/index';
        
		if(!$this->session->companyuserdata('company_user_id'))
		{
			redirect('users/login');
		}

		$company_user_id = $this->session->companyuserdata('company_user_id');
		$companydata = $this->Employee_model->get_companyinfo($company_user_id);

		$data['company'] = $companydata;
        $data['activejobs'] = $this->jobs_model->get_activejobs($company_user_id);


        // echo '<pre>';
        // print_r($data);
        // echo 'pre';
        // die;
        $data['categories'] = $this->jobs_model->getcategory(); 
        $data['countries'] = $this->jobs_model->getcountry();

		$this->load->view('templates/company/header.php');
		$this->load->view('templates/company/navbar.php', $data);
        $this->load->view('templates/company/plans.php', $data);
        $this->load->view('templates/home/footer.php');
	}


	
	public function inbox(){
		$data['settings'] = $this->jobs_model->get_settings();
		if(empty($_GET['chatid'])){
			$chatid = 1;

		}else{
			$chatid = $_GET['chatid'];
		}

		$config['base_url'] = base_url().'jobs/index';
        
		if(!$this->session->companyuserdata('company_user_id'))
		{
			redirect('users/login');
		}

		$company_user_id = $this->session->companyuserdata('company_user_id');
		$companydata = $this->Employee_model->get_companyinfo($company_user_id);

		$data['company'] = $companydata;

		$data['chats'] = $this->Employee_model->get_chats($company_user_id);

		
 		// echo '<pre>';
        // print_r($data);
        // echo 'pre';
        // die;
		$data['msgs'] = $this->Employee_model->get_msgs($chatid);


		$this->load->view('templates/company/header.php');
		$this->load->view('templates/company/navbar.php', $data);
		$this->load->view('templates/company/msg.php', $data);
        $this->load->view('templates/home/footer.php');


	}



	public function createchat($userid)
    {
		$data['settings'] = $this->jobs_model->get_settings();
		if(!$this->session->companyuserdata('company_user_id'))
		{
			redirect('users/login');
		}
		
		$company_user_id = $this->session->companyuserdata('company_user_id');
		$companydata = $this->Employee_model->get_companyinfo($company_user_id);
		$chats = $this->Employee_model->get_chats($company_user_id);


		if(!$chats)
		{
			redirect('users/login');
            
        }
        else
		{

            $this->Employee_model->createchat($userid , $company_user_id);

            $this->session->set_flashdata('msg_added', 'Your account is now updated.');
            redirect('company/inbox?chatid='.$chatid."#newmsg");
            
        }
        
	}



	public function closejob($jobid)
    {

		if(!$this->session->companyuserdata('company_user_id'))
		{
			redirect('users/login');
		}
		

		$this->Employee_model->closejob($jobid);

		$this->session->set_flashdata('msg_added', 'Your account is now updated.');
		redirect('company/expiredjobs');
		
    
        
	}

	

	public function addmsg()
    {
		$data['settings'] = $this->jobs_model->get_settings();
		if(!$this->session->companyuserdata('company_user_id'))
		{
			redirect('users/login');
		}
		
		$company_user_id = $this->session->companyuserdata('company_user_id');
		$companydata = $this->Employee_model->get_companyinfo($company_user_id);

		$data['title'] = 'Message | Shugulika';
		$this->form_validation->set_rules('text', 'Message', 'required');
		$this->form_validation->set_rules('chatid', 'Chat ID', 'required');
		

		if($this->form_validation->run() === FALSE)
		{
 
			if(empty($companydata['company']))
			{
				redirect('company/inbox');
			}
			else
			{
				$this->session->set_flashdata('user_fieldempty', 'Empty Fields');
				redirect('company/inbox');
			}
            
        }
        else
		{
			$chatid = $this->input->post('chatid');
            $this->Employee_model->sendmsg();

            $this->session->set_flashdata('msg_added', 'Your account is now updated.');
            redirect('company/inbox?chatid='.$chatid."#newmsg");
            
        }
        
	}




	

	public function applications($jobid){
		$data['settings'] = $this->jobs_model->get_settings();
        $config['base_url'] = base_url().'jobs/index';
        
		if(!$this->session->companyuserdata('company_user_id'))
		{
			redirect('users/login');
		}

		$company_user_id = $this->session->companyuserdata('company_user_id');
		$companydata = $this->Employee_model->get_companyinfo($company_user_id);

		$data['company'] = $companydata;
		$data['applications'] = $this->jobs_model->get_applications($jobid);
		$jobinfo = $this->jobs_model->get_singlejob($jobid);
		$data['jobinfo'] = $jobinfo;

		


        // echo '<pre>';
        // print_r($data);
        // echo 'pre';
        // die;
        $data['categories'] = $this->jobs_model->getcategory(); 
        $data['countries'] = $this->jobs_model->getcountry();

		$this->load->view('templates/company/header.php');
		$this->load->view('templates/company/navbar.php', $data);
        $this->load->view('templates/company/applications.php', $data);
        $this->load->view('templates/home/footer.php');
	}
    
    
	
	
	public function expiredjobs(){
		$data['settings'] = $this->jobs_model->get_settings();
        $config['base_url'] = base_url().'jobs/index';
        
		if(!$this->session->companyuserdata('company_user_id'))
		{
			redirect('users/login');
		}

		$company_user_id = $this->session->companyuserdata('company_user_id');
		$companydata = $this->Employee_model->get_companyinfo($company_user_id);

		$data['company'] = $companydata;
        $data['expjobs'] = $this->jobs_model->get_expiredjobs($company_user_id);


        // echo '<pre>';
        // print_r($data);
        // echo 'pre';
        // die;
        $data['categories'] = $this->jobs_model->getcategory(); 
        $data['countries'] = $this->jobs_model->getcountry();

		$this->load->view('templates/company/header.php');
		$this->load->view('templates/company/navbar.php', $data);
        $this->load->view('templates/company/expiredjobs.php', $data);
        $this->load->view('templates/home/footer.php');
	}
    
    
	
	
	
    
    



	/*
     * Function: register
     * Purpose: This controller is responsible for registering a new user
     *          URL is /register
     * Params: none
     * Return: none
     */
    public function register(){
		$data['settings'] = $this->jobs_model->get_settings();
		if($this->session->userdata('logged_in')){
            redirect('users/');
        }
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('addr', 'Address', 'required');
		$this->form_validation->set_rules('prov', 'Province', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required|regex_match[/^\d{3}-\d{3}-\d{4}$/]');
		$this->form_validation->set_rules('fax', 'Fax', 'regex_match[/^\d{3}-\d{3}-\d{4}$/]');
		$this->form_validation->set_rules('pcode', 'Postal Code', 'required|regex_match[/^[A-Z][0-9][A-Z]\s[0-9][A-Z][0-9]$/]');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password2', 'Confirm Password', 'required|matches[password]');
		$data['countries'] = $this->jobs_model->getcountry();
		
        if($this->form_validation->run() === FALSE){
            
            $this->load->view('templates/auth/header');
            $this->load->view('templates/auth/register', $data);
            $this->load->view('templates/auth/footer');
        }
        else{
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            //$enc_password = md5($this->input->post('password'));
            $this->user_model->register($enc_password);

            $this->session->set_flashdata('user_registered', 'You are now registered and can log in');
            redirect('users/login');
            
        }
        
         
    }


    /*
     * Function: logout
     * Purpose: This controller is responsible for logging out an existing 
				user by removing session data and cookie
     *          URL is /logout 
     * Params: none
     * Return: none
     */
    public function logout(){

        $this->session->unset_userdata('company_user_id');
        $this->session->unset_userdata('company_user_email');
        $this->session->unset_userdata('company_logged_in');
		
		delete_cookie('jobboard_company_id');

        $this->session->set_flashdata('user_loggedout', 'You are now logged out');
        redirect('users/login');
    }
    
	/*
     * Function: remove
     * Purpose: This controller is responsible for removing the current logged in user
     *          URL is user/remove
     * Params: none
     * Return: none
     */
	public function remove()
	{
		if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
		
		$this->user_model->remove_user();
		
		$this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');
		
		redirect('jobs');
	}
	
	/*
     * Function: forgot
     * Purpose: This controller is responsible for sending a user an 
				email with a link to reset their forgotten password
     *          URL is user/forgot
     * Params: none
     * Return: none
     */
	public function forgot()
	{
		if($this->session->userdata('logged_in')){
            redirect('jobs');
        }
		$this->form_validation->set_rules('username', 'Username', 'callback_username_exists[' . $this->input->post('username') . ']');
		
        $data['title'] = 'Forgot Password';
        if($this->form_validation->run() === FALSE){
            $this->load->view('templates/header');
            $this->load->view('users/forgot', $data);
            $this->load->view('templates/footer');
        } 
        else {
			$this->session->set_flashdata('email_sent', 'You have been sent an email with a recovery password link.');
			
			$token = rand( 111111, 999999);
			$this->user_model->set_password_reset_token($this->input->post('username'), $token);
			
			//For emailing reset password link
			/*$this->load->library('email');
            
            $this->email->from('abc@abc.com', 'MegzKay');
            $this->email->to($this->input->post('email'),$this->input->post('name'));
            $this->email->subject('Email from ciblog');
            $this->email->message("Please click the following link to recover your email: ".base_url()."reset/".$token);
            
            $this->email->send();

			*/
			
			redirect('users/login');
        }
		
	}
	
	/*
     * Function: username_exists
     * Purpose: This is a callback method used to check if a user's name exists
     *          URL is user/forgot
     * Params: $username: the name of the user
     * Return: True if user exists, false if user doesn't exist
     */
	public function username_exists($username)
	{
		
		$user_exists = $this->user_model->check_user_exists($username);
		if (!$user_exists )
		{
			$this->form_validation->set_message('username_exists', 'That username does not exist');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	/*
     * Function: reset
     * Purpose: This controller is responsible for reseting a user account with a new password
     *          URL is user/reset/TOKEN
     * Params: $token: a unique token that indicates which user needs to update their password
     * Return: none
     */
	public function reset($token)
	{

		$data['title'] = 'Reset';
		$data['token'] = $token;
		
		$user_id = $this->user_model->check_token_exists($token);
		
		if($user_id)
		{
			$this->form_validation->set_rules('password', 'Password', 'required');
			if($this->form_validation->run() === FALSE)
			{
				$this->load->view('templates/header');
				$this->load->view('users/reset', $data);
				$this->load->view('templates/footer');
			} 
			else 
			{
				$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
				$this->user_model->change_password($user_id,$enc_password);
				redirect('users/login');
			}
			
		}
		else
		{
			$this->session->set_flashdata('token_failed', "The link you have clicked has expired. Please click forgot password again.");
		}
		
	}
	
	/*
     * Function: edit
     * Purpose: This is the controller used for the edit users page
     *          URL is /jobs/edit/USER_ID
     * Params:  $user_id: Identifies which specific user
     * Return: none
     */
   
	

	 

	//How to get callbacks to work: https://forum.codeigniter.com/thread-64657.html
	/*
     * Function: checkpwdmatch
     * Purpose: This is a callback method used to check if the password 
				entered by a user matches that user's password in the database
     * Params: $password: password user entered
     * Return: True if password belongs to the user, false otherwise
     */
	public function checkpwdmatch($password)
	{
		
		$this->db->where('id', $this->session->userdata('user_id'));
        $result = $this->db->get('users');
		
        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return true;
			}
			else
			{
				$this->form_validation->set_message('checkpwdmatch', 'Incorrect password');
				return FALSE;
			}
            
			
			
        } 
		
		$this->form_validation->set_message('checkpwdmatch', 'The password field can not be empty');
		return FALSE;
        
	}
	
	/*
     * Function: check_username_exists_or_unique
     * Purpose: This is a callback method used to check if the new 
				username a user has entered exists and is not their own
     * Params: $username: username user entered
     * Return: True if username is their own or is unique, false if username is not unique 
     */
	public function check_username_exists_or_unique($username)
	{

		$user = $this->session->userdata('username');
		if($user != $username)
		{
			$this->db->where('username', $username);
			$result = $this->db->get('users');
			
			if($result->row(0))
			{
				$this->form_validation->set_message('check_username_exists_or_unique', 'This username is taken by someone else');
				return FALSE;
			}
			return TRUE;
		
		}
		return TRUE;
        
	}
	
	
	/*
     * Function: check_email_exists_or_unique
     * Purpose: This is a callback method used to check if the new 
				email a user has entered exists and is not their own
     * Params: $email: username user entered
     * Return: True if email is their own or is unique, false if email is not unique 
     */
	public function check_email_exists_or_unique($email)
	{

		$user = $this->session->userdata('email');
		if($user != $email)
		{
			$this->db->where('email', $email);
			$result = $this->db->get('users');
			
			if($result->row(0))
			{
				$this->form_validation->set_message('check_email_exists_or_unique', 'This email is taken by someone else');
				return FALSE;
			}
			return TRUE;
		
		}
		return TRUE;
        
	}
	
	public function joblist()
	{
		$data['settings'] = $this->jobs_model->get_settings();
		if(!$this->session->userdata('user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";


		$this->load->view('templates/home/header.php');
		$this->load->view('templates/home/navbar.php', $data);
		$this->load->view('templates/jobs/listingjob.php', $data);
		$this->load->view('templates/jobs/joblistingbody.php', $data);
		$this->load->view('templates/home/footer.php');
	}
	


	public function resume()
	{
		if(!$this->session->userdata('user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";


		$this->load->view('templates/resume/resume.php', $data);
   
	}
	
	
}
