<?php
class Pages extends CI_Controller{

    public function __construct()
    {
      parent::__construct();
      $this->load->model('admin_model');

    }

	
	/*
     * Function: view
     * Purpose: This controller is responsible for showing static pages,
     * Params:  $page: optional parameter, default is home. This is the name of the page to view
     * Return: none
     */
    public function view($page='home',$offset=0){

        $data['jobs'] = $this->jobs_model->get_recentjobs(); 
        $data['companies'] = $this->jobs_model->get_topcompany(); 
        $data['settings'] = $this->jobs_model->get_settings();

        if(!empty($data['jobs']))
        {
            $data['nojobs'] = "";
        }
        else
        {
            $data['nojobs'] = "No jobs available";
        }
        if($this->session->userdata('logged_in')){
            redirect('users/');
        }

        if($this->session->userdata('company_user_id')){
            redirect('company/');
        }
        
        if(!file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            show_404();
        }
        $data['title'] = ucfirst($page);
        $data['categories'] = $this->jobs_model->getcategory(); 
        $data['countries'] = $this->jobs_model->getcountry();
        
        $this->load->view('templates/index/header');
        $this->load->view('templates/index/navbar', $data);
        $this->load->view('templates/index/'.$page, $data);
        $this->load->view('templates/home/footer', $data);
    }

    public function getcountry()
	{
		$data['countries'] = $this->jobs_model->getcountry();

		$this->load->view('templates/ajax/getcountry.php', $data);
	}

	public function getcity()
	{
		
		$data['cities'] = $this->jobs_model->getcity();

		$this->load->view('templates/ajax/getcity.php', $data);
	}
   
    public function plans()
    {
        $data['settings'] = $this->jobs_model->get_settings();
        if(!$this->session->userdata('user_id'))
        {
            $this->load->view('templates/home/header.php');
            $this->load->view('templates/auth/navbar.php', $data);
            $this->load->view('templates/services/plans.php');
            $this->load->view('templates/home/footer.php');
        }
        else{
        $userid= $this->session->userdata('user_id');
        $currUser = $this->user_model->get_userinfo($userid);

        $data['user'] = $currUser;
        $data['title'] = "User Account";

        $this->load->view('templates/home/header.php');
        $this->load->view('templates/home/navbar.php');
        $this->load->view('templates/services/plansuser', $data);
        $this->load->view('templates/home/footer.php', $data);
        }

    }
    
    public function services()
    {
        $data['settings'] = $this->jobs_model->get_settings();
        if(!$this->session->userdata('user_id'))
        {
            $this->load->view('templates/home/header.php');
            $this->load->view('templates/auth/navbar.php', $data);
            $this->load->view('templates/services/services.php');
            $this->load->view('templates/home/footer.php', $data);
        }
        else{
        $userid= $this->session->userdata('user_id');
        $currUser = $this->user_model->get_userinfo($userid);

        $data['user'] = $currUser;
        $data['title'] = "User Account";

        $this->load->view('templates/home/header.php');
        $this->load->view('templates/home/navbar.php', $data);
        $this->load->view('templates/services/servicesuser', $data);
        $this->load->view('templates/home/footer.php', $data);
        }

    }
    public function career()
    {
        $data['settings'] = $this->jobs_model->get_settings();
        if(!$this->session->userdata('user_id'))
        {
            $this->load->view('templates/home/header.php');
            $this->load->view('templates/auth/navbar.php', $data);
            $this->load->view('templates/career/career.php');
            $this->load->view('templates/home/footer.php', $data);
        }
        else{
        $userid= $this->session->userdata('user_id');
        $currUser = $this->user_model->get_userinfo($userid);

        $data['user'] = $currUser;
        $data['title'] = "User Account";

        $this->load->view('templates/home/header.php');
        $this->load->view('templates/home/navbar.php', $data);
        $this->load->view('templates/career/career', $data);
        $this->load->view('templates/home/footer.php', $data);
        }

    }


	/*
     * Function: contact
     * Purpose: This controller is responsible for showing the contact page, 
				and processing the message sent 
				URL is /contact
     * Params:  $page: optional parameter, default is home. This is the name of the page to view
     * Return: none
     */
    public function contact()
    {
        $data['settings'] = $this->jobs_model->get_settings();
        $data['title'] = "Contact";
        
        $this->form_validation->set_rules('name','Name', 'required');
        $this->form_validation->set_rules('email','Email',  'required|valid_email');
        $this->form_validation->set_rules('subject', 'Body', 'required');
        $this->form_validation->set_rules('message', 'Body', 'required');
        
        if($this->form_validation->run()==FALSE)
        {

            if(!$this->session->userdata('user_id'))
            {
                $this->load->view('templates/home/header.php');
                $this->load->view('templates/auth/navbar.php', $data);
                $this->load->view('templates/pages/contact', $data);
                $this->load->view('templates/home/footer.php', $data);
            }
            else{
            $userid= $this->session->userdata('user_id');
            $currUser = $this->user_model->get_userinfo($userid);
    
            $data['user'] = $currUser;
            $data['title'] = "User Account";
    
            $this->load->view('templates/home/header.php');
            $this->load->view('templates/home/navbar.php', $data);
            $this->load->view('templates/pages/contact', $data);
            $this->load->view('templates/home/footer.php', $data);
            }
        }
        else
        {
			
            $this->load->library('email');
            
            $this->email->from('info@shugulika.com', 'Support Department');
            $this->email->to('support@shugulika.com', $this->input->post('name'));
            $this->email->subject($this->input->post('subject') . 'By :' . $this->input->post('email') );
            $this->email->message($this->input->post('message'));
            
            $this->email->send();
            
            //$this->email->send(FALSE);
            //$this->email->print_debugger(array('headers'));
        
            $this->session->set_flashdata('msg_sent','Your message has been sent');
            redirect('pages/contact');
        }
        
    }
}
