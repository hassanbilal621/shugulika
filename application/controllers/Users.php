<?php
class Users extends CI_Controller{


	public function __construct()
    {
      parent::__construct();
      $this->load->model('employee_model');
    }
	
	public function index()
	{
		$data['settings'] = $this->jobs_model->get_settings();
		if(!$this->session->userdata('user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";
		$data['educations'] = $this->user_model->get_education($userid);
		$data['projects'] = $this->user_model->get_projects($userid); 
		$data['experiences'] = $this->user_model->get_experiences($userid); 
		$data['categories'] = $this->jobs_model->getcategory(); 
        $data['countries'] = $this->jobs_model->getcountry();

		
		$this->load->view('templates/home/header.php', $data);
		$this->load->view('templates/home/navbar.php', $data);
		$this->load->view('templates/home/home.php', $data);
        $this->load->view('templates/home/footer.php');
	}



		
    public function edit()
    {
		$data['settings'] = $this->jobs_model->get_settings();
		$userid= $this->session->userdata('user_id');

        if(!$this->session->userdata('logged_in') && $userid == $this->session->userdata('user_id')){
            redirect('users/login');
        }
		$data['title'] = 'Edit User Account';
		$this->form_validation->set_rules('name', 'Name', 'required');

		if($this->form_validation->run() === FALSE)
		{
			$data['user'] = $this->user_model->get_userinfo($userid); 
			if(empty($data['user']))
			{
				redirect('users');
			}
			else
			{
				$this->load->view('templates/header');
				$this->load->view('users/edit', $data);
				$this->load->view('templates/footer');
			}
            
        }
        else
		{
			if(!file_exists($_FILES["userfile"]["tmp_name"])){

				$userid= $this->session->userdata('user_id');
				$currUser = $this->user_model->get_userinfo($userid);
			
				$imgname = $currUser["picture"];
				$this->user_model->update($userid ,$imgname);
			}
			else{
				$imgname = $this->do_upload();
				$this->user_model->update($userid ,$imgname);
				}
            $this->session->set_flashdata('user_updated', 'Your account is now updated.');
            redirect('users');
            
        }
        
    }
	


	//Master Function
	// public function do_upload(){
	// 	$config = array(
	// 	'upload_path' => "assets/uploads/",
	// 	'allowed_types' => "gif|jpg|png|jpeg|pdf",
	// 	'overwrite' => TRUE,
	// 	'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
	// 	'max_height' => "768",
	// 	'max_width' => "1024"
	// 	);

	// 	$this->load->library('upload', $config);
	// 	if($this->upload->do_upload())
	// 	{
	// 		$data = array('upload_data' => $this->upload->data());
	// 		echo '<pre>';
	// 		print_r($data);
	// 		echo '<pre>';
	// 		exit;
	// 		//$this->load->view('upload_success',$data);
	// 	}
	// 	else
	// 	{
	// 		$error = array('error' => $this->upload->display_errors());
	// 		echo '<pre>';
	// 		print_r($error);
	// 		echo '<pre>';
	// 	}
	// }
	public function add_document()
    {

		if(!$this->session->userdata('user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('user_id');
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
			);
	
		$this->load->library('upload', $config);

		//$this->upload->do_upload('userfile');


		if($this->upload->do_upload('userfile'))
		{
			$filedata = array('upload_data' => $this->upload->data());
	
			$filename = $filedata['upload_data']['file_name'];
			
			$this->user_model->add_document($userid, $filename);
			$this->session->set_flashdata('user_document_updated', 'Your file sucessfully uploaded.');
			redirect('users');
			
			//$this->load->view('upload_success',$data);
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

	}

	public function delproject($projectid)
	{
		if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
		
		$this->user_model->delproject($projectid);

		redirect('users');
	}
	
	public function delexperience($expid)
	{
		if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
		
		$this->user_model->delexperience($expid);

		
		redirect('users');
	}
	
	public function deleducation($eduid)
	{
		if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
		
		$this->user_model->deleducation($eduid);

		
		redirect('users');
	}


	
	public function addproject($user_id)
    {

        if(!$this->session->userdata('logged_in') && $userid == $this->session->userdata('user_id')){
            redirect('users/login');
        }
		$data['title'] = 'Add Education';
		$this->form_validation->set_rules('tittle', 'Degree', 'required');
		$this->form_validation->set_rules('desc', 'Description', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');


		if($this->form_validation->run() === FALSE)
		{

			redirect('users');
            
        }
        else
		{
			
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
			);
	
		$this->load->library('upload', $config);
		if($this->upload->do_upload('userfile'))
		{
			$imgdata = array('upload_data' => $this->upload->data());
	
			$imgname = $imgdata['upload_data']['file_name'];
			
			$this->user_model->addproject($user_id , $imgname);

			$this->session->set_flashdata('user_updated', 'Your account is now updated.');
			redirect('users');
			//$this->load->view('upload_success',$data);
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}



        }
        
	}

	public function addeducation($user_id)
    {

        if(!$this->session->userdata('logged_in') && $userid == $this->session->userdata('user_id')){
            redirect('users/login');
        }
		$data['title'] = 'Add Education';
		$this->form_validation->set_rules('degree', 'Degree', 'required');
		$this->form_validation->set_rules('desc', 'Description', 'required');
		$this->form_validation->set_rules('startdate', 'Start Date', 'required');
		$this->form_validation->set_rules('enddate', 'End Date', 'required');
		$this->form_validation->set_rules('type', 'Dgree Type', 'required');

		if($this->form_validation->run() === FALSE)
		{

			$data['user'] = $this->user_model->get_userinfo($user_id); 
			if(empty($data['user']))
			{
				show_404();
			}
			else
			{
				$this->session->set_flashdata('user_fieldempty', 'Empty Fields');
				redirect('users');
			}
            
        }
        else
		{
            $this->user_model->addeducation($user_id);

            $this->session->set_flashdata('user_updated', 'Your account is now updated.');
            redirect('users');
            
        }
        
	}

	public function addexperience($user_id)
    {

        if(!$this->session->userdata('logged_in') && $userid == $this->session->userdata('user_id')){
            redirect('users/login');
        }
		$data['title'] = 'Add Experience';
		$this->form_validation->set_rules('tittle', 'Tittle', 'required');
		$this->form_validation->set_rules('company', 'Company Name', 'required');
		$this->form_validation->set_rules('desc', 'Description', 'required');
		$this->form_validation->set_rules('startdate', 'Start Date', 'required');
		$this->form_validation->set_rules('enddate', 'End Date', 'required');


		if($this->form_validation->run() === FALSE)
		{

			$data['user'] = $this->user_model->get_userinfo($user_id); 
			if(empty($data['user']))
			{
				show_404();
			}
			else
			{
				redirect('users');
			}
            
        }
        else
		{
			
            $this->user_model->addexperience($user_id);

            $this->session->set_flashdata('user_updated', 'Your account is now updated.');
            redirect('users');
            
        }
        
	}

	public function messages(){
		$data['settings'] = $this->jobs_model->get_settings();
		if(empty($_GET['chatid'])){
			$chatid = 1;

		}else{
			$chatid = $_GET['chatid'];
		}

		if(!$this->session->userdata('user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$data['chats'] = $this->user_model->get_chats($userid);

		$data['msgs'] = $this->user_model->get_msgs($chatid);


		$this->load->view('templates/home/header.php');
		$this->load->view('templates/home/navbar.php', $data);
		$this->load->view('templates/home/msg.php', $data);
        $this->load->view('templates/home/footer.php');


	}
	
	public function addmsg()
    {

        if(!$this->session->userdata('logged_in') && $userid == $this->session->userdata('user_id')){
            redirect('users/login');
		}
		
		$data['title'] = 'Message | Shugulika';
		$this->form_validation->set_rules('text', 'Message', 'required');
		$this->form_validation->set_rules('chatid', 'Chat ID', 'required');
		

		if($this->form_validation->run() === FALSE)
		{
			$data['user'] = $this->user_model->get_userinfo($user_id); 
			if(empty($data['user']))
			{
				redirect('users/messages');
			}
			else
			{
				$this->session->set_flashdata('user_fieldempty', 'Empty Fields');
				redirect('users/messages');
			}
            
        }
        else
		{
			$chatid = $this->input->post('chatid');
            $this->user_model->sendmsg();

            $this->session->set_flashdata('user_updated', 'Your account is now updated.');
            redirect('users/messages?chatid='.$chatid);
            
        }
        
	}


	public function invoice(){
		$data['settings'] = $this->jobs_model->get_settings();
		if(!$this->session->userdata('user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/home/header.php');
		$this->load->view('templates/home/navbar.php', $data);
		$this->load->view('templates/services/invoice.php', $data);
        $this->load->view('templates/home/footer.php');


	}
	/*
     * Function: register
     * Purpose: This controller is responsible for registering a new user
     *          URL is /register
     * Params: none
     * Return: none
     */
    public function register(){
		if($this->session->userdata('logged_in')){
            redirect('users/');
		}
		$data['settings'] = $this->jobs_model->get_settings();
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('addr', 'Address', 'required');
		$this->form_validation->set_rules('country', 'Province', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		//$this->form_validation->set_rules('phone', 'Phone', 'required|regex_match[/^\d{3}-\d{3}-\d{4}$/]');
		// $this->form_validation->set_rules('pcode', 'Postal Code', 'required|regex_match[/^[A-Z][0-9][A-Z]\s[0-9][A-Z][0-9]$/]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]|is_unique[company.email]');
        //$this->form_validation->set_rules('password', 'Password', 'required');
		//$this->form_validation->set_rules('password2', 'Confirm Password', 'required|matches[password]');
	
		
        if($this->form_validation->run() === FALSE){
			$data['countries'] = $this->jobs_model->getcountry();
			$data['cities'] = $this->jobs_model->getcity();
			
            $this->load->view('templates/auth/header');
            $this->load->view('templates/auth/register', $data);
            $this->load->view('templates/auth/footer');
        }
        else{
		
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            //$enc_password = md5($this->input->post('password'));
			$this->user_model->register($enc_password);
			

			//For emailing reset password link
			$this->load->library('email');
            
            $this->email->from('support@shugulika.com', 'Support Team');
            $this->email->to($this->input->post('email'),$this->input->post('name'));
            $this->email->subject('Registration Confirmation');
            $this->email->message("You are now successfully registered. ");
            
            $this->email->send();

            $this->session->set_flashdata('user_registered', 'You are now successfully registered.');
            redirect('users/login');
            
        }
        
         
	}


    public function registercompany(){
		$data['settings'] = $this->jobs_model->get_settings();
		if($this->session->userdata('logged_in')){
            redirect('users/');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('addr', 'Address', 'required');
		$this->form_validation->set_rules('country', 'Province', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		//$this->form_validation->set_rules('phone', 'Phone', 'required|regex_match[/^\d{3}-\d{3}-\d{4}$/]');
		// $this->form_validation->set_rules('pcode', 'Postal Code', 'required|regex_match[/^[A-Z][0-9][A-Z]\s[0-9][A-Z][0-9]$/]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]|is_unique[company.email]');
        //$this->form_validation->set_rules('password', 'Password', 'required');
		//$this->form_validation->set_rules('password2', 'Confirm Password', 'required|matches[password]');
	
		
        if($this->form_validation->run() === FALSE){
			$data['countries'] = $this->jobs_model->getcountry();
			$data['cities'] = $this->jobs_model->getcity();
			
            $this->load->view('templates/auth/header');
            $this->load->view('templates/auth/register', $data);
            $this->load->view('templates/auth/footer');
        }
        else{
		
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            //$enc_password = md5($this->input->post('password'));
			$this->user_model->registercompany($enc_password);
			

			//For emailing reset password link
			$this->load->library('email');
            
            $this->email->from('support@shugulika.com', 'Support Team');
            $this->email->to($this->input->post('email'),$this->input->post('name'));
            $this->email->subject('Registration Confirmation');
            $this->email->message("You are now successfully registered. ");
            
            $this->email->send();

            $this->session->set_flashdata('user_registered', 'You are now successfully registered.');
            redirect('users/login');
            
        }
        
         
	}



	public function getcountry()
	{
		$data['countries'] = $this->jobs_model->getcountry();

		$this->load->view('templates/ajax/getcountry.php', $data);
	}

	public function getcity()
	{
		
		$data['cities'] = $this->jobs_model->getcity();

		$this->load->view('templates/ajax/getcity.php', $data);
	}
	public function getcitys()
	{
		
		$data['cities'] = $this->jobs_model->getcity();

		$this->load->view('templates/ajax/getcitys.php', $data);
	}

	
	public function getacity()
	{
		
		$data['cities'] = $this->jobs_model->getcity();

		$this->load->view('templates/ajax/getadmincity.php', $data);
	}
	


    public function login(){
		$data['settings'] = $this->jobs_model->get_settings();
		if($this->session->userdata('logged_in')){
            redirect('users/');
		}
		

		if($this->session->userdata('company_user_id')){
            redirect('company/');
        }
		
        $data['title'] = 'Sign In';
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if($this->form_validation->run() === FALSE){
            $this->load->view('templates/auth/header');
            $this->load->view('templates/auth/login', $data);
            $this->load->view('templates/auth/footer');
        } 
        else {
            $username = $this->input->post('username');
			$password = $this->input->post('password');
			
			
			$user_id = $this->user_model->login($username, $password);
			$company_user_id = $this->user_model->company_login($username, $password);

			//$employee_id = $this->employee_model->login($username, $password);
            if($user_id){
                $user_data = array(
                    'user_id' => $user_id,
                    'user_email' => $username,
                    'logged_in' => true
                );
                $this->session->set_userdata($user_data);
				
				
				//set cookie for 1 year
				$cookie = array(
					'name'   => 'jobboard_user_id',
					'value'  => $user_id,
					'expire' => time()+31556926
				);
				$this->input->set_cookie($cookie);

				
                $this->session->set_flashdata('user_loggedin', 'You are now logged in');
                redirect('users/');
			}

			elseif($company_user_id){

				
				$checkaccount = $this->user_model->companydisable($company_user_id);
				
				// echo $checkaccount;

				// die;
				if($checkaccount){
					
					$this->session->set_flashdata('company_disable', 'Your Account has been disabled contact Administrator for more info');
					redirect('users/login');

				}
				


                $company_user_data = array(
                    'company_user_id' => $company_user_id,
                    'company_user_email' => $username,
                    'company_logged_in' => true
				);

                $this->session->set_companyuserdata($company_user_data);
				
				//set cookie for 1 year
				$cookie = array(
					'name'   => 'jobboard_company_id',
					'value'  => $company_user_id,
					'expire' => time()+31556926
				);
				$this->input->set_cookie($cookie);
				

			

			

                $this->session->set_flashdata('company_user_loggedin', 'You are now logged in');
                redirect('company/');
			} 
	
            else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('users/login');
            }		
        }
    }

    /*
     * Function: logout
     * Purpose: This controller is responsible for logging out an existing 
				user by removing session data and cookie
     *          URL is /logout 
     * Params: none
     * Return: none
     */
    public function logout(){

        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');
		
		delete_cookie('jobboard_user_id');

        $this->session->set_flashdata('user_loggedout', 'You are now logged out');
        redirect('users/login');
    }
    
	/*
     * Function: remove
     * Purpose: This controller is responsible for removing the current logged in user
     *          URL is user/remove
     * Params: none
     * Return: none
     */
	public function remove()
	{
		if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
		
		$this->user_model->remove_user();
		
		$this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');
		
		redirect('jobs');
	}
	
	/*
     * Function: forgot
     * Purpose: This controller is responsible for sending a user an 
				email with a link to reset their forgotten password
     *          URL is user/forgot
     * Params: none
     * Return: none
     */

	public function forgot()
	{
		$data['settings'] = $this->jobs_model->get_settings();
		if($this->session->userdata('logged_in')){
            redirect('jobs');
		}


	
		$this->form_validation->set_rules('username', 'Email', 'required|valid_email');
		
        $data['title'] = 'Forgot Password';
        if($this->form_validation->run() === FALSE){
			$this->load->view('templates/auth/header');
            $this->load->view('templates/auth/forgot', $data);
            $this->load->view('templates/auth/footer');
        } 
        else {
		

			$user_id = $this->user_model->check_email_login($this->input->post('username'));
			$companyid = $this->user_model->check_company_email_login($this->input->post('username'));

            if($user_id){
				$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
				$password = array(); 
				$alpha_length = strlen($alphabet) - 1; 
				for ($i = 0; $i < 8; $i++) 
				{
					$n = rand(0, $alpha_length);
					$password[] = $alphabet[$n];
				}
		
				$newpassword = implode($password);
				$enc_password = password_hash($newpassword, PASSWORD_DEFAULT);

				$this->user_model->set_password_user($this->input->post('username'), $enc_password);
			
		
				//For emailing reset password link
				$this->load->library('email');
				
				$this->email->from('support@shugulika.com', 'Shugulika');
				$this->email->to($this->input->post('email'),$this->input->post('email'));
				$this->email->subject('Password Reset Request From Shugulika');
				$this->email->message("You requested a password reset. Here is your new password : ".$newpassword);
				
				$this->email->send();
				
	
				$this->session->set_flashdata('email_sent', 'You have been sent an email with a recovery password.');
                redirect('users/login');
			}
			elseif($companyid){

				$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
				$password = array(); 
				$alpha_length = strlen($alphabet) - 1; 
				for ($i = 0; $i < 8; $i++) 
				{
					$n = rand(0, $alpha_length);
					$password[] = $alphabet[$n];
				}
		
				$newpassword = implode($password);
				$enc_password = password_hash($newpassword, PASSWORD_DEFAULT);
				$this->user_model->set_password_company($this->input->post('username'), $enc_password);
	
				//For emailing reset password link
				$this->load->library('email');
				
				$this->email->from('support@shugulika.com', 'Shugulika');
				$this->email->to($this->input->post('email'),$this->input->post('email'));
				$this->email->subject('Password Reset Request From Shugulika');
				$this->email->message("You requested a password reset. Here is your new password : ".$newpassword);
				
				$this->email->send();
				
	
				$this->session->set_flashdata('email_sent', 'You have been sent an email with a recovery password.');
                redirect('users/login');
			}

			else{

				$this->session->set_flashdata('login_failed', 'Request is invalid. Incorrect email address.');
				redirect('users/forgot');
			}
			
		
        }
		
	}
	
	/*
     * Function: username_exists
     * Purpose: This is a callback method used to check if a user's name exists
     *          URL is user/forgot
     * Params: $username: the name of the user
     * Return: True if user exists, false if user doesn't exist
     */
	public function username_exists($username)
	{
		
		$user_exists = $this->user_model->check_user_exists($username);
		if (!$user_exists )
		{
			$this->form_validation->set_message('username_exists', 'That username does not exist');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	/*
     * Function: reset
     * Purpose: This controller is responsible for reseting a user account with a new password
     *          URL is user/reset/TOKEN
     * Params: $token: a unique token that indicates which user needs to update their password
     * Return: none
     */
	public function reset($token)
	{

		$data['title'] = 'Reset';
		$data['token'] = $token;
		
		$user_id = $this->user_model->check_token_exists($token);
		
		if($user_id)
		{
			$this->form_validation->set_rules('password', 'Password', 'required');
			if($this->form_validation->run() === FALSE)
			{
				$this->load->view('templates/header');
				$this->load->view('users/reset', $data);
				$this->load->view('templates/footer');
			} 
			else 
			{
				$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
				$this->user_model->change_password($user_id,$enc_password);
				redirect('users/login');
			}
			
		}
		else
		{
			$this->session->set_flashdata('token_failed', "The link you have clicked has expired. Please click forgot password again.");
		}
		
	}
	
	/*
     * Function: edit
     * Purpose: This is the controller used for the edit users page
     *          URL is /jobs/edit/USER_ID
     * Params:  $user_id: Identifies which specific user
     * Return: none
     */

	function do_upload() {
		
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
			);
	
		$this->load->library('upload', $config);

		if($this->upload->do_upload('userfile'))
		{
			$imgdata = array('upload_data' => $this->upload->data());
	
			$imgname = $imgdata['upload_data']['file_name'];
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

        return $imgname;
    }
	


	 

	//How to get callbacks to work: https://forum.codeigniter.com/thread-64657.html
	/*
     * Function: checkpwdmatch
     * Purpose: This is a callback method used to check if the password 
				entered by a user matches that user's password in the database
     * Params: $password: password user entered
     * Return: True if password belongs to the user, false otherwise
     */
	public function checkpwdmatch($password)
	{
		
		$this->db->where('id', $this->session->userdata('user_id'));
        $result = $this->db->get('users');
		
        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return true;
			}
			else
			{
				$this->form_validation->set_message('checkpwdmatch', 'Incorrect password');
				return FALSE;
			}
            
			
			
        } 
		
		$this->form_validation->set_message('checkpwdmatch', 'The password field can not be empty');
		return FALSE;
        
	}
	
	/*
     * Function: check_username_exists_or_unique
     * Purpose: This is a callback method used to check if the new 
				username a user has entered exists and is not their own
     * Params: $username: username user entered
     * Return: True if username is their own or is unique, false if username is not unique 
     */
	public function check_username_exists_or_unique($username)
	{

		$user = $this->session->userdata('username');
		if($user != $username)
		{
			$this->db->where('username', $username);
			$result = $this->db->get('users');
			
			if($result->row(0))
			{
				$this->form_validation->set_message('check_username_exists_or_unique', 'This username is taken by someone else');
				return FALSE;
			}
			return TRUE;
		
		}
		return TRUE;
        
	}
	
	
	/*
     * Function: check_email_exists_or_unique
     * Purpose: This is a callback method used to check if the new 
				email a user has entered exists and is not their own
     * Params: $email: username user entered
     * Return: True if email is their own or is unique, false if email is not unique 
     */
	public function check_email_exists_or_unique($email)
	{

		$user = $this->session->userdata('email');
		if($user != $email)
		{
			$this->db->where('email', $email);
			$result = $this->db->get('users');
			
			if($result->row(0))
			{
				$this->form_validation->set_message('check_email_exists_or_unique', 'This email is taken by someone else');
				return FALSE;
			}
			return TRUE;
		
		}
		return TRUE;
        
	}
	
	public function joblist()
	{
		if(!$this->session->userdata('user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";


		$this->load->view('templates/home/header.php');
		$this->load->view('templates/home/navbar.php');
		$this->load->view('templates/jobs/listingjob.php', $data);
		$this->load->view('templates/jobs/joblistingbody.php', $data);
		$this->load->view('templates/home/footer.php');
	}
	
	public function testresume()
	{
		// if(!$this->session->userdata('user_id'))
		// {
		// 	redirect('users/login');
		// }
		// $userid= $this->session->userdata('user_id');
		// $currUser = $this->user_model->get_userinfo($userid);

		// $data['user'] = $currUser;
		// $data['title'] = "User Account";

		//$data = [];
		//load the view and saved it into $html variable
		$this->load->view('templates/resume/resume.php');
       	//$this->load->view('templates/resume/resume-temp-1.php', $data, true);

		// $mpdf = new \Mpdf\Mpdf();
        // $html = $this->load->view('templates/resume/resume.php',[],true);
        // $mpdf->WriteHTML($html);
        // $mpdf->Output();
	}
	

	public function resume()
	{
		if(!$this->session->userdata('user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";


		$data['educations'] = $this->user_model->get_education($userid);
		$data['projects'] = $this->user_model->get_projects($userid); 
		$data['experiences'] = $this->user_model->get_experiences($userid); 

		//$data = [];
		//load the view and saved it into $html variable
		// $this->load->view('templates/resume/resume.php');
       	//$this->load->view('templates/resume/resume-temp-1.php', $data, true);


		// 		echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// exit;
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->SetDisplayMode('fullwidth');
		
		$this->load->view('templates/resume/resume1.php' , $data);
        
		// Get output html
		$html = $this->output->get_output();

	
        //$html = $this->load->view('templates/resume/resume.php',[],true);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
	}




	

	
	public function resume1()
	{
		if(!$this->session->userdata('user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/resume/resume1.php');
        
		// Get output html
		$html = $this->output->get_output();
		
		// Load pdf library
		$this->load->library('Pdf');
		
		// Load HTML content
		$this->dompdf->loadHtml($html);
		
		// (Optional) Setup the paper size and orientation
		$this->dompdf->setPaper('A4', 'landscape');
		
		// Render the HTML as PDF
		$this->dompdf->render();
		
		// Output the generated PDF (1 = download and 0 = preview)
		$this->dompdf->stream("resume.pdf", array("Attachment"=>0));
		
		
	}


}
