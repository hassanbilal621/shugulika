<?php
class Admin_model extends CI_Model{
    
    public function login($username, $password){

        $this->db->where('username', $username);
        $result = $this->db->get('admin');

        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return $result->row(0)->id;
			}
			else
			{
				return false;
			}
			
			
        } else {
            return false;
        }
	}


	public function bloggerlogin($username, $password){

        $this->db->where('username', $username);
        $result = $this->db->get('blogger');

        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return $result->row(0)->id;
			}
			else
			{
				return false;
			}
			
			
        } else {
            return false;
        }
	}
		
	
	public function addpost($aurthorname , $avt , $thumbnail)
	{
		$data = array(
            'subject' => $this->input->post('tittle'),
			'content' => $this->input->post('content'),
			'aurthor_name' => $aurthorname,
			'aurthor_avt' => $avt,
			'time' => date('Y-m-d H:i:s'),
			'thumbnail' => $thumbnail
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('blog', $data);
	}


		
	public function add_category()
	{
		$data = array(
            'name' => $this->input->post('catname'),
			'description' => $this->input->post('catdesc'),
			'url' => $this->input->post('catkeyword')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('categories', $data);
	}

	public function get_bloggerinfo($blogger_id)
	{
		$this->db->where('id', $blogger_id);
		$result = $this->db->get('blogger');
		
		return $result->row_array();
	}
	
	public function delblog($blogid)
	{
		$this->db->where('id', $blogid);
		$this->db->delete('blog');
	}

	public function delcategory($catid)
	{
		$this->db->where('id', $catid);
		$this->db->delete('categories');
	
	}
		
	public function get_admininfo($admin_id)
	{
		$this->db->where('id', $admin_id);
		$result = $this->db->get('admin');
		
		return $result->row_array();
	}


		
	public function get_total_jobs()
	{

		$result = $this->db->count_all('jobs');
		
		return $result;
	}
	
	public function get_total_company()
	{

		$result = $this->db->count_all('company');
		
		return $result;
	}
	

	public function get_total_users()
	{

		$result = $this->db->count_all('users');
		
		return $result;
	}
	

	public function get_total_blog()
	{

		$result = $this->db->count_all('blog');
		
		return $result;
	}
	

		
	public function get_settings()
	{
		$this->db->where('id', 1);
		$result = $this->db->get('settings');
		
		return $result->row_array();
	}
	




	public function savesettings()
	{
		$data = array(
            'address' => $this->input->post('address'),
			'support_email' => $this->input->post('support_email'),
			'contact_number' => $this->input->post('contact_number'),
			'about_us' => $this->input->post('about_us'),
			'facebook' => $this->input->post('facebook'),
			'twitter' => $this->input->post('twitter'),
			'linkdin' => $this->input->post('linkdin')
        );
		
		$this->security->xss_clean($data);
		$this->db->where('id', 1);
		$this->db->update('settings', $data);

	}


	// Users Get / Add / Update / Delete 
	
	public function add_user($imgname, $enc_password)
	{
		$data = array(
            'name' => $this->input->post('name'),
			'fathername' => $this->input->post('fname'),
			'phone' => $this->input->post('phone'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s'),
			'address' => $this->input->post('address'),
			//'country' => $this->input->post('country'),
            //'city' => $this->input->post('city'),
			'picture' => $imgname
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('users', $data);
	}

	public function update_user($user_id, $imgname)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'fathername' => $this->input->post('fname'),
			'phone' => $this->input->post('phone'),
			'picture' => $imgname
        );
		
		$this->security->xss_clean($data);
		$this->db->where('id', $user_id);
		$this->db->update('users', $data);

	}


	public function del_user($userid)
	{
		$this->db->where('id', $userid);
		$this->db->delete('users');
	}

	public function get_users(){
        $this->db->order_by('users.id', 'DESC');
        $query = $this->db->get('users');
        return $query->result_array();
	}

	public function get_categories(){
        $this->db->order_by('categories.id', 'DESC');
        $query = $this->db->get('categories');
        return $query->result_array();
	}
	


	public function get_companies(){
        $this->db->order_by('company.company_id', 'DESC');
        $query = $this->db->get('company');
        return $query->result_array();
	}


	public function get_countries(){
        $this->db->order_by('countries.country_name', 'ASC');
        $query = $this->db->get('countries');
        return $query->result_array();
	}

	
	public function get_cities($country_name){
		$this->db->where('country_name', $country_name);
        $this->db->order_by('cities.city_name', 'ASC');
        $query = $this->db->get('cities');
        return $query->result_array();
	}

	public function get_bloggers(){
        $this->db->order_by('blogger.id', 'DESC');
        $query = $this->db->get('blogger');
        return $query->result_array();
	}

	public function get_admins(){
        $this->db->order_by('admin.id', 'DESC');
        $query = $this->db->get('admin');
        return $query->result_array();
    }

	public function add_admin($enc_password)
	{
		$data = array(
            'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('admin', $data);
	}

	public function add_city()
	{
		$data = array(
            'geoname_id' => $this->input->post('geoname_id'),
			'country_iso_code' => $this->input->post('country_iso_code'),
			'country_name' => $this->input->post('country_name'),
			'time_zone' => $this->input->post('time_zone'),
			'city_name' => $this->input->post('city_name')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('cities', $data);
	}

	
	
	public function add_blogger($enc_password)
	{
		$data = array(
            'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('blogger', $data);
	}

	public function update_admin($admin_id, $enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
            'password' => $enc_password
        );
		
		$this->security->xss_clean($data);
		$this->db->where('id', $admin_id);
		$this->db->update('admin', $data);

	}



	public function del_company($companyid)
	{
		$data = array(
			'status' => "disable"
        );
		
		$this->security->xss_clean($data);
		$this->db->where('company_id', $companyid);
		$this->db->update('company', $data);

	}

	
	public function enb_company($companyid)
	{
		$data = array(
			'status' => "enable"
        );
		
		$this->security->xss_clean($data);
		$this->db->where('company_id', $companyid);
		$this->db->update('company', $data);

	}




	public function update_blogger($blogger_id, $enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
            'password' => $enc_password
		);
		$this->security->xss_clean($data);
		$this->db->where('id', $blogger_id);
		$this->db->update('blogger', $data);
	}
	

	public function del_admin($adminid)
	{
		$this->db->where('id', $adminid);
		$this->db->delete('admin');
	}


	public function delorder($orderid)
	{
		$this->db->where('orderid', $orderid);
		$this->db->delete('orders');
	}


	public function del_bloggers($bloggerid)
	{
		$this->db->where('id', $bloggerid);
		$this->db->delete('blogger');
	}
	public function del_city($city_name)
	{
		$this->db->where('city_name', $city_name);
		$this->db->delete('cities');
	}

	public function get_jobs(){
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.company_id', 'left');
		$query = $this->db->get('jobs');
		
        return $query->result_array();
	}

	public function get_approvejobs(){
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');
		
        return $query->result_array();
	}
	
	public function get_pendingjobs(){
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');
		
        return $query->result_array();
	}
	
	public function get_appliedjobs(){
		$this->db->order_by('applications.appid', 'DESC');
		$this->db->join('users', 'applications.userid = users.id', 'left');
		$this->db->join('jobs', 'applications.jobid = jobs.jobid', 'left');
		$query = $this->db->get('applications');
		
        return $query->result_array();
	}
	
	public function get_orders(){
		$this->db->order_by('orders.orderid', 'DESC');
		$this->db->join('users', 'orders.userid = users.id', 'left');
		$this->db->join('invoices', 'orders.invoiceid = invoices.invoiceid', 'left');
		$query = $this->db->get('orders');
		
        return $query->result_array();
	}


	public function get_invoices(){
		$this->db->order_by('invoices.invoiceid', 'DESC');
		$this->db->join('orders', 'invoices.orderid = orders.invoiceid', 'left');
		$this->db->join('users', 'orders.userid = users.id', 'left');

		$query = $this->db->get('invoices');
		
        return $query->result_array();
	}


}