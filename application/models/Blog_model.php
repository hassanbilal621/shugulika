<?php

class Jobs_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }
	/*
     * Function: get_jobs
     * Purpose: This method is responsible for retrieving jobs from the database and then returning them 
     * Params:  $jobnum= FALSE: this is to get a specific job
				$limit = 0: default value of 0, this is for the use of pagination
				$offset = "": default value of 0, this is for the use of pagination
				$where = "": this is the where clause for search results. 
							Should be in form: array('field'=>FIELD, 'value'=>VALUE)
     * Return: query result - single array if $jobnum is not FALSE
     */
    public function getblog(){
        $this->db->order_by('blog_posts.id', 'DESC');
        $query = $this->db->get('blog_posts', 5);
        return $query->result_array();
    }


    public function get_blogs($blogid)
    {

		$this->db->where('id', $blogid);
		$result = $this->db->get('blog_posts');
		
		return $result->row_array();
	}

    public function get_blog_comments($blogid){
        $this->db->where('blogid', $blogid);
        $this->db->order_by('blog_comment.id', 'DESC');
        $this->db->join('users', 'blog_comment.userid = users.id', 'left');
        $query = $this->db->get('blog_comment', 10);
        return $query->result_array();
    }
	
	public function delblog($blogid)
	{
		$this->db->where('id', $blogid);
		$this->db->delete('blog');
	}



}

?>