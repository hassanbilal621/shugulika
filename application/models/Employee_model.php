<?php
class Employee_model extends CI_Model{



    public function company_login($username, $password){
		echo "bakwas";
		exit;

        $this->db->where('email', $username);
        $result = $this->db->get('company');

        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return $result->row(0)->company_id;
			}
			else
			{
				return false;
			}
            
			
			
        } else {
            return false;
        }
	}
	

	public function get_activejob($company_user_id)
	{

		$this->db->from('jobs');
		$this->db->where('companyid', $company_user_id);
		$query = $this->db->get();
		return $query->num_rows();
	}
	

	public function get_expiredjob($company_user_id)
	{
		$this->db->from('jobs');
		$this->db->where('companyid', $company_user_id);
		$query = $this->db->get();
		return $query->num_rows();
	}
	


	public function update($company_user_id, $imgname)
	{
		$data = array(
            'name' => $this->input->post('name'),
            'company_desc' => $this->input->post('company_desc'),
			'address' => $this->input->post('address'),
			'company_title' => $this->input->post('company_title'),
			'companyname' => $this->input->post('companyname'),
			'logo' => $imgname
        );
		
		
		$this->security->xss_clean($data);
		$this->db->where('company_id', $company_user_id);
        $this->db->update('company', $data);
	}
	
	public function get_chats($company_user_id){
		$this->db->order_by('chats.chatid', 'DESC');
		$this->db->where('companyid', $company_user_id);
		$this->db->join('users', 'chats.userid = users.id', 'left');

        $query = $this->db->get('chats');
        return $query->result_array();
	}
	

	public function get_msgs($chatid){
		$this->db->order_by('msg.msgid', 'ASC');


		$this->db->join('chats', 'msg.chatid = chats.chatid', 'left');
		$this->db->join('company', 'chats.companyid = company.company_id', 'left');
		$this->db->join('users', 'chats.userid = users.id', 'left');
		$this->db->where('msg.chatid', $chatid);

		//$this->db->where('chatid', $chatid);

        $query = $this->db->get('msg');
        return $query->result_array();
	}

	public function sendmsg()
	{
		$data = array(
			'text' => $this->input->post('text'),
            'msgby' => 2,
            'time' => date('Y-m-d H:i:s'),
            'chatid' => $this->input->post('chatid')
        );
				
		$this->security->xss_clean($data);
        $this->db->insert('msg', $data);
	}


	public function createchat($userid ,$companyid)
	{
		$data = array(
            'userid' => $userid,
            'companyid' => $companyid
        );
				
		$this->security->xss_clean($data);
        $this->db->insert('chats', $data);
	}
	
	
	/*
     * Function: check_username_exists
     * Purpose: This method checks to see if passed in username exists in database, 
				if does will return true, otherwise false
     * Params: $username - the username
     * Return: True if username exists, false if not
     */
    public function check_company_exists($username)
	{
		$this->db->where('username', $username);
		$result = $this->db->get('users');
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	public function remove_company_user()
	{
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$this->db->delete('jobs');
		
		$this->db->where('id', $this->session->userdata('user_id'));
		$this->db->delete('users');
	}
	
	
	public function get_companyinfo($company_user_id)
	{
		$this->db->where('company_id', $company_user_id);
		$result = $this->db->get('company');
		
		return $result->row_array();
	}
	
	
	public function update_company($user_id)
	{
		$data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'pcode' => $this->input->post('pcode'),
			'address' => $this->input->post('addr'),
			'city' => $this->input->post('city'),
			'prov' => $this->input->post('prov'),
			'phone' => $this->input->post('phone'),
			'fax' => $this->input->post('fax')
        );
		
		
		$this->security->xss_clean($data);
		$this->db->where('id', $user_id);
        $this->db->update('users', $data);
	}


	public function closejob($jobid)
	{
		$setdate = "2017-08-27 07:18:45";
		$data = array(
			'date_ending' => $setdate
        );
		
		
		$this->security->xss_clean($data);
		$this->db->where('jobid', $jobid);
        $this->db->update('jobs', $data);
	}
}


