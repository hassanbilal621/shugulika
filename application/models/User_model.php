<?php
class User_model extends CI_Model{
	
    public function register($enc_password){
        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => $enc_password,
			'address' => $this->input->post('addr'),
			'city' => $this->input->post('city'),
			'country' => $this->input->post('country'),
			'phone' => $this->input->post('phone')

        );

		$this->security->xss_clean($data);
        return $this->db->insert('users', $data);
	}



    public function registercompany($enc_password){
        $data = array(
            'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'companyname' => $this->input->post('companyname'),
			'company_desc' => $this->input->post('company_desc'),
			'company_title' => $this->input->post('company_title'),
            'password' => $enc_password,
			'address' => $this->input->post('addr'),
			'city' => $this->input->post('city'),
			'country' => $this->input->post('country'),
			'status' => "enable",
			'phone' => $this->input->post('phone')

        );

		$this->security->xss_clean($data);
        return $this->db->insert('company', $data);
	}


	public function addeducation($user_id)
	{
		$data = array(
            'degree' => $this->input->post('degree'),
            'desc' => $this->input->post('desc'),
            'startdate' => $this->input->post('startdate'),
            'enddate' => $this->input->post('enddate'),
			'type' => $this->input->post('type'),
			'userid' => $user_id
        );
		
		
		$this->security->xss_clean($data);
        $this->db->insert('education', $data);
	}

	public function addexperience($user_id)
	{
		$data = array(
			'tittle' => $this->input->post('tittle'),
			'company' => $this->input->post('company'),
            'desc' => $this->input->post('desc'),
            'startdate' => $this->input->post('startdate'),
            'enddate' => $this->input->post('enddate'),
			'userid' => $user_id
        );
		
		
		$this->security->xss_clean($data);
        $this->db->insert('experiences', $data);
	}

	public function addproject($user_id, $imgname)
	{

		$data = array(
			'tittle' => $this->input->post('tittle'),
            'desc' => $this->input->post('desc'),
            'date' => $this->input->post('startdate'),
            'img' => $imgname,
			'userid' => $user_id
        );
				
		$this->security->xss_clean($data);
        $this->db->insert('projects', $data);
	}



	public function get_chats($userid){
		$this->db->order_by('chats.chatid', 'DESC');
		$this->db->where('userid', $userid);
		$this->db->join('company', 'chats.companyid = company.company_id', 'left');

        $query = $this->db->get('chats');
        return $query->result_array();
	}
	

	public function get_msgs($chatid){
		$this->db->order_by('msg.msgid', 'ASC');


		$this->db->join('chats', 'msg.chatid = chats.chatid', 'left');
		$this->db->join('company', 'chats.companyid = company.company_id', 'left');
		$this->db->join('users', 'chats.userid = users.id', 'left');
		$this->db->where('msg.chatid', $chatid);

		//$this->db->where('chatid', $chatid);

        $query = $this->db->get('msg');
        return $query->result_array();
	}
	
	public function sendmsg()
	{
		$data = array(
			'text' => $this->input->post('text'),
            'msgby' => 1,
            'time' => date('Y-m-d H:i:s'),
            'chatid' => $this->input->post('chatid')
        );
				
		$this->security->xss_clean($data);
        $this->db->insert('msg', $data);
	}


	public function get_education($userid){
		$this->db->order_by('education.id', 'DESC');
		$this->db->where('userid', $userid);
		
        $query = $this->db->get('education');
        return $query->result_array();
    }

	public function get_projects($userid){
		$this->db->order_by('projects.id', 'DESC');
		$this->db->where('userid', $userid);
        $query = $this->db->get('projects');
        return $query->result_array();
    }

	public function get_experiences($userid){
		$this->db->order_by('experiences.id', 'DESC');
		$this->db->where('userid', $userid);
        $query = $this->db->get('experiences');
        return $query->result_array();
    }

	public function add_blog_comments($userid){
        $data = array(
            'comment' => $this->input->post('comment'),
			'blogid' => $this->input->post('blogid'),
			'useravt' => $this->input->post('useravt'),
            'datetime' => date('Y-m-d H:i:s'),
			'user' => $userid
        );

		$this->security->xss_clean($data);
        return $this->db->insert('blog_comment', $data);
	}
	
    public function get_blog_all(){
        $this->db->order_by('blog.id', 'DESC');
        $query = $this->db->get('blog', 10);
        return $query->result_array();
    }
	
    public function login($username, $password){

        $this->db->where('email', $username);
        $result = $this->db->get('users');
	
        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return $result->row(0)->id;
			}
			else
			{
				return false;
			}
            
			
			
        } else {
            return false;
        }
    }
	
	public function check_email_login($email){
	
        $this->db->where('email', $email);
        $result = $this->db->get('users');

        if($result->num_rows() == 1){
			return true;			
			
        } else {
            return false;
        }
    }
	
	public function check_company_email_login($email){
	
        $this->db->where('email', $email);
        $result = $this->db->get('company');

        if($result->num_rows() == 1){
			return true;			
			
        } else {
            return false;
        }
    }
    public function company_login($username, $password){
	
        $this->db->where('email', $username);
        $result = $this->db->get('company');

        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return $result->row(0)->company_id;
			}
			else
			{
				return false;
			}
            
			
			
        } else {
            return false;
        }
	}
	


	public function companydisable($companyid){
	
		$this->db->where('company_id', $companyid);
		$this->db->where('status', "enable");
        $result = $this->db->get('company');

        if($result->num_rows() == 1){
	
			return false;
					
        } else {
            return true;
        }
    }
	/*
     * Function: check_username_exists
     * Purpose: This method checks to see if passed in username exists in database, 
				if does will return true, otherwise false
     * Params: $username - the username
     * Return: True if username exists, false if not
     */
    public function check_user_exists($username)
	{
		$this->db->where('username', $username);
		$result = $this->db->get('users');
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function delproject($projectid)
	{
		$this->db->where('id', $projectid);
		$this->db->delete('projects');
	}
	public function delexperience($expid)
	{
		$this->db->where('id', $expid);
		$this->db->delete('experiences');
	}
	public function deleducation($eduid)
	{
		$this->db->where('id', $eduid);
		$this->db->delete('education');
	}

	public function remove_user()
	{
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$this->db->delete('jobs');
		
		$this->db->where('id', $this->session->userdata('user_id'));
		$this->db->delete('users');
	}
	
	public function set_password_reset_token($username, $token)
	{
		
		$this->db->where('username', $username);
		$data = array(
			'password_reset_token'=>$token
		);
		$this->db->update('users', $data);

	}
	

	public function set_password_user($email, $password)
	{
		$this->db->where('email', $email);
		$data = array(
			'password'=>$password
		);
		$this->db->update('users', $data);
	}


	public function set_password_company($email, $password)
	{
		$this->db->where('email', $email);
		$data = array(
			'password'=>$password
		);
		$this->db->update('company', $data);
	}


	public function check_token_exists($token)
	{

		$this->db->where('password_reset_token', $token);
		$result = $this->db->get('users');
		if($result)
		{
			return $result->row(0)->id;
		}
		else
		{
			return 0;
		}
	}
	public function change_password($user_id,$enc_password)
	{
		$this->db->where('id', $user_id);
		$data = array(
			'password'=>$enc_password,
			'password_reset_token'=>""
		);
		$this->db->update('users', $data);

		
		
	}

	public function make_session()
	{
		$user_id = get_cookie('jobboard_user_id');
		if($user_id)
		{
			$this->db->where('id', $user_id);
			$result = $this->db->get('users');
			
			$user_data = array(
				'user_id' => $user_id,
				'username' => $result->row(0)->username,
				'logged_in' => true
			);
			$this->session->set_userdata($user_data);
		}
		


	}
	
	public function get_userinfo($user_id)
	{
		$this->db->where('id', $user_id);
		$result = $this->db->get('users');
		
		return $result->row_array();
	}
	

	
    public function get_blogs($blogid)
    {

		$this->db->where('id', $blogid);
		$result = $this->db->get('blog');
		
		return $result->row_array();
	}


	public function get_blog_comments($blogid){
        $this->db->where('blogid', $blogid);
		$this->db->order_by('blog_comment.id', 'DESC');
		$this->db->join('users', 'blog_comment.user = users.id', 'left');
        $query = $this->db->get('blog_comment', 10);
        return $query->result_array();
    }
	/*
     * Function: get_empname
     * Purpose: This method is responsible for returning the name of an employer( user's name)
     * Params:  $userid: the id of the user
     * Return: name of a user
     */
    public function get_empname($userid)
    {
        $query = $this->db->get_where('users', array('id'=>$userid));
        return $query->row_array()['name'];
    }
	
	public function update($user_id, $imgname)
	{
		$data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
			'address' => $this->input->post('address'),
            'fathername' => $this->input->post('fname'),
			'phone' => $this->input->post('phone'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'bio' => $this->input->post('bio'),
			'picture' => $imgname
        );
		
		
		$this->security->xss_clean($data);
		$this->db->where('id', $user_id);
        $this->db->update('users', $data);
	}
	
	public function add_document($userid, $filename)
	{

		$data = array(
			'cv_attach' => $filename
        );
				
		$this->security->xss_clean($data);
		$this->db->where('id', $userid);
        $this->db->update('users', $data);
	}




}


