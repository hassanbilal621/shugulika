<?php return array (
  'sans-serif' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $fontDir . '\ZapfDingbats',
    'bold' => $fontDir . '\ZapfDingbats',
    'italic' => $fontDir . '\ZapfDingbats',
    'bold_italic' => $fontDir . '\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $fontDir . '\Symbol',
    'bold' => $fontDir . '\Symbol',
    'italic' => $fontDir . '\Symbol',
    'bold_italic' => $fontDir . '\Symbol',
  ),
  'serif' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $fontDir . '\DejaVuSans-Bold',
    'bold_italic' => $fontDir . '\DejaVuSans-BoldOblique',
    'italic' => $fontDir . '\DejaVuSans-Oblique',
    'normal' => $fontDir . '\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $fontDir . '\DejaVuSansMono-Bold',
    'bold_italic' => $fontDir . '\DejaVuSansMono-BoldOblique',
    'italic' => $fontDir . '\DejaVuSansMono-Oblique',
    'normal' => $fontDir . '\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $fontDir . '\DejaVuSerif-Bold',
    'bold_italic' => $fontDir . '\DejaVuSerif-BoldItalic',
    'italic' => $fontDir . '\DejaVuSerif-Italic',
    'normal' => $fontDir . '\DejaVuSerif',
  ),
  'roboto' => array(
    'normal' => $fontDir . '\200a2d78ffb8c24ec732e40ccfad00bc',
  ),
  'material icons' => array(
    'normal' => $fontDir . '\58df3c1a3226715c92128e811e423d04',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '\ac37173b05988b8ae0deeabe52422fd6',
  ),
  'ionicons' => array(
    'normal' => $fontDir . '\bf29d2340536ec8385f7315d65d1d736',
  ),
  'glyphicons halflings' => array(
    'normal' => $fontDir . '\a98edd391a83216f205791dd7ce6e86f',
  ),
  'font awesome 5 brands' => array(
    'normal' => $fontDir . '\2a453c7ae13b9db116591a9e7b3ccf33',
  ),
  'font awesome 5 free' => array(
    'normal' => $fontDir . '\24fb7901799fa94fb6eb77f7bd62cee4',
  ),
) ?>