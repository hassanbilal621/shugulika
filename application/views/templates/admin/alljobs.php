

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/flatly/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/filter/styless.css">

    
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>All Jobs</h5></div>
                                </div>  
                            </div>
                            <div class="container">
                                <input type="search" id="search" class="form-control" placeholder="Type here to Search">
                                <div id="root"></div>
                                <div class="pages"></div>
                            </div>
                            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                            <script src="<?php echo base_url();?>assets/filter/table-sortable.js"></script>
                            <script>
                            var data = [
                                        <?php foreach ($alljobs as $allorder): ?>
                                        {
                                            orderid: "<?php echo $allorder['jobid']; ?>",
                                            orderby: "<?php echo $allorder['name']; ?>",
                                            email: "<?php echo $allorder['companyname']; ?>",
                                            title: "<?php echo $allorder['title']; ?>",
                                            invoiceid: "<?php echo $allorder['date_posted']; ?>",
                                            subject: "<?php echo $allorder['date_ending']; ?>"
                        
                                        },
                                        <?php endforeach; ?>
                                        ]

                                    var columns = {
                                        'orderid': 'Job ID',
                                        'orderby': 'User Name',
                                        'email': 'Company Name',
                                        'title': 'Job Title',
                                        'invoiceid': 'Date Posted',
                                        'subject': 'Date Ending'
                                    }
         
                            </script>
                            <script src="<?php echo base_url();?>assets/filter/script.js"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>