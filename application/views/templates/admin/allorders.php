

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/flatly/bootstrap.min.css"> -->
    <link rel="stylesheet" href="../assets/filter/styless.css">

    
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>All Orders</h5></div>
                                </div>  
                            </div>
                            <div class="container">
                                <input type="search" id="search" class="form-control" placeholder="Type here to Search">
                                <div id="root"></div>
                                <div class="pages"></div>
                            </div>
                            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                            <script src="<?php echo base_url();?>assets/filter/table-sortable.js"></script>
                            <script>
                            var data = [
                                        <?php foreach ($allorders as $allorder): ?>
                                        {
                                            orderid: "<?php echo $allorder['orderid']; ?>",
                                            orderby: "<?php echo $allorder['name']; ?>",
                                            email: "<?php echo $allorder['email']; ?>",
                                            title: "<?php echo $allorder['title']; ?>",
                                            invoiceid: "<?php echo $allorder['invoiceid']; ?>",
                                            subject: "<?php echo $allorder['subject']; ?>",
                                            invoicestatus: "<?php echo $allorder['paystatus']; ?>",
                                            expiry: "<?php echo $allorder['expiry']; ?>",
                                            actions: '<a href="<?php echo base_url(); ?>admin/delorder/<?php echo $allorder['orderid']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a> ',
                                            status: '<?php
                                                        $currdate =  date("d/m/Y");
                                                        $dateexpiry=date_create($allorder['expiry']);
                                            
                                                        if($currdate < date_format($dateexpiry,"d/m/Y")){ 
                                                        echo 'Active ' ; } 
                                                        else{    
                                                            echo 'Expired' ; 
                                                        } ?>'
                                        },
                                        <?php endforeach; ?>
                                        ]

                                    var columns = {
                                        'orderid': 'Order ID',
                                        'orderby': 'Order By',
                                        'email': 'Email',
                                        'title': 'Title',
                                        'invoiceid': 'Invoice ID',
                                        'subject': 'Subject',
                                        'invoicestatus': 'Invoice Status',
                                        'expiry': 'Order Expiry',
                                        'actions': 'Actions',
                                        'status': 'Order Status'
                                    }
         
                            </script>
                            <script src="<?php echo base_url();?>assets/filter/script.js"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>