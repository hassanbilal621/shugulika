<!-- 
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>All Applications</h5>
                                </div>  
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Application ID</th>
                                        <th>Job ID</th>
                                        <th>Job Tittle</th>
                                        <th>Applicant Name</th>     
                                        <th>Applicant Email</th>
                                        <th>Applied Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
         
                                <tbody>
                                    <?php foreach ($applications as $application): ?>
                                    <tr>
                                        <td><?php echo $application['appid']; ?></td>
                                        <td><?php echo $application['jobid']; ?></td>
                                        <td><?php echo $application['title']; ?></td>
                                        <td><?php echo $application['name']; ?></td>
                                        <td><?php echo $application['email']; ?></td>
                                        <td><?php echo $application['created']; ?></td>
        
                                        <td>
                                            <a href="<?php echo base_url(); ?>assets/uploads/<?php echo $application['attachment']; ?>" class="delete" title="Download Attachment" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">cloud_download</i></button></a>
                                            <a href="<?php echo base_url(); ?>users/resume/view/<?php echo $application['userid']; ?>" class="delete" title="View PDF Resume" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">picture_as_pdf</i></button></a>
                                        </td>
                                    </tr>
                                    
                                    <?php endforeach; ?>
           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     -->


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/filter/styless.css">

    
<div class="row">
    <div class="col s12 m12 l12">
        <div class="card-panel">
            <div class="row">
                <div class="container">
                     <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-8"><h5>All Applications</h5></div>
                            </div>  
                        </div>
                        <div class="container">
                            <input type="search" id="search" class="form-control" placeholder="Type here to Search">
                            <div id="root"></div>
                            <div class="pages"></div>
                        </div>
                        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                        <script src="<?php echo base_url();?>assets/filter/table-sortable.js"></script>
                        <script>
                        var data = [
                                    <?php foreach ($applications as $user): ?>
                                    {
                                        appid: "<?php echo $user['appid']; ?>",
                                        jobid: "<?php echo $user['jobid']; ?>",
                                        title: "<?php echo $user['title']; ?>",
                                        name: "<?php echo $user['name']; ?>",
                                        email: "<?php echo $user['email']; ?>",
                                        created: "<?php echo $user['created']; ?>",
                                        actions: '</a><a href="<?php echo base_url(); ?>admin/resume/<?php echo $application['userid']; ?>" class="delete" target="_blank" title="View PDF Resume" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">picture_as_pdf</i></button></a>'

                                    },
                                    <?php endforeach; ?>
                                    ]

                                var columns = {
                                    'appid': 'Application ID',
                                    'jobid': 'Job ID',
                                    'title': 'Job Tittle',
                                    'name': 'Applicant Name',
                                    'email': 'Applicant Email',
                                    'created': 'Applied Date',
                                    'actions': 'Actions'
                                }
     
                        </script>
                        <script src="<?php echo base_url();?>assets/filter/script.js"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


