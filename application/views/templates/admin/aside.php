    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav" class="nav-expanded nav-lock nav-collapsible">
          <div class="brand-sidebar">
            <h1 class="logo-wrapper">
              <a href="index.html" class="brand-logo darken-1">
                <span class="logo-text hide-on-med-and-down"> <img src="<?php echo base_url(); ?>assets/admin/images/logo/logo-size.png" style="width:140px !important; height:30px !important;" alt="materialize logo"></span>
              </a>
              <a href="#" class="navbar-toggler">
                <i class="material-icons">radio_button_checked</i>
              </a>
            </h1>
          </div>
          <ul id="slide-out" class="side-nav fixed leftside-navigation">
            <li class="no-padding">
              <ul class="collapsible" data-collapsible="accordion">
     
                  <li class="bold">
                    <a href="<?php echo base_url(); ?>admin/" class="waves-effect waves-cyan">
                      <i class="material-icons">dashboard</i>
                      <span class="nav-text">Dashboard</span>
                    </a>
                  </li>

                  <li class="bold">
                    <a href="<?php echo base_url();?>admin/alljobs" class="waves-effect waves-cyan">
                      <i class="material-icons">description</i>
                      <span class="nav-text">Jobs</span>
                    </a>
                  </li>


                  <!-- <li class="bold">
                    <a href="<?php echo base_url();?>admin/allorders" class="waves-effect waves-cyan">
                      <i class="material-icons">business_center</i>
                      <span class="nav-text">Orders</span>
                    </a>
                  </li> -->

<!-- 
                  <li class="bold">
                    <a href="<?php echo base_url();?>admin/appliedjobs" class="waves-effect waves-cyan">
                      <i class="material-icons">business_center</i>
                      <span class="nav-text">Invoices</span>
                    </a>
                  </li> -->

                  <!-- <li class="bold">
                  <a class="collapsible-header waves-effect waves-cyan">
                    <i class="material-icons">description</i>
                    <span class="nav-text"> Jobs</span>
                  </a>
                  <div class="collapsible-body">
                    <ul>
                      <li>
                        <a href="<?php echo base_url();?>admin/postedjobs">
                          <i class="material-icons">keyboard_arrow_right</i>
                          <span>All Posted Jobs</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo base_url();?>admin/pendingjobs">
                          <i class="material-icons">keyboard_arrow_right</i>
                          <span>Pending Jobs</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo base_url();?>admin/approvejobs">
                          <i class="material-icons">keyboard_arrow_right</i>
                          <span>Approved Jobs</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  </li> -->
                  <!-- <li class="bold">
                  <a class="collapsible-header waves-effect waves-cyan">
                    <i class="material-icons">business_center</i>
                    <span class="nav-text"> Orders</span>
                  </a>
                  <div class="collapsible-body">
                    <ul>
                      <li>
                        <a href="<?php echo base_url();?>admin/allorders">
                          <i class="material-icons">keyboard_arrow_right</i>
                          <span>All  Orders</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo base_url();?>admin/pendingorders">
                          <i class="material-icons">keyboard_arrow_right</i>
                          <span>Pending Orders</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo base_url();?>admin/activeorders">
                          <i class="material-icons">keyboard_arrow_right</i>
                          <span>Active Orders</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  </li> -->
                  <!-- <li class="bold">
                  <a class="collapsible-header waves-effect waves-cyan">
                    <i class="material-icons">business_center</i>
                    <span class="nav-text"> Invoices</span>
                  </a>
                  <div class="collapsible-body">
                    <ul>
                      <li>
                        <a href="<?php echo base_url();?>admin/invoices">
                          <i class="material-icons">keyboard_arrow_right</i>
                          <span>All  Invoices</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo base_url();?>admin/unpaidinvoices">
                          <i class="material-icons">keyboard_arrow_right</i>
                          <span>Unpaid Invoices</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo base_url();?>admin/paidinvoices">
                          <i class="material-icons">keyboard_arrow_right</i>
                          <span>Paid Invoices</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  </li> -->
                  <li class="bold">
                    <a href="<?php echo base_url();?>admin/appliedjobs" class="waves-effect waves-cyan">
                      <i class="material-icons">assignment</i>
                      <span class="nav-text">Applications</span>
                    </a>
                  </li>
                  <!-- <li class="bold">
                    <a href="<?php echo base_url();?>admin/support" class="waves-effect waves-cyan">
                      <i class="material-icons">contact_mail</i>
                      <span class="nav-text">Support</span>
                    </a>
                  </li> -->
    
                  <li class="bold">
                    <a href="<?php echo base_url();?>admin/administrators" class="waves-effect waves-cyan">
                      <i class="material-icons">enhanced_encryption</i>
                      <span class="nav-text">Adminstrators</span>
                    </a>
                  </li>
                  <!-- <li class="bold">
                    <a href="<?php echo base_url();?>admin/reports" class="waves-effect waves-cyan">
                      <i class="material-icons">style</i>
                      <span class="nav-text">Reports</span>
                    </a>
                  </li> -->
                  <li class="bold">
                    <a href="<?php echo base_url();?>admin/bloggers" class="waves-effect waves-cyan">
                      <i class="material-icons">person_outline</i>
                      <span class="nav-text">Bloggers</span>
                    </a>
                  </li>

                  <li class="bold">
                    <a href="<?php echo base_url();?>admin/users" class="waves-effect waves-cyan">
                      <i class="material-icons">group</i>
                      <span class="nav-text">Users</span>
                    </a>
                  </li>
                  <li class="bold">
                    <a href="<?php echo base_url();?>admin/companies" class="waves-effect waves-cyan">
                      <i class="material-icons">next_week</i>
                      <span class="nav-text">Companies</span>
                    </a>
                  </li>
                  <li class="bold">
                    <a href="<?php echo base_url();?>admin/countries" class="waves-effect waves-cyan">
                      <i class="material-icons">public</i>
                      <span class="nav-text">Countries</span>
                    </a>
                  </li>
                  <li class="bold">
                    <a href="<?php echo base_url();?>admin/category" class="waves-effect waves-cyan">
                      <i class="material-icons">mail_outline</i>
                      <span class="nav-text">Categories</span>
                    </a>
                  </li>
                  <li class="bold">
                    <a href="<?php echo base_url();?>admin/settings" class="waves-effect waves-cyan">
                      <i class="material-icons">mail_outline</i>
                      <span class="nav-text">Settings</span>
                    </a>
                  </li>
                </ul>
              </li>
            </li>
          </ul>
          <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only gradient-45deg-light-blue-cyan gradient-shadow">
            <i class="material-icons">menu</i>
          </a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->