<div class="row">
    <div class="col s12 m12 l12">
        <div class="card-panel">
            <h4 class="header2">Add Category</h4>
            <div class="row">
            <?php echo form_open('admin/category'); ?>
                <div class="col s12">
                    <div class="row">
                    <div class="input-field col s6">
                        <input type="text" name="catname" required placeholder="Address*">
                        <label for="catname">Category Name</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="text" name="catdesc"  placeholder="Support Email*">
                        <label for="catdesc">Category Description</label>
                    </div>
                    </div>


                    <div class="row">
                    <div class="input-field col s6">
                        <input type="text" name="catkeyword"  placeholder="Contact Number*">
                        <label for="catkeyword">Category KeyWord</label>
                    </div>
    
                    </div>
     

                    <!-- <div class="row">
                    <div class="input-field col s12">
                        <input id="email5" type="email">
                        <label for="email">Email</label>
                    </div>
                    </div> -->

                    <div class="row">
                        <div class="input-field col s12">
                        <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Add Category
                            <i class="material-icons right">send</i>
                        </button>
                        </div>
                    </div>
                    </div>
                </div>
            <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>




<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/flatly/bootstrap.min.css"> -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/filter/styless.css">


<div class="row">
    <div class="col s12 m12 l12">
        <div class="card-panel">
            <div class="row">
                <div class="container">
                        <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-8"><h5>All Categories</h5></div>
                            </div>  
                        </div>
                        <div class="container">
                            <input type="search" id="search" class="form-control" placeholder="Type here to Search">
                            <div id="root"></div>
                            <div class="pages"></div>
                        </div>
                        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                        <script src="<?php echo base_url();?>assets/filter/table-sortable.js"></script>
                        <script>
                        var data = [
                                    <?php foreach ($categories as $allorder): ?>
                                    {
                                        orderid: "<?php echo $allorder['id']; ?>",
                                        orderby: "<?php echo $allorder['name']; ?>",
                                        email: "<?php echo $allorder['description']; ?>",
                                        subject: "<?php echo $allorder['url']; ?>",
                                        actions: '<a href="<?php echo base_url(); ?>admin/delcategory/<?php echo $allorder['id']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a>'
                    
                                    },
                                    <?php endforeach; ?>
                                    ]

                                var columns = {
                                    'orderid': 'ID',
                                    'orderby': 'Category Name',
                                    'email': 'Category Description',
                                    'subject': 'Category Keyword',
                                    'actions': 'Actions'

                                }
        
                        </script>
                        <script src="<?php echo base_url();?>assets/filter/script.js"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>