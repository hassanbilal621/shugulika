
    <style>
        table.table tr th, table.table tr td {
            border-color: #e9e9e9;
        }   

    </style>

    <!-- <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>Company List</h5></div>
                                </div>  
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Company Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Registration Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
         
                                <tbody>
                                    <?php foreach ($users as $user): ?>
                                    <tr>
                                        <td><?php echo $user['company_id']; ?></td>
                                        <td><?php echo $user['name']; ?></td>
                                        <td><?php echo $user['companyname']; ?></td>
                                        <td><?php echo $user['email']; ?></td>
                                        <td><?php echo $user['address']; ?></td>
                                        <td><?php echo $user['phone']; ?></td>
                                        <td><?php echo $user['register_date']; ?></td>
                                        <td>
                                          
                                            <button id="<?php echo $user['company_id']; ?>" class="userinfo btn modal-trigger" class="edit" title="Edit" data-toggle="modal" data-target="#myModal" data-myvalue="trouducul" data-myvar="bisbis"><i class="material-icons">&#xE254;</i></button>
                                            <a href="<?php echo base_url(); ?>admin/delcompany/<?php echo $user['company_id']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a>
                                        </td>
                                    </tr>
                                    
                                    <?php endforeach; ?>
           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     -->











    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/filter/styless.css">

    
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>Companies List</h5></div>
                                </div>  
                            </div>
                            <div class="container">
                                <input type="search" id="search" class="form-control" placeholder="Type here to Search">
                                <div id="root"></div>
                                <div class="pages"></div>
                            </div>
                            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                            <script src="<?php echo base_url();?>assets/filter/table-sortable.js"></script>
                            <script>
                            var data = [
                                        <?php foreach ($users as $user): ?>
                                        {
                                            company_id: "<?php echo $user['company_id']; ?>",
                                            name: "<?php echo $user['name']; ?>",
                                            companyname: "<?php echo $user['companyname']; ?>",
                                            email: "<?php echo $user['email']; ?>",
                                            address: "<?php echo $user['address']; ?>",
                                            phone: "<?php echo $user['phone']; ?>",
                                            status: "<?php if($user['status'] == "disable") {echo "Disabled"; }else{echo "Active";}  ?>",
                                            register_date: "<?php echo $user['register_date']; ?>",
                                        actions: '<?php if($user['status'] == "disable") {?><a href="<?php echo base_url(); ?>admin/enbcompany/<?php echo $user['company_id']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger">Enable Account</button></a> <?php }else{?><a href="<?php echo base_url(); ?>admin/delcompany/<?php echo $user['company_id']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger">Disable Account</button></a><?php }?>'

                                        },
                                        <?php endforeach; ?>
                                        ]

                                    var columns = {
                                        'company_id': 'ID',
                                        'name': 'Name',
                                        'companyname': 'Company Name',
                                        'email': 'Email',
                                        'address': 'Address',
                                        'phone': 'Phone',
                                        'status': 'Status',
                                        'register_date': 'Registration Date',
                                        'actions': 'Actions'
                                    }
         
                            </script>
                            <script src="<?php echo base_url();?>assets/filter/script.js"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

















    

    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content modal-body">
        </div>
    </div>


    <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
    <script>
        function getCity(val) {
            $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>users/getcity/"+val,
            data:'country_name='+val,
            success: function(data){
                $("#city-list").html(data);
                //alert(data);
            }
            });
        }
    </script>


    <script type='text/javascript'>
        $(document).ready(function(){

            $('.userinfo').click(function(){
                var userid = this.id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo base_url();?>admin/ajax_edit_usermodal/"+userid,
                    data:'country_name=pakistan',
                    success: function(data){
                        $(".modal-content").html(data);
                        $('#modal1').modal('open');
                    }
                });
            });
        });
    </script>