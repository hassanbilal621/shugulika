
    <style>
        table.table tr th, table.table tr td {
            border-color: #e9e9e9;
        }   

    </style>

    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <h4 class="header2">Add Countries</h4>
                <?php if($this->session->flashdata('country_added')): ?>                                    
                    <div class="row">
                        <div id="card-alert" class="card gradient-45deg-green-teal">
                            <div class="card-content white-text">
                            <p>
                                <i class="material-icons">check</i> <?php echo $this->session->flashdata('country_added'); ?> </p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>  
                <?php if($this->session->flashdata('country_empty')): ?>                                    
                    <div class="row">
                        <div id="card-alert" class="card gradient-45deg-amber-amber">
                            <div class="card-content white-text">
                            <p>
                                <i class="material-icons">check</i> <?php echo $this->session->flashdata('country_empty'); ?> </p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>  
                <?php if($this->session->flashdata('country_deleted')): ?>                                    
                    <div class="row">
                        <div id="card-alert" class="card gradient-45deg-green-teal">
                            <div class="card-content white-text">
                            <p>
                                <i class="material-icons">check</i> <?php echo $this->session->flashdata('country_deleted'); ?> </p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>   
                <?php echo form_open_multipart('admin/addcountry'); ?>
                <div class="row">
                    <div class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="first_name" value="<?php echo set_value('geoname_id');?>" name="geoname_id" type="text" >
                                <label for="first_name" class="">Geo Name ID  <?php echo form_error('geoname_id','<span class="error">', '</span>'); ?></label>
                            </div>
                            <div class="input-field col s6">
                                <input id="first_name"  value="<?php echo set_value('continent_code');?>" name="continent_code" type="text">
                                <label for="first_name" class="">Continent Code  <?php echo form_error('continent_code','<span class="error">', '</span>'); ?></label>
                            </div>
                            <div class="row">
                            <div class="input-field col s6">
                                <input id="password6"  value="<?php echo set_value('continent_name');?>" name="continent_name" type="text">
                                <label for="password">Continent Name  <?php echo form_error('continent_name','<span class="error">', '</span>'); ?></label>
                            </div>
                            <div class="input-field col s6">
                                <input id="password6"  value="<?php echo set_value('country_name');?>" name="country_name" type="text">
                                <label for="password">Country Name  <?php echo form_error('country_name','<span class="error">', '</span>'); ?></label>
                            </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!-- <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>Countries</h5></div>
                                </div>  
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Geo Name ID</th>
                                        <th>Continent Code</th>
                                        <th>Continent Name</th>
                                        <th>Country Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
         
                                <tbody>
                                    <?php foreach ($countries as $country): ?>
                                    <tr>
                                        <td><?php echo $country['geoname_id']; ?></td>
                                        <td><?php echo $country['continent_code']; ?></td>
                                        <td><?php echo $country['continent_name']; ?></td>
                                        <td><?php echo $country['country_name']; ?></td>
                                        <td>
                                          
                                            <button id="<?php echo $country['country_name']; ?>" class="userinfo btn modal-trigger" class="edit" title="Cities" data-toggle="modal" data-target="#myModal" data-myvalue="trouducul" data-myvar="bisbis"><i class="material-icons">&#xE254;</i></button>
                                            <a href="<?php echo base_url(); ?>admin/deladmin/<?php echo $admin['id']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->


    
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>Countries</h5></div>
                                </div>  
                            </div>
                            <div class="container">
                                <input type="search" id="search" class="form-control" placeholder="Type here to Search">
                                <div id="root"></div>
                                <div class="pages"></div>
                            </div>
                            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                            <script src="../assets/filter/table-sortable.js"></script>
                            <script>
                            var data = [
                                        <?php foreach ($countries as $country): ?>
                                        {
                                            geoname: "<?php echo $country['geoname_id']; ?>",
                                            continentcode: "<?php echo $country['continent_code']; ?>",
                                            continent: "<?php echo $country['continent_name']; ?>",
                                            countryname: "<?php echo $country['country_name']; ?>",
                                            action: '<button id="<?php echo $country['country_name']; ?>" class="userinfo btn modal-trigger" class="edit" title="Cities" data-toggle="modal" data-target="#myModal" data-myvalue="trouducul" data-myvar="bisbis"><i class="material-icons">&#xE254;</i></button> <a href="<?php echo base_url(); ?>admin/deladmin/<?php echo $admin['id']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a>',
            
                                        },
                                        <?php endforeach; ?>
                                        ]

                                    var columns = {
                                        'geoname': 'Geo Name ID',
                                        'continentcode': 'Continent Code',
                                        'continent': 'Continent Name',
                                        'countryname': 'Country Name',
                                        'action': 'Actions'
                                    }
         
                            </script>
                            <script src="../assets/filter/script.js"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    

    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content modal-body">
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

    <script type='text/javascript'>
        $(document).ready(function(){
            $('.userinfo').click(function(){
                var countryname = this.id;
             
                $.ajax({
                    type: "GET",
                    url: "<?php echo base_url();?>admin/ajax_cities/"+countryname,
                    data:'country_name=pakistan',
                    success: function(data){
                        $(".modal-content").html(data);
                        $('#modal1').modal('open');
                    }
                });
            });
        });
    </script>