        <!-- START FOOTER -->
        <footer class="page-footer">
            <div class="footer-copyright">
            <div class="container">
                <span>Copyright ©
                <script type="text/javascript">
                    document.write(new Date().getFullYear());
                </script> Shugulika- Let's Become Ideal
                </span>
            </div>
            </div>
        </footer>
        <!-- END FOOTER -->
        <!-- ================================================ Scripts ================================================ -->
        <!-- jQuery Library -->

        
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/vendors/jquery-3.2.1.min.js"></script>
        <!--materialize js-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/materialize.min.js"></script>
        <!--prism-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/vendors/prism/prism.js"></script>
        <!--scrollbar-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>

        <!--jsgrid-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/vendors/jsgrid/js/db.js"></script>
        <!--data-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/vendors/jsgrid/js/jsgrid.min.js"></script>
        <!--table-jsgrid.js - Page Specific JS codes-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/scripts/table-jsgrid.js"></script>
        <!-- chartjs -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/vendors/chartjs/chart.min.js"></script>
        <!--plugins.js - Some Specific JS codes for Plugin Settings-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins.js"></script>
        <!--custom-script.js - Add your own theme custom JS-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/custom-script.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/scripts/dashboard-ecommerce.js"></script>
        <!--advanced-ui-modals.js - Some Specific JS codes -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/scripts/advanced-ui-modals.js"></script>

    </body>
</html>