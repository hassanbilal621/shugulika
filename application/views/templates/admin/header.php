<!DOCTYPE html>
<html lang="en">
  <!--================================================================================
  Item Name: Materialize - Material Design Admin Template
  Version: 4.0
  Author: PIXINVENT
  Author URL: https://themeforest.net/user/pixinvent/portfolio
  ================================================================================ -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Shugulika best place to get ideal job. ">
    <meta name="keywords" content="Shugulika, ideal , naukri, jobs, find job, find a job">
    <title>Shugulika - Admin Dashboard</title>
    <!-- Favicons-->
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/header/black.png" sizes="32x32">

    <!-- CORE CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/themes/semi-dark-menu/materialize.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/themes/semi-dark-menu/style.css" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/custom/custom.css" type="text/css" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="<?php echo base_url(); ?>assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/vendors/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/vendors/flag-icon/css/flag-icon.min.css" type="text/css" rel="stylesheet">
  </head>