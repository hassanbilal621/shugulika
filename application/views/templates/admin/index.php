    
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->

        <section id="content">
          <!--start container-->
          <div class="container">
            <!--card stats start-->
            <div id="card-stats">
              <div class="row">
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">add_shopping_cart</i>
                        <p>All Jobs</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0"><?php echo $total_jobs ?></h5>
                        <p class="no-margin">Jobs</p>
               
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">perm_identity</i>
                        <p>All Clients</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0"><?php echo $total_users ?></h5>
                        <p class="no-margin">Users</p>
        
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">perm_identity</i>
                        <p>All Companies</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0"><?php echo $total_company ?></h5>
                        <p class="no-margin">Companies</p>
          
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">timeline</i>
                        <p>Total Blogs</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0"><?php echo $total_blog ?></h5>
                        <p class="no-margin">Blogs</p>
                  
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--card stats end-->
            <!--yearly & weekly revenue chart start-->
            <!-- <div id="sales-chart">
              <div class="row">
                <div class="col s12">
                  <div id="revenue-chart" class="card">
                    <div class="card-content">
                      <h4 class="header mt-0">REVENUE FOR 2017
                        <span class="purple-text small text-darken-1 ml-1">
                          <i class="material-icons">keyboard_arrow_up</i> 15.58 %</span>
                      </h4>
                      <div class="row">
                        <div class="col s12">
                          <div class="yearly-revenue-chart">
                            <canvas id="thisYearRevenue" class="firstShadow" height="350"></canvas>
                            <canvas id="lastYearRevenue" height="350"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->
            <!--yearly & weekly revenue chart end-->
            <!-- Member online, Currunt Server load & Today's Revenue Chart start -->
      

            <!--end container-->
        </section>
        <!-- Floating Action Button -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END WRAPPER -->
    </div>
      <!-- END MAIN -->