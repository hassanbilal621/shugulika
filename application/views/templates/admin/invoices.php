
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>Pending Orders</h5></div>
                                </div>  
                            </div>
                            <table class="table table-bordered">
                            <thead>
                                    <tr>
                                        <th>Invoice ID</th>
                                        <th>Title</th>
                                        <th>Subject</th>
                                        <th>Order By </th>
                                        <th>Email</th>
                                        <th>Order ID</th>
                                        <th>Actions</th>
                                        <th>Invoice Status</th>
                                    </tr>
                                </thead>
         
                                <tbody>
                                    <?php foreach ($invoices as $invoice): ?>
                                            <tr>
                                                <td><?php echo $invoice['invoiceid']; ?></td>
                                                <td><?php echo $invoice['title']; ?></td>
                                                <td><?php echo $invoice['subject']; ?></td>
                                                <td><?php echo $invoice['name']; ?></td>
                                                <td><?php echo $invoice['email']; ?></td>
                                                <td><?php echo $invoice['orderid']; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>admin/delinvoice/<?php echo $invoice['id']; ?>" class="delete" title="Delete Invoice" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a>
                                                </td>
                                                <td>
                                                    
                                                <?php if($invoice['paystatus'] == 1){ ?>
                                                <div id="card-alert" class="card green">
                                                        <div class="card-content white-text">
                                                        <p>Paid</p>
                                                        </div>

                                                    </div>
                                                    <?php } else{
                                                        ?>
                                                        <div id="card-alert" class="card orange">
                                                            <div class="card-content white-text">
                                                        <p>Unpaid</p>
                                                        </div>

                                                    </div>
                
                                                    <?php } ?>
                                                    </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </td>
           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
