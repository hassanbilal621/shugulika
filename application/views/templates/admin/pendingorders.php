
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>Pending Orders</h5></div>
                                </div>  
                            </div>
                            <table class="table table-bordered">
                            <thead>
                                    <tr>
                                        <th>Order ID</th>
                                        <th>Order By</th>
                                        <th>Email </th>
                                        <th>Title</th>
                                        <th>Invoice ID</th>
                                        <th>Subject</th>
                                        <th>Invoice Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
         
                                <tbody>
                                    <?php foreach ($allorders as $allorder): ?>
                                        <?php 
                                            $currdate =  date("d/m/Y");
                                            $dateexpiry=date_create($allorder['expiry']);
                                            if($allorder['status'] != 1  || $currdate < date_format($dateexpiry,"d/m/Y")){ 
                                            ?>
                                            <tr>
                                                <td><?php echo $allorder['orderid']; ?></td>
                                                <td><?php echo $allorder['name']; ?></td>
                                                <td><?php echo $allorder['email']; ?></td>
                                                <td><?php echo $allorder['title']; ?></td>
                                                <td><?php echo $allorder['invoiceid']; ?></td>
                                                <td><?php echo $allorder['subject']; ?></td>
                                                <td><?php echo $allorder['paystatus']; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>admin/activeorder/<?php echo $allorder['id']; ?>" class="done" title="Click to Active Job" data-toggle="tooltip"> <button class="btn modal-trigger" style="background-color:green !important;"><i class="material-icons">done</i></button></a>
                                                    <a href="<?php echo base_url(); ?>admin/delorder/<?php echo $allorder['id']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php endforeach; ?>
           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
