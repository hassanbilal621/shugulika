
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>All Posted Jobs</h5></div>
                                </div>  
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Job ID</th>
                                        <th>Posted By</th>
                                        <th>Company Name</th>
                                        <th>Title</th>
                                        <th>Country</th>
                                        <th>City</th>
                                        <th>Salary</th>        
                                        <th>Date Posted</th>
                                        <th>Actions</th>
                                        <th>Job Status</th>
                        
                                    </tr>
                                </thead>
         
                                <tbody>
                                    <?php foreach ($postedjobs as $postedjob): ?>
                                    <tr>
                                        <td><?php echo $postedjob['jobid']; ?></td>
                                        <td><?php echo $postedjob['name']; ?></td>
                                        <td><?php echo $postedjob['companyname']; ?></td>
                                        <td><?php echo $postedjob['title']; ?></td>
                                        <td><?php echo $postedjob['country']; ?></td>
                                        <td><?php echo $postedjob['city']; ?></td>
                                        <td><?php echo $postedjob['salary']; ?></td>  
                                        <td><?php echo $postedjob['date_posted']; ?></td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>admin/deladmin/<?php echo $postedjob['id']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a>
                                        </td>
                                        <td>
                                        <?php
                
                                        if($postedjob['jobstatus'] == 1){ ?>
                                           <div id="card-alert" class="card green">
                                                <div class="card-content white-text">
                                                <p>Active</p>
                                                </div>

                                            </div>
                                            <?php } elseif($postedjob['jobstatus'] == 2){
                                                ?>
                                                <div id="card-alert" class="card orange">
                                                    <div class="card-content white-text">
                                                <p>Pending</p>
                                                </div>

                                            </div>
                                            <?php } else{
                                                ?>
                                                <div id="card-alert" class="card cyan">
                                                    <div class="card-content white-text">
                                                <p>Archived</p>
                                                </div>

                                            </div>
                                                <?php } ?>
                                        </td>
                                  
                                    </tr>
                                    
                                    <?php endforeach; ?>
           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
