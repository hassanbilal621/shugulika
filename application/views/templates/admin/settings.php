<div class="row">
    <div class="col s12 m12 l12">
        <div class="card-panel">
            <h4 class="header2">Settings</h4>
            <div class="row">
            <?php echo form_open('admin/savesettings'); ?>
                <div class="col s12">
                    <div class="row">
                    <div class="input-field col s6">
                        <input type="text" name="address"  value="<?php echo $settings['address'];?>" placeholder="Address*">
                        <label for="address">Address</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="text" name="support_email"  value="<?php echo  $settings['support_email'];?>" placeholder="Support Email*">
                        <label for="support_email">Support Email</label>
                    </div>
                    </div>


                    <div class="row">
                    <div class="input-field col s6">
                        <input type="number" name="contact_number"  value="<?php echo  $settings['contact_number'];?>" placeholder="Contact Number*">
                        <label for="contact_number">Contact Number</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="text" name="about_us"  value="<?php echo  $settings['about_us'];?>" placeholder="About Us*">
                        <label for="about_us">About Us</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s4">
                        <input type="text" name="facebook"  value="<?php echo  $settings['facebook'];?>" placeholder="Face Book*">
                        <label for="facebook">Face Book</label>
                    </div>
                    <div class="input-field col s4">
                        <input type="text" name="twitter"  value="<?php echo  $settings['twitter'];?>" placeholder="Twitter*">
                        <label for="twitter">Twitter</label>
                    </div>
                    <div class="input-field col s4">
                        <input type="text" name="linkdin"  value="<?php echo  $settings['linkdin'];?>" placeholder="Linkedin*">
                        <label for="linkdin">Linkedin</label>
                    </div>
                    </div>


                    <!-- <div class="row">
                    <div class="input-field col s12">
                        <input id="email5" type="email">
                        <label for="email">Email</label>
                    </div>
                    </div> -->

                    <div class="row">
                        <div class="input-field col s12">
                        <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save Settings
                            <i class="material-icons right">send</i>
                        </button>
                        </div>
                    </div>
                    </div>
                </div>
            <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>