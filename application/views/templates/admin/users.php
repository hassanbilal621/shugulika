
    <style>
        table.table tr th, table.table tr td {
            border-color: #e9e9e9;
        }   

    </style>

    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <h4 class="header2">Add User</h4>
                <?php if($this->session->flashdata('user_added')): ?>                                    
                    <div class="row">
                        <div id="card-alert" class="card gradient-45deg-green-teal">
                            <div class="card-content white-text">
                            <p>
                                <i class="material-icons">check</i> <?php echo $this->session->flashdata('user_added'); ?> </p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>  
                <?php if($this->session->flashdata('user_empty')): ?>                                    
                    <div class="row">
                        <div id="card-alert" class="card gradient-45deg-amber-amber">
                            <div class="card-content white-text">
                            <p>
                                <i class="material-icons">check</i> <?php echo $this->session->flashdata('user_empty'); ?> </p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>  
                <?php if($this->session->flashdata('user_updated')): ?>                                    
                    <div class="row">
                        <div id="card-alert" class="card gradient-45deg-green-teal">
                            <div class="card-content white-text">
                            <p>
                                <i class="material-icons">check</i> <?php echo $this->session->flashdata('user_updated'); ?> </p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php endif; ?> 
                <?php if($this->session->flashdata('user_deleted')): ?>                                    
                    <div class="row">
                        <div id="card-alert" class="card gradient-45deg-green-teal">
                            <div class="card-content white-text">
                            <p>
                                <i class="material-icons">check</i> <?php echo $this->session->flashdata('user_deleted'); ?> </p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>   
                <?php echo form_open_multipart('admin/adduser'); ?>
                <div class="row">
                    <div class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="first_name" value="<?php echo set_value('name');?>" name="name" type="text" >
                                <label for="first_name" class="">First Name  <?php echo form_error('name','<span class="error">', '</span>'); ?></label>
                            </div>
                            <div class="input-field col s6">
                                <input id="last_name"  value="<?php echo set_value('fname');?>" name="fname" type="text">
                                <label for="last_name" class="">Father Name  <?php echo form_error('fname','<span class="error">', '</span>'); ?></label>
                            </div>
                            </div>
                            <div class="input-field col s6">
                                <input id="first_name"  value="<?php echo set_value('phone');?>" name="phone" type="text">
                                <label for="first_name" class="">Phone Number  <?php echo form_error('phone','<span class="error">', '</span>'); ?></label>
                            </div>
                            <div class="input-field col s6">
                                <input id="first_name"  value="<?php echo set_value('username');?>" name="username" type="text">
                                <label for="first_name" class="">User Name  <?php echo form_error('username','<span class="error">', '</span>'); ?></label>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>Profile Picture</span>
                                        <input type="file" name="userfile">
                                        </div>
                                        <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="input-field col s12">
                                <input id="email5"  value="<?php echo set_value('email');?>" name="email" type="email">
                                <label for="email" class="">Email  <?php echo form_error('email','<span class="error">', '</span>'); ?></label>
                            </div>
                            </div>
                            <div class="row">
                            <div class="input-field col s12">
                                <input id="password6"  value="<?php echo set_value('password');?>" name="password" type="password">
                                <label for="password">Password  <?php echo form_error('password','<span class="error">', '</span>'); ?></label>
                            </div>
                            </div>
                            <div class="row">
                            <div class="input-field col s12">
                                <input id="password6"  value="<?php echo set_value('address');?>" name="address" type="text">
                                <label for="password">Address  <?php echo form_error('address','<span class="error">', '</span>'); ?></label>
                            </div>
                            </div>
            
                            <div class="row">
                                <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                                </div>
                            </div>
                
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!-- <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>Users List</h5></div>
                                </div>  
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Registration Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
         
                                <tbody>
                                    <?php foreach ($users as $user): ?>
                                    <tr>
                                        <td><?php echo $user['id']; ?></td>
                                        <td><?php echo $user['name']; ?></td>
                                        <td><?php echo $user['username']; ?></td>
                                        <td><?php echo $user['email']; ?></td>
                                        <td><?php echo $user['address']; ?></td>
                                        <td><?php echo $user['phone']; ?></td>
                                        <td><?php echo $user['register_date']; ?></td>
                                        <td>
                                          
                                            <button id="<?php echo $user['id']; ?>" class="userinfo btn modal-trigger" class="edit" title="Edit" data-toggle="modal" data-target="#myModal" data-myvalue="trouducul" data-myvar="bisbis"><i class="material-icons">&#xE254;</i></button>
                                            <a href="<?php echo base_url(); ?>admin/deluser/<?php echo $user['id']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a>
                                        </td>
                                    </tr>
                                    
                                    <?php endforeach; ?>
           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     -->

    



    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/flatly/bootstrap.min.css"> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/filter/styless.css">

    
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>Users List</h5></div>
                                </div>  
                            </div>
                            <div class="container">
                                <input type="search" id="search" class="form-control" placeholder="Type here to Search">
                                <div id="root"></div>
                                <div class="pages"></div>
                            </div>
                            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                            <script src="<?php echo base_url();?>assets/filter/table-sortable.js"></script>
                            <script>
                            var data = [
                                        <?php foreach ($users as $user): ?>
                                        {
                                            id: "<?php echo $user['id']; ?>",
                                            name: "<?php echo $user['name']; ?>",
                                            username: "<?php echo $user['username']; ?>",
                                            email: "<?php echo $user['email']; ?>",
                                            address: "<?php echo $user['address']; ?>",
                                            phone: "<?php echo $user['phone']; ?>",
                                            register_date: "<?php echo $user['register_date']; ?>",
                                            actions: '<button id="<?php echo $user['id']; ?>" class="userinfo btn modal-trigger" class="edit" title="Edit" data-toggle="modal" data-target="#myModal" data-myvalue="trouducul" data-myvar="bisbis"><i class="material-icons">&#xE254;</i></button> <a href="<?php echo base_url(); ?>admin/deluser/<?php echo $user['id']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a>'

                                        },
                                        <?php endforeach; ?>
                                        ]

                                    var columns = {
                                        'id': 'ID',
                                        'name': 'Name',
                                        'username': 'User Name',
                                        'email': 'Email',
                                        'address': 'Address',
                                        'phone': 'Phone',
                                        'register_date': 'Registration Date',
                                        'actions': 'Actions'
                                    }
         
                            </script>
                            <script src="<?php echo base_url();?>assets/filter/script.js"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

















    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content modal-body">
        </div>
    </div>


    <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
    <script>
        function getCity(val) {
            $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>users/getcity/"+val,
            data:'country_name='+val,
            success: function(data){
                $("#city-list").html(data);
                //alert(data);
            }
            });
        }
    </script>


    <script type='text/javascript'>
        $(document).ready(function(){

            $('.userinfo').click(function(){
                var userid = this.id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo base_url();?>admin/ajax_edit_usermodal/"+userid,
                    data:'country_name=pakistan',
                    success: function(data){
                        $(".modal-content").html(data);
                        $('#modal1').modal('open');
                    }
                });
            });
        });
    </script>