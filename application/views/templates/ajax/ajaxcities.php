<div class="col s12 m12 l12">
    <div class="card-panel">
        <?php echo form_open_multipart('admin/addcity'); ?>
            <div class="row">
                <nav>
                    <div class="nav-wrapper">
                        <a href="#" class="brand-logo center">Add new city <span id="modal-myvalue"></span> <span id="modal-myvar"></span> <span id="modal-bb"></span></a>
                    </div>
                </nav>
            </div>
            <div class="row">
                <input type="hidden" name="country_name" value="<?php echo $country_name; ?>"?>
                <div class="input-field col s6">
                    <input id="first_name" value=""  name="geoname_id" type="text" >
                    <label for="first_name" class="active">Geo Name ID  <?php echo form_error('geoname_id','<span class="error">', '</span>'); ?></label>
                </div>
                <div class="input-field col s6">
                    <input id="first_name"  value="" name="country_iso_code" type="text">
                    <label for="first_name" class="active">Continent Iso Code  <?php echo form_error('country_iso_code','<span class="error">', '</span>'); ?></label>
                </div>
            </div>
            <div class="row">
                <input type="hidden" name="adminid" value=""?>
                <div class="input-field col s6">
                    <input id="first_name" value=""  name="time_zone" type="text" >
                    <label for="first_name" class="active">Time Zone  <?php echo form_error('time_zone','<span class="error">', '</span>'); ?></label>
                </div>
                <div class="input-field col s6">
                    <input id="first_name"  value="" name="city_name" type="text">
                    <label for="first_name" class="active">City Name  <?php echo form_error('city_name','<span class="error">', '</span>'); ?></label>
                </div>
            </div>





            <div class="row">
                <div class="input-field col s12">
                <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                    <i class="material-icons right">send</i>
                </button>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>

<div class="col s12 m12 l12">
    <div class="card-panel">
        <div class="row">
            <div class="container">
                    <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-8"><h5>Cities</h5></div>
                        </div>  
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Geo Name ID</th>
                                <th>Continent Iso Code</th>
                                <th>Time Zone</th>
                                <th>City Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
    
                        <tbody>
                            <?php foreach ($cities as $city): ?>
                            <?php if (empty($city['city_name'])) { }
                                     else{        
                            ?>
                            <tr>
                                <td><?php echo $city['geoname_id']; ?></td>
                                <td><?php echo $city['country_iso_code']; ?></td>
                                <td><?php echo $city['time_zone']; ?></td>
                                <td><?php echo $city['city_name']; ?></td>
                                <td>
                                    <a href="<?php echo base_url(); ?>admin/delcity/<?php echo $city['city_name']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a>
                                </td>
                            </tr>
                                     <?php } ?>
                            <?php endforeach; ?>
    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>