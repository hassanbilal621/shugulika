<div class="jp_top_header_main_wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="jp_top_header_left_wrapper">
                        <div class="jp_top_header_left_cont">
                            <p><i class="fa fa-phone"></i> &nbsp;Phone &nbsp;<?php echo $settings['contact_number']; ?></p>
                            <p class=""><i class="fa fa-envelope"></i> &nbsp;Email :&nbsp;<a href="#"><?php echo $settings['support_email']; ?></a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="jp_top_header_right_wrapper">
                        <div class="jp_top_header_right_cont">
                            <ul>
                                <?php if($this->session->userdata('company_user_id')){ ?>
                                    <li><a href="<?php echo base_url()?>company/logout"><i class="fa fa-sign-in"></i>&nbsp; LOGOUT</a></li>
                                <?php } else {?>
                                <li><a href="<?php echo base_url(); ?>users/register"><i class="fa fa-user"></i>&nbsp; REGISTER</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="jp_top_header_right__social_cont">
                            <ul>
                                <li><a href="<?php echo $settings['facebook']; ?>"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="<?php echo $settings['twitter']; ?>"><i class="fa fa-twitter"></i></a></li>
 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Top Header Wrapper End -->
    <!-- Top Header Wrapper End -->
    <!-- Header Wrapper Start -->
    <div class="jp_top_header_img_wrapper">
        <div class="gc_main_menu_wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 hidden-xs hidden-sm full_width">
                        <div class="gc_header_wrapper">
                            <div class="gc_logo">
                                <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url().'assets/images/header/logonew.png' ?>" alt="Logo" title="Shugulika" class="img-responsive"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 full_width">
                        <div class="header-area hidden-menu-bar stick" id="sticker">
                            <!-- mainmenu start -->
                            <div class="mainmenu">
                                <div class="gc_right_menu">
                                    <ul>
                                        <li id="search_button">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_3" x="0px" y="0px" viewBox="0 0 451 451" style="enable-background:new 0 0 451 451;" xml:space="preserve"><g><path id="search" d="M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z" fill="#23c0e9"/></g></svg>
                                        </li>
                                        <li>
                                            <div id="search_open" class="gc_search_box">
                                                <input type="text" placeholder="Search here">
                                                <button><i class="fa fa-search" aria-hidden="true"></i></button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <ul class="float_left">
                                    <li class="gc_main_navigation parent"><a href="<?php echo base_url(); ?>" class="gc_main_navigation">Home</a></li>
                                    <li class="gc_main_navigation parent"><a href="<?php echo base_url(); ?>pages/services" class="gc_main_navigation">Services</a></li>
                                    <li class="gc_main_navigation parent"><a href="<?php echo base_url(); ?>pages/career" class="gc_main_navigation">Career Tips</a></li>
                                    <li class="gc_main_navigation parent"><a href="<?php echo base_url(); ?>blogs/index" class="gc_main_navigation">Blog</a></li>
                                    <li class="gc_main_navigation parent"><a href="<?php echo base_url(); ?>pages/contact" class="gc_main_navigation">Contact</a></li>
                                </ul>
                            </div>
                            <!-- mainmenu end -->
                            <!-- mobile menu area start -->
                            <header class="mobail_menu">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6">
                                            <div class="gc_logo">
                                                <a href="<?php echo base_url();?>"><img src="<?php echo base_url().'assets/images/header/logonew.png' ?>" alt="Logo" title="Grace Church"></a>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6">
                                            <div class="cd-dropdown-wrapper">
                                                <a class="house_toggle" href="#0">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.177 31.177" style="enable-background:new 0 0 31.177 31.177;" xml:space="preserve" width="25px" height="25px"><g><g><path class="menubar" d="M30.23,1.775H0.946c-0.489,0-0.887-0.398-0.887-0.888S0.457,0,0.946,0H30.23    c0.49,0,0.888,0.398,0.888,0.888S30.72,1.775,30.23,1.775z" fill="#000000"/></g><g><path class="menubar" d="M30.23,9.126H12.069c-0.49,0-0.888-0.398-0.888-0.888c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,8.729,30.72,9.126,30.23,9.126z" fill="#000000"/></g><g><path class="menubar" d="M30.23,16.477H0.946c-0.489,0-0.887-0.398-0.887-0.888c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,16.079,30.72,16.477,30.23,16.477z" fill="#000000"/></g><g><path class="menubar" d="M30.23,23.826H12.069c-0.49,0-0.888-0.396-0.888-0.887c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,23.43,30.72,23.826,30.23,23.826z" fill="#000000"/></g><g><path class="menubar" d="M30.23,31.177H0.946c-0.489,0-0.887-0.396-0.887-0.887c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.398,0.888,0.888C31.118,30.78,30.72,31.177,30.23,31.177z" fill="#000000"/></g></g></svg>
													</a>
                                                <nav class="cd-dropdown">
                                                    <h2><a href="<?php echo base_url(); ?>">Ideal<span>Naukri</span></a></h2>
                                                    <a href="#0" class="cd-close">Close</a>
                                                    <ul class="cd-dropdown-content">
                                        
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>">Home</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>pages/services">Services</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>pages/career">Career Tips</a>
                                                        </li>
                                                        <!-- .has-children -->
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>pages/contact">Contact</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>blogs/index">Blog</a>
                                                        </li>
														<li>
                                                            <a href="<?php echo base_url(); ?>users/register">Sign Up</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>users/login">Login</a>
                                                        </li>

                                                    </ul>
                                                    <!-- .cd-dropdown-content -->



                                                </nav>
                                                <!-- .cd-dropdown -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- .cd-dropdown-wrapper -->
                            </header>
                        </div>
                    </div>
                    <!-- mobile menu area end -->
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                        <div class="jp_navi_right_btn_wrapper">
                            <ul>
                                <li><a href="index"><i class="fa fa-plus-circle"></i>&nbsp; Post a job</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Wrapper End -->
	  <!-- jp Tittle Wrapper Start -->
    <!-- <div class="jp_tittle_main_wrapper">
        <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Login</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="#">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Pages</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Login</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- jp Tittle Wrapper End -->
	
	<!-- jp login wrapper start -->
	<div class="login_section">
		<!-- login_form_wrapper -->
		<div class="login_form_wrapper">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<!-- login_wrapper -->
						<h1>LOGIN TO YOUR ACCOUNT</h1>
						<div class="login_wrapper">
                            <div class="login_message">
								<?php if($this->session->flashdata('email_sent')): ?>
                                     <?php echo '<p class="alert alert-success">'.$this->session->flashdata('email_sent').'</p>'; ?>
                                <?php endif; ?>

                                <?php if($this->session->flashdata('user_registered')): ?>
                                    <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_registered').'</p>'; ?>
                                <?php endif; ?>
                                <?php if($this->session->flashdata('login_failed')): ?>
                                         <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'; ?>
                                <?php endif; ?>

                                <?php if($this->session->flashdata('company_disable')): ?>
                                         <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('company_disable').'</p>'; ?>
                                <?php endif; ?>


                            </div>
                            <br>
                            <?php echo form_open('users/login'); ?>
							<div class="formsix-pos">
								<div class="form-group i-email">
									<input type="text" name="username" class="form-control" required="" id="email2" placeholder="Username*">
								</div>
							</div>
							<div class="formsix-e">
								<div class="form-group i-password">
									<input type="password" name="password" class="form-control" required="" id="password2" placeholder="Password *">
								</div>
							</div>
							<div class="login_remember_box">
								<!-- <label class="control control--checkbox">Remember me
									<input type="checkbox">
									<span class="control__indicator"></span>
								</label> -->
								<a href="<?php base_url(); ?>forgot" class="forget_password">
									Forgot Password
								</a>
							</div>
							<div class="login_btn_wrapper">
                                <button type="submit" style="background: none; color: inherit; border: none; padding: 0;	font: inherit; outline: inherit;     color: #fff; width: 100%; height: 50px;
                                                             padding: 6px 25px; line-height: 36px; margin-bottom: 20px; text-align: center; border-radius: 8px; background: #23c0e9; font-size: 16px; border: 1px solid #23c0e9;"> LOGIN</button>
                            </div>
                            <?php echo form_close(); ?>
							<div class="login_message">
								<p>Don’t have an account ? <a href="<?php base_url(); ?>register"> Register Now </a> </p>
							</div>
						</div>
						<p>In case you are using a public/shared computer we recommend that
you logout to prevent any un-authorized access to your account</p>
						<!-- /.login_wrapper-->
					</div>
				</div>
			</div>
		</div>
		<!-- /.login_form_wrapper-->
	</div>