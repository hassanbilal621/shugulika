
<style>
.load-more__btn{



}

.jp_first_blog_post_main_wrapper
{
    margin-bottom: 25px;
}


</style>

<div class="jp_tittle_main_wrapper">
    <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Recent Blogs</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="#">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Pages</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Blog</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<div class="jp_blog_cate_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 margin_top hidden-sm hidden-xs">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_blog_right_box_search">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Search</h4>
                                </div>
                                <div class="jp_blog_right_search_wrapper">
                                    <input type="text" placeholder="Search">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_blog_right_box">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Jobs by Category</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <ul id="loadmorecategories">
      
                                        <?php foreach ($categories as $category): ?>
                                            <p>
                                            <li><i class="fa fa-caret-right"></i> <a href="<?php echo base_url().'jobs/jobcategory/'.$category['name'];?>" ><?php echo $category['name'];?> </a></li> </p>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_add_resume_wrapper jp_blog_right_box">
                                <div class="jp_add_resume_img_overlay"></div>
                                <div class="jp_add_resume_cont">
                                    <img src="images/content/resume_logo.png" alt="logo">
                                    <h4>Get Best Matched Jobs On your Email. Add Resume NOW!</h4>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;ADD RESUME</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_blog_cate_left_main_wrapper" id="loadmoreblog">
                                <?php foreach ($blog_all as $blog): ?>
                                <div id="blogcontent">
                                    <div class="jp_first_blog_post_main_wrapper">
                                        <div class="jp_first_blog_post_img">
                                            <img src="<?php echo base_url()."assets/uploads/".  $blog['thumbnail']; ?>" class="img-responsive" alt="blog_img">
                                        </div>
                                        <div class="jp_first_blog_post_cont_wrapper">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-calendar"></i> &nbsp;&nbsp;<?php $dateblog=date_create($blog['time']);
                                                                echo date_format($dateblog,"H:i A"); ?></a></li>
                                                <li><a href="#"><i class="fa fa-clone"></i> &nbsp;&nbsp;IT jobs</a></li>
                                            </ul>
                                            <h3><a href="<?php echo base_url().'blogs/view/'.$blog['id'];?>"><?php echo $blog['subject']; ?></a></h3>
                                            <p><?php echo $blog['content']; ?></p>
                                        </div>
                                        <div class="jp_first_blog_bottom_cont_wrapper">
                                            <div class="jp_blog_bottom_left_cont">
                                                <ul>
                                                    <li><img src="<?php echo base_url()."assets/uploads/".  $blog['aurthor_avt']; ?>" width="50px" alt="small_img" class="img-circle">&nbsp;&nbsp; <?php echo $blog['aurthor_name']; ?></li>
                                                </ul>
                                            </div>
                                            <div class="jp_blog_bottom_right_cont">
                                                <p class="hidden-xs"><a href="#" class="hidden-xs"><i class="fa fa-comments"></i></a></p>
                                   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                          
                                
                        </div>
                        
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>


