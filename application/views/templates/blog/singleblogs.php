<div class="jp_tittle_main_wrapper">
        <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Blogs</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="#">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Blogs</a> <i class="fa fa-angle-right"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<div class="jp_blog_cate_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 margin_top visible-lg visible-md">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_blog_right_box_search">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Search</h4>
                                </div>
                                <div class="jp_blog_right_search_wrapper">
                                    <input type="text" placeholder="Search">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_blog_right_box">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Jobs by Category</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <ul id="loadmorecategories">
      
                                        <?php foreach ($categories as $category): ?>
                                            <p>
                                            <li><i class="fa fa-caret-right"></i> <a href="<?php echo base_url().'jobs/jobcategory/'.$category['name'];?>" ><?php echo $category['name'];?> </a></li> </p>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_add_resume_wrapper jp_blog_right_box">
                                <div class="jp_add_resume_img_overlay"></div>
                                <div class="jp_add_resume_cont">
                                    <img src="images/content/resume_logo.png" alt="logo">
                                    <h4>Get Best Matched Jobs On your Email. Add Resume NOW!</h4>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;ADD RESUME</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_blog_cate_left_main_wrapper">
                                <div class="jp_first_blog_post_main_wrapper">
                                    <div class="jp_first_blog_post_img">
                                        <img src="<?php echo base_url()."assets/uploads/".$blogs['thumbnail']; ?>" class="img-responsive" alt="blog_img">
                                    </div>
                                    <div class="jp_first_blog_post_cont_wrapper">
                                        <ul>
                                            <li><a><i class="fa fa-calendar"></i> &nbsp;&nbsp; <?php 
                                            
                                            $date=date_create($blogs['time']);
                                            echo date_format($date,"d/m/Y h:i A");  
                                            
                                            ?></a></li>
                                      
                                        </ul>
                                        <h3><a><?php echo $blogs['subject']; ?></a></h3>
                                        <p><?php echo $blogs['content']; ?></p>
                                    </div>
                    
                                    <div class="jp_first_blog_bottom_cont_wrapper">
                                        <div class="jp_blog_bottom_left_cont">
                                            <ul>
                                                <li><img src="<?php echo base_url()."assets/uploads/". $blogs['aurthor_avt']; ?>" width="50px" class="img-circle">&nbsp;&nbsp; <?php echo $blogs['aurthor_name']; ?></li>
                                            </ul>
                                        </div>
                              
                                    </div>
                                </div>
                            </div>
                        </div>
        
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_blog_single_comment_main_wrapper">
                                <div class="jp_blog_single_comment_main">
                                    <h2>Comments</h2>
                                </div>



                                <div class="jp_blog_single_comment_box_wrapper" id="commentsloadmore">
                                    <div class="row">
                                    <?php foreach ($blogcomments as $blogcomment): ?>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-bottom:25px">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="jp_blog_comment_main_section_wrapper">
                                                <div class="media">
                                                        <div class="media-left">
                                                            <a href="#">
                                                                <img class="media-object" src="<?php echo base_url().'assets/uploads/'. $blogcomment['picture']; ?>" style="max-width:102px !important; max-height:102px !important" alt="...">
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                    
                                                            <h4 class="media-heading"><a> <i class="fa fa-reply"></i>&nbsp; <a> Reply by</a>&nbsp;&nbsp; <i class="fa fa-user"></i>&nbsp;<a><?php echo $blogcomment['name']; ?></a></h4>
                                                            <ul>
                                                                <li><?php $datecomment=date_create($blogcomment['datetime']);
                                                                        echo date_format($datecomment,"H:i A"); ?> </a>&nbsp;&nbsp;  <i class="fa fa-calendar"></i>&nbsp; 
                                                                <a><?php echo date_format($datecomment,"d/m/Y"); ?></a></li>
                                                            </ul>
                                                            <p><?php echo $blogcomment['comment']; ?></p>
                                            
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>  
                                       </div>
                                    <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if($this->session->userdata('user_id')){
                            ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="jp_blog_single_form_main_wrapper">
                                    <div class="jp_blog_single_form_heading">
                                        <h2>Leave A comment</h2>
                                    </div>
                                    <?php echo form_open('blogs/view/'. $blogs['id']); ?>
                                    <div class="jp_contact_form_box">
                                        <div class="row">
                    
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="jp_contact_inputs_wrapper jp_contact_inputs4_wrapper">
        
                                                    <i class="fa fa-text-height"></i><textarea rows="6" type="text" placeholder="Type Your Comment *"  name="comment" value="<?php echo set_value('name');?>"></textarea>
                                                    <?php echo form_error('name','<span class="error">', '</span>'); ?>

                                                    <input type="hidden" class="form-control" name="blogid" value="<?php echo $blogs['id']; ?>" />
                                                    <?php echo form_error('password','<span class="error">', '</span>'); ?>


                                                    <input type="hidden" class="form-control" name="user" value="<?php echo $user["name"]?>" />
                                                    <?php echo form_error('password','<span class="error">', '</span>'); ?>

                                                    <input type="hidden" class="form-control" name="useravt" value="<?php echo $user["picture"]?>" />
                                                    <?php echo form_error('password','<span class="error">', '</span>'); ?>


                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="jp_contact_form_btn_wrapper">
                                                    <ul>
                                                        <li><button type="submit" class="btn btn-primary">Submit</button></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
        
            </div>
        </div>
    </div>