    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav" class="nav-expanded nav-lock nav-collapsible">
          <div class="brand-sidebar">
            <h1 class="logo-wrapper">
              <a href="index.html" class="brand-logo darken-1">
                <span class="logo-text hide-on-med-and-down"> <img src="<?php echo base_url(); ?>assets/admin/images/logo/logo-size.png" style="width:140px !important; height:30px !important;" alt="materialize logo"></span>
              </a>
              <a href="#" class="navbar-toggler">
                <i class="material-icons">radio_button_checked</i>
              </a>
            </h1>
          </div>
          <ul id="slide-out" class="side-nav fixed leftside-navigation">
            <li class="no-padding">
              <ul class="collapsible" data-collapsible="accordion">
     
                  <li class="bold">
                    <a href="<?php echo base_url(); ?>blogger/" class="waves-effect waves-cyan">
                      <i class="material-icons">dashboard</i>
                      <span class="nav-text">All Blogs</span>
                    </a>
                  </li>

                  <li class="bold">
                    <a href="<?php echo base_url(); ?>blogger/postblog" class="waves-effect waves-cyan">
                      <i class="material-icons">alarm_add</i>
                      <span class="nav-text">Post Blog</span>
                    </a>
                  </li>
                </ul>
              </li>
            </li>
          </ul>
          <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only gradient-45deg-light-blue-cyan gradient-shadow">
            <i class="material-icons">menu</i>
          </a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->