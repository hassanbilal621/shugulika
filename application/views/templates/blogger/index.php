<section id="content">
          <!--breadcrumbs start-->
          <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
              <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
            <div class="container">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">Welcome to Blogger Dashboard!</h5>
                  <ol class="breadcrumbs">
                    <li><a >All Blogs</a></li>
      

                  </ol>
                </div>
  
              </div>
            </div>
          </div>
          <!--breadcrumbs end-->
          <!--start container-->
            <div class="container">
              <div class="section">
                <div id="blog-post-full">
                <!-- medium size post-->
                <?php foreach ($blog_all as $blog): ?>
                  <div class="card medium">
                    <div class="card-image">
                      <img src="<?php echo base_url();?>assets/uploads/<?php echo  $blog['thumbnail'];?>" alt="sample">
                      <span class="card-title"><?php echo  $blog['subject'];?></span>
          
                    </div>
                    <div class="card-content">
                      <p class="ultra-small"><?php echo  $blog['time'];?></p>
                      <p><?php echo  $blog['content'];?></p>
                    </div>
                    <div class="card-action">
                      <div class="chip">
                        <img src="<?php echo base_url();?>assets/uploads/<?php echo  $blog['aurthor_avt'];?>" alt="Contact Person" class="cyan"> <a href="#!"><?php echo  $blog['aurthor_name'];?></a>
                      </div>
                      <a href="<?php echo base_url();?>blogger/delblog/<?php echo $blog['id']; ?>" class="waves-effect waves-teal btn-flat right m-0 p-0">Delete Blog<i class="material-icons right">delete</i></a>
                    </div>
                  </div>
                  <?php endforeach; ?>
                </div>   
              </div>
            </div>
          <!--end container-->
        </section>