<div class="row">
    <div class="col s12 m12 l12">
        <div class="card-panel">
            <h4 class="header2">Post Blog</h4>
            <div class="row">
            <?php echo form_open_multipart('blogger/postblog'); ?>
                <div class="col s12">
                    <div class="row">
                    <div class="input-field col s6">
                        <input type="text" name="tittle" required>
                        <label for="address">Blog Title</label>
                    </div>
                
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="content"  required>
                            <label for="support_email">Blog Content</label>
                        </div>
                    </div>
               

                    <div class="row">
                        <div class="file-field input-field col s6">
                            <input class="file-path validate" type="text" required>
                            <div class="btn">
                            <span>Blog Thumbnail</span>
                            <input type="file" name="userfile">
                            </div>
                        </div>
        
                    </div>

                    <!-- <div class="row">
                    <div class="input-field col s12">
                        <input id="email5" type="email">
                        <label for="email">Email</label>
                    </div>
                    </div> -->

                    <div class="row">
                        <div class="input-field col s12">
                        <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Post Blog
                            <i class="material-icons right">send</i>
                        </button>
                        </div>
                    </div>
                    </div>
                </div>
            <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>