    <style scoped>

        .button-success,
        .button-error,
        .button-warning,
        .button-secondary {
            color: white;
            border-radius: 4px;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        }

        .button-success {
            background: rgb(28, 184, 65); /* this is a green */
        }

        .button-error {
            background: rgb(202, 60, 60); /* this is a maroon */
        }

        .button-warning {
            background: rgb(223, 117, 20); /* this is an orange */
        }

        .button-secondary {
            background: rgb(66, 184, 221); /* this is a light blue */
        }

    </style>
    
    <div class="jp_tittle_main_wrapper">
    <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Active Jobs</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a >Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a >Jobs</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Active Jobs</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp listing sidebar Wrapper Start -->
    <div class="jp_listing_sidebar_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                    <div class="row">
                       
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_add_resume_wrapper jp_job_location_wrapper">
                                <div class="jp_add_resume_img_overlay"></div>
                                <div class="jp_add_resume_cont">
                            
                                    <h4>Get Best Matched Jobs On your Email. Add Resume NOW!</h4>
                              
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_listing_tabs_wrapper">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="gc_causes_view_tabs_wrapper">
                                        <div class="gc_causes_view_tabs">
                                            <ul class="nav nav-pills">
                                                <li class="active"><a data-toggle="pill" href="#grid"><i class="fa fa-file"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="gc_causes_select_box_wrapper">
                             
                                    </div>
                                </div>
                    
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                    <div class="gc_causes_search_box_wrapper gc_causes_search_box_wrapper2">
                                        <div class="gc_causes_search_box">
                                            <p>Your jobs listed below</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tab-content">
                                <div id="grid" class="tab-pane fade in active">
                                    <div class="row">
                                    <?php 
                                        if(!$activejobs){
                                            echo "<br><center>No Applications Found</center>";
                                        }
                                    ?>
                                    <?php foreach ($activejobs as $activejob): ?>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="jp_job_post_main_wrapper_cont jp_job_post_grid_main_wrapper_cont">
                                                <div class="jp_job_post_main_wrapper jp_job_post_grid_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                <img src="<?php echo base_url().'assets/uploads/'.$activejob['img']; ?>" style="width:120px !important; height:120px !important;" alt="post_img" />
                                                            </div>
                                                            <div class="jp_job_post_right_cont jp_job_post_grid_right_cont">
                                                                <h4><?php echo $activejob['title'];?></h4>
                                                                <p><?php echo $activejob['name'];?></p>
                                                                <ul>
                                                                    <li><i class="fa fa-cc-paypal"></i>&nbsp; <?php echo $activejob['salary'];?> </li>
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp; <?php echo date('F j, Y',strtotime($activejob['date_posted'])); ?></li>
                                                      
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp; <?php echo strtoupper($activejob['jobcountry']);?> &nbsp;-&nbsp; <?php echo strtoupper($activejob['jobcity']);?></li>
                                                                    
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class=" jp_job_post_grid_right_btn_wrapper">
                                                                <ul>
                                                                    <li>&nbsp;</li>

                                                                    <li><button type="button" onclick="window.location.href='<?php echo base_url(); ?>company/applications/<?php echo $activejob['jobid'];?>'"class="btn btn-success">View Applications</button>  <button type="button" class="btn btn-secondary" onclick="window.location.href='<?php echo base_url(); ?>company/closejob/<?php echo $activejob['jobid'];?>'">Close</button> </li>
                                                               
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-tags"></i>Keyword :</li>
                                                        <li><a> <?php echo $activejob['keywords'];?></li>
                                             
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                    
                                    </div>
                                </div>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp listing sidebar Wrapper End -->