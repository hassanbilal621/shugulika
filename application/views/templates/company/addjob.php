	<script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
	<script>


	function getCity(val) {
		$.ajax({
		type: "GET",
		url: "<?php echo base_url(); ?>users/getcity/"+val,
		data:'country_name='+val,
		success: function(data){
			$("#city-list").html(data);
		}
		});
	}

	</script>
   
    <!-- jp Tittle Wrapper Start -->
    <div class="jp_tittle_main_wrapper">
        <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Post New Job</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="#">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Job-Posting</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp ad post Wrapper Start -->
	<div class="jp_adp_main_section_wrapper">
		<div class="container">
			<?php echo form_open_multipart('company/postjob'); ?>
			<div class="row">
			
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="jp_adp_form_heading_wrapper">
						<h2>Job Details</h2>
					</div>
					<div class="jp_adp_form_wrapper">
						<input pattern=".{3,}" title="3 characters minimum" type="text" class="form-control" name="jobtitile" required placeholder="Job Tittle" value="<?php echo set_value('jobtitile');?>"/>
						<?php echo form_error('jobtitile','<span class="error">', '</span>'); ?>
					</div>
					<div class="jp_adp_form_wrapper">
						<select name="country" id="country-list" onChange="getCity(this.value);">

							<option value disabled selected>Select Country</option>
								<?php foreach ($countries as $country): ?>

								<?php if (empty($country['country_name'])) { }
								else{        
								?>
									<option value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
								<?php }?>

										
							<?php endforeach; ?>
								
                        </select>
					</div>
	
			
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 bottom_line_Wrapper">
					<div class="jp_adp_form_heading_wrapper">
						<p>Fields with * are mandetory</p>
					</div>
					<div class="jp_adp_form_wrapper">
						<select name="category" required>
							<option value="">Select Job Category</option>
							<?php foreach ($categories as $category): ?>
								<option> <?php echo $category['name']; ?> </option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="jp_adp_form_wrapper">
						<select name="city" id="city-list">
								<option value="">Select City</option>
						</select>
					</div>

				</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
						<div class="col-lg-6 col-md-6 col-md-6 col-xs-12">
							<div class="jp_adp_form_wrapper">
								<input type="date" class="form-control" name="startdate" required placeholder="Start Date" value="<?php echo set_value('startdate');?>"/>
								<?php echo form_error('startdate','<span class="error">', '</span>'); ?>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-md-6 col-xs-12">
							<div class="jp_adp_form_wrapper">
								<input type="date" class="form-control" name="enddate" required placeholder="End Date" value="<?php echo set_value('enddate');?>"/>
								<?php echo form_error('enddate','<span class="error">', '</span>'); ?>
							</div>
						</div>
				
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
						<div class="col-lg-6 col-md-6 col-md-6 col-xs-12">
							<div class="jp_adp_form_wrapper">
								<input type="text" class="form-control" name="keyword" required placeholder="Key Word" value="<?php echo set_value('keyword');?>"/>
								<?php echo form_error('keyword','<span class="error">', '</span>'); ?>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-md-6 col-xs-12">
							<div class="jp_adp_form_wrapper">
								<input type="text" class="form-control" name="jobaddress" required placeholder="Job Address" value="<?php echo set_value('jobaddress');?>"/>
								<?php echo form_error('jobaddress','<span class="error">', '</span>'); ?>
							</div>
						</div>
				
				</div>


				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
						<div class="col-lg-6 col-md-6 col-md-6 col-xs-12">
							<div class="jp_adp_form_wrapper">
								<input type="number" class="form-control" name="salary" required placeholder="Salary" value="<?php echo set_value('salary');?>"/>
								<?php echo form_error('salary','<span class="error">', '</span>'); ?>
							</div>
						</div>

				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="jp_adp_textarea_main_wrapper">
						<textarea rows="7" placeholder="Job Description" name="jobdesc" required value="<?php echo set_value('jobdesc');?>"></textarea>
						<?php echo form_error('jobdesc','<span class="error">', '</span>'); ?>
					</div>
				</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="jp_adp_textarea_main_wrapper">
						<textarea rows="7" placeholder="Minimum qualifications" name="minqualification" required value="<?php echo set_value('minqualification');?>"></textarea>
						<?php echo form_error('minqualification','<span class="error">', '</span>'); ?>
					</div>
				</div>


				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="jp_adp_textarea_main_wrapper">
						<textarea rows="7" placeholder="How To Apply" name="howtoapply"  required value="<?php echo set_value('howtoapply');?>"></textarea>
						<?php echo form_error('howtoapply','<span class="error">', '</span>'); ?>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="jp_adp_choose_resume">
						<p>Company Post</p>
						<div class="custom-input">
						  <span><i class="fa fa-upload"></i> &nbsp;Upload Job Post</span>
						  <input type="file" name="userfile" id="resume" required />
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="jp_adp_choose_resume_bottom_btn_post">
						<ul>
							<li><button type="submit"><i class="fa fa-plus-circle"></i>&nbsp; Post a job</button></li>
						</ul>
					</div>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
    <!-- jp ad post Wrapper End -->