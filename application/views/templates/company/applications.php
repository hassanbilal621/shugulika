

<div class="jp_tittle_main_wrapper">
<div class="jp_tittle_img_overlay"></div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="jp_tittle_heading_wrapper">
                <div class="jp_tittle_heading">
                    <h2>Job  Title : <?php echo $jobinfo['title']; ?></h2>
                </div>
                <div class="jp_tittle_breadcrumb_main_wrapper">
                    <div class="jp_tittle_breadcrumb_wrapper">
                        <ul>
                            <li><a >Home</a> <i class="fa fa-angle-right"></i></li>
                            <li><a >Jobs</a> <i class="fa fa-angle-right"></i></li>
                            <li>User Applications</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- jp listing sidebar Wrapper Start -->
<div class="jp_listing_sidebar_main_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                <div class="row">
                
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_add_resume_wrapper jp_job_location_wrapper">
                            <div class="jp_add_resume_img_overlay"></div>
                            <div class="jp_add_resume_cont">
                            
                                <h4>Get Best Matched Jobs On your Email. Add Resume NOW!</h4>
                        
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_add_resume_wrapper jp_job_location_wrapper">
                
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 jp_cl_right_bar">
                <div class="row">
                    <?php 
                        if(!$applications){
                            ?>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <center>
                                    <div class="alert alert-danger fade in">

                                    <a href="#" class="close" data-dismiss="alert">&times;</a>

                                    <strong>Error!</strong> There is no applications found.

                                    </div>
                                </center>
                            </div>


                            <?php
                                }
                            ?>
                    <?php foreach ($applications as $application): ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <div class="jp_recent_resume_img_wrapper">
                                <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $application['picture']; ?>" width="70px" alt="resume_img" />
                            </div>
                            <div class="jp_recent_resume_cont_wrapper">
                                <h3><?php echo $application['name']; ?></h3>
                                <p><i class="fa fa fa-envelope-o"></i> <a href="#"><?php echo $application['email']; ?></a></p>
                            </div>
                            <div class="jp_recent_resume_btn_wrapper">
                                <ul>
                            <li> <button type="button" class="btn btn-success" onclick="window.location.href='<?php echo base_url(); ?>company/userpdf/<?php echo $application['id'];?>'"><i class="fa fa-file-pdf-o"></i></button> <?php if($application['cv_attach']){ ?> <button type="button" class="btn btn-info"  onclick="window.location.href='<?php echo base_url(); ?>assets/uploads/<?php echo $application['cv_attach'];?>'"  download="userapplication"><i class="fa fa-cloud-download"></i></button> <?php } ?> <button type="button" class="btn btn-info" onclick="window.location.href='<?php echo base_url(); ?>company/createchat/<?php echo $application['id'];?>'"><i class="fa fa fa-envelope-o"></i> </button> </li>
                                    <!-- <li><a href="#">View Profile</a></li>
                                    <li>&nbsp;</li>
                                    <li><a href="#">View Profile</a></li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_adp_choose_resume_bottom_btn_post jp_adp_choose_resume_bottom_btn_post2">
                            <ul>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
</div>
                