    <div class="jp_tittle_main_wrapper">
    <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Expired Jobs</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a >Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a >Jobs</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Expired Jobs</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    
    
    <!-- jp listing sidebar Wrapper Start -->
    <div class="jp_listing_sidebar_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                    <div class="row">
      
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_add_resume_wrapper jp_job_location_wrapper">
                                <div class="jp_add_resume_img_overlay"></div>
                                    <div class="jp_add_resume_cont">
                                        
                                        <h4>Get Best Matched Jobs On your Email. Add Resume NOW!</h4>
                                
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_listing_tabs_wrapper">

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="gc_causes_view_tabs_wrapper">
                                        <div class="gc_causes_view_tabs">
                                            <ul class="nav nav-pills">
                                                <li class="active"><a data-toggle="pill" href="#grid"><i class="fa fa-file"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="gc_causes_select_box_wrapper">
                              
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                    <div class="gc_causes_search_box_wrapper gc_causes_search_box_wrapper2">
                                        <div class="gc_causes_search_box">
                                            <p>Your jobs listed below</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tab-content">
                                <div id="grid" class="tab-pane fade in active">
                                    <div class="row">
                                    <?php 
                                        if(!$expjobs){
                                            echo "<br><center>No Expired Jobs Found</center>";
                                        }
                                    ?>
                                    <?php foreach ($expjobs as $expjob): ?>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="jp_job_post_main_wrapper_cont jp_job_post_grid_main_wrapper_cont">
                                                <div class="jp_job_post_main_wrapper jp_job_post_grid_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                <img src="<?php echo base_url().'assets/uploads/'.$expjob['img']; ?>" style="width:120px !important; height:120px !important;" alt="post_img" />
                                                            </div>
                                                            <div class="jp_job_post_right_cont jp_job_post_grid_right_cont">
                                                                <h4><?php echo $expjob['title'];?></h4>
                                                                <p><?php echo $expjob['name'];?></p>
                                                                <ul>
                                                                    <li><i class="fa fa-cc-paypal"></i>&nbsp; <?php echo $expjob['salary'];?> </li>
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp; <?php echo date('F j, Y',strtotime($expjob['date_posted'])); ?></li>
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp; <?php echo strtoupper($expjob['jobcountry']);?> &nbsp;-&nbsp; <?php echo strtoupper($expjob['jobcity']);?></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class=" jp_job_post_grid_right_btn_wrapper">
                                                                <ul>
                                                                    <li>&nbsp;</li>

                                                                    <li><button type="button" onclick="window.location.href='<?php echo base_url(); ?>company/applications/<?php echo $expjob['jobid'];?>'" class="btn btn-success">View Applications</button> </li>
                                                               
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-tags"></i>Keyword :</li>
                                                        <li><a> <?php echo $expjob['keywords'];?></li>
                                             
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                    
                                    </div>
                                </div>
                        
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- jp listing sidebar Wrapper End -->