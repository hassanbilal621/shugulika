
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if($this->session->userdata('logged_in')){
	$this->user_model->make_session();
}
?>

<!doctype html>

<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>

        <meta charset="utf-8" />
        <title>Shugulika - Company</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta name="description" content="Shugulika" />
        <meta name="keywords" content="Shugulika" />
        <meta name="author" content="" />
        <meta name="MobileOptimized" content="320" />
        <!--srart theme style -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/animate.css' ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css' ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/font-awesome.css' ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/fonts.css' ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/reset.css' ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/owl.carousel.css' ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/owl.theme.default.css' ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/flaticon.css' ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/style_II.css' ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/responsive2.css' ?>" />
        
        <!-- favicon links -->
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url().'images/header/favicon.ico' ?>" />
    </head>
    <body>
