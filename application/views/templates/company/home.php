<!-- Header Wrapper End -->
<!-- jp Tittle Wrapper Start -->
<div class="jp_tittle_main_wrapper jp_cs_tittle_main_wrapper">
   <div class="jp_tittle_img_overlay"></div>
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="jp_tittle_heading_wrapper">
               <div class="jp_tittle_heading">
                  <h2><?php echo $company['companyname']; ?></h2>
               </div>
               <div class="jp_tittle_breadcrumb_main_wrapper">
                  <div class="jp_tittle_breadcrumb_wrapper">
                     <ul>
                        <li><a href="#">Home</a> <i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Company</a> <i class="fa fa-angle-right"></i></li>
                        <li><?php echo $company['companyname']; ?></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="jp_cs_com_info_wrapper">
               <div class="jp_cs_com_info_img">
                  <img src="<?php echo base_url(); ?>assets/uploads/<?php if(isset($company['logo'])){ echo $company['logo']; }else{ echo "companyclipart.png"; } ?>" width="150px" alt="job_img">
               </div>
               <div class="jp_cs_com_info_img_cont">
                  <h2><?php echo $company['company_title']; ?></h2>
                  <br>
                  <h3><i class="fa fa-map-marker"></i> &nbsp;&nbsp;<?php echo $company['address']; ?></h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- jp Tittle Wrapper End -->
<!-- jp listing Single cont Wrapper Start -->
<div class="jp_listing_single_main_wrapper">
   <div class="container">
      <div class="row">
         <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="jp_listing_left_sidebar_wrapper">
               <div class="jp_job_des">
                  <!-- <ul>
                     <li><i class="fa fa-globe"></i>&nbsp;&nbsp; <a href="#">www.example.com</a></li>
                     <li><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; <a href="#">Download Info</a></li>
                     
                     </ul> -->
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="jp_cp_right_side_inner_wrapper">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                           <div class="jp_cp_right_side_inner_wrapper">
                              <div class="jp_pricing_form_wrapper">
                                 <?php echo form_open_multipart('company/edit/'); ?>
                                 <h2>Company Description</h2>
                                 <p><?php echo $company['company_desc']; ?></p>
                                 <br><br>
                                 <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-120">
                                       <div class="jp_pricing_inputs_wrapper">
                                          <h2 style="color:black;">Profile :</h2>
                                       </div>
                                    </div>
                                    <br><br><br>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                       <div class="jp_pricing_inputs_wrapper">
                                          <i class="fa fa-user"></i><input type="text" required name="name" value="<?php echo $company['name']; ?>" placeholder="Name*">
                                       </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                       <div class="jp_pricing_inputs_wrapper jp_pricing_inputs2_wrapper">
                                          <i class="fa fa-envelope"></i><input type="text" required name="address" value="<?php echo $company['address']; ?>" placeholder="Address*">
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="jp_pricing_inputs_wrapper jp_pricing_inputs2_wrapper">
                                          <i class="fa fa-align-justify"></i><input type="text" name="company_desc" placeholder="Company Description*" title="Company Description*"  required value="<?php echo $company['company_desc']; ?>" >
                                       </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                       <div class="jp_pricing_inputs_wrapper">
                                          <i class="fa fa-terminal"></i><input type="text" required name="companyname" value="<?php echo $company['companyname']; ?>" placeholder="Company Name*">
                                       </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                       <div class="jp_pricing_inputs_wrapper jp_pricing_inputs2_wrapper">
                                          <i class="fa fa-sitemap"></i><input type="text" required name="company_title" value="<?php echo $company['company_title']; ?>" placeholder="Company Type*">
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="jp_pricing_form_btn_wrapper">
                                          <ul>
                                             <li >
                                                <div class="custom-input">
                                                   <span><i class="fa fa-upload"></i> &nbsp;Upload Logo</span>
                                                   <input type="file" name="userfile" value="" id="resume">
                                                </div>
                                             </li>
                                             <li style="float:right;"><button type="submit"><i class="fa fa-plus-circle"></i> Submit</button></li>
                                          </ul>
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="jp_pricing_form_btn_wrapper">
                                          <ul>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                                 <?php echo form_close(); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               =
            </div>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="jp_rightside_job_categories_wrapper jp_rightside_listing_single_wrapper">
                     <div class="jp_rightside_job_categories_heading">
                        <h4>Jobs Overview</h4>
                     </div>
                     <div class="jp_jop_overview_img_wrapper">
                        <div class="jp_jop_overview_img">
                           <img src="<?php echo base_url(); ?>assets/uploads/<?php if(isset($company['logo'])){ echo $company['logo']; }else{ echo "companyclipart.png"; } ?>" width="150px" alt="job_img">
                        </div>
                     </div>
                     <div class="jp_job_listing_single_post_right_cont">
                        <div class="jp_job_listing_single_post_right_cont_wrapper">
                           <h4><?php echo $company['companyname']; ?></h4>
                           <p><?php echo $company['company_title']; ?></p>
                        </div>
                     </div>
                     <div class="jp_listing_overview_list_outside_main_wrapper">
                        <!-- <div class="jp_listing_overview_list_main_wrapper">
                           <div class="jp_listing_list_icon">
                               <i class="fa fa-map-marker"></i>
                           </div>
                           <div class="jp_listing_list_icon_cont_wrapper">
                               <ul>
                                   <li>Location:</li>
                                   <li><?php echo $company['address']; ?></li>
                           
                               </ul>
                           </div>
                           </div> -->
                        <div class="jp_listing_overview_list_main_wrapper" style="padding-top:10px;">
                           <div class="jp_listing_list_icon_cont_wrapper">
                              <ul>
                                 <li>
                                 </li>
                                 <center>
                                    <li  style="padding-bottom:10px;"><button onclick="window.location.href = '<?php echo base_url(); ?>company/activejobs';" type="button" class="btn btn-primary">
                                       Active Jobs <span class="badge badge-light"><?php if(isset($activejob)){ echo $activejob; }else{ echo "0";} ?></span>
                                       <span class="sr-only">unread messages</span>
                                       </button>
                                    </li>
                                    <li>
                                       <button onclick="window.location.href = '<?php echo base_url(); ?>company/expiredjobs';" type="button" class="btn btn-danger">
                                       Expired Jobs <span class="badge badge-light"><?php if(isset($expiredjob)){ echo $expiredjob; }else{ echo "0";} ?></span>
                                       <span class="sr-only">unread messages</span>
                                       </button>
                                    </li>
                                 </center>
                              </ul>
                           </div>
                        </div>
                        <!-- <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                           <div class="jp_listing_list_icon">
                               <i class="fa fa-th-large"></i>
                           </div>
                           <div class="jp_listing_list_icon_cont_wrapper">
                               <ul>
                                   <li>Category:</li>
                                   <li>Developer</li>
                               </ul>
                           </div>
                           </div>
                           <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                           <div class="jp_listing_list_icon">
                               <i class="fa fa-star"></i>
                           </div>
                           <div class="jp_listing_list_icon_cont_wrapper">
                               <ul>
                                   <li>Experience:</li>
                                   <li>4+ Years Experience</li>
                               </ul>
                           </div>
                           </div>
                           <div class="jp_listing_right_bar_btn_wrapper">
                           <div class="jp_listing_right_bar_btn">
                               <ul>
                                   <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;Follow Facebook</a></li>
                                   <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;Follow NOw!</a></li>
                               </ul>
                           </div>
                           </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- jp listing Single cont Wrapper End -->
