<style>
/*---------chat window---------------*/
.container{
    max-width:900px;
}
.inbox_people {
	background: #fff;
	float: left;
	overflow: hidden;
	width: 30%;
	border-right: 1px solid #ddd;
}

.inbox_msg {
	border: 1px solid #ddd;
	clear: both;
	overflow: hidden;
}

.top_spac {
	margin: 20px 0 0;
}

.recent_heading {
	float: left;
	width: 40%;
}

.srch_bar {
	display: inline-block;
	text-align: right;
	width: 60%;
	padding:
}

.headind_srch {
	padding: 10px 29px 10px 20px;
	overflow: hidden;
	border-bottom: 1px solid #c4c4c4;
}

.recent_heading h4 {
	color: #0465ac;
    font-size: 16px;
    margin: auto;
    line-height: 29px;
}

.srch_bar input {
	outline: none;
	border: 1px solid #cdcdcd;
	border-width: 0 0 1px 0;
	width: 80%;
	padding: 2px 0 4px 6px;
	background: none;
}

.srch_bar .input-group-addon button {
	background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
	border: medium none;
	padding: 0;
	color: #707070;
	font-size: 18px;
}

.srch_bar .input-group-addon {
	margin: 0 0 0 -27px;
}

.chat_ib h5 {
	font-size: 15px;
	color: #464646;
	margin: 0 0 8px 0;
}

.chat_ib h5 span {
	font-size: 13px;
	float: right;
}

.chat_ib p {
    font-size: 12px;
    color: #989898;
    margin: auto;
    display: inline-block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}

.chat_img {
	float: left;
	width: 11%;
}

.chat_img img {
	width: 100%
}

.chat_ib {
	float: left;
	padding: 0 0 0 15px;
	width: 88%;
}

.chat_people {
	overflow: hidden;
	clear: both;
}

.chat_list {
	border-bottom: 1px solid #ddd;
	margin: 0;
	padding: 18px 16px 10px;
}

.inbox_chat {
	height: 550px;
	overflow-y: scroll;
}

.active_chat {
	background: #e8f6ff;
}

.incoming_msg_img {
	display: inline-block;
	width: 6%;
}

.incoming_msg_img img {
	width: 100%;
}

.received_msg {
	display: inline-block;
	padding: 0 0 0 10px;
	vertical-align: top;
	width: 92%;
}

.received_withd_msg p {
	background: #ebebeb none repeat scroll 0 0;
	border-radius: 0 15px 15px 15px;
	color: #646464;
	font-size: 14px;
	margin: 0;
	padding: 5px 10px 5px 12px;
	width: 100%;
}

.time_date {
	color: #747474;
	display: block;
	font-size: 12px;
	margin: 8px 0 0;
}

.received_withd_msg {
	width: 57%;
}

.mesgs{
	float: left;
	padding: 30px 15px 0 25px;
	width:70%;
}

.sent_msg p {
	background:#0465ac;
	border-radius: 12px 15px 15px 0;
	font-size: 14px;
	margin: 0;
	color: #fff;
	padding: 5px 10px 5px 12px;
	width: 100%;
}

.outgoing_msg {
	overflow: hidden;
	margin: 26px 0 26px;
}

.sent_msg {
	float: right;
	width: 46%;
}

.input_msg_write input {
	background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
	border: medium none;
	color: #4c4c4c;
	font-size: 15px;
	min-height: 48px;
	width: 100%;
	outline:none;
}

.type_msg {
	border-top: 1px solid #c4c4c4;
	position: relative;
}

.msg_send_btn {
	background: #05728f none repeat scroll 0 0;
	border:none;
	border-radius: 50%;
	color: #fff;
	cursor: pointer;
	font-size: 15px;
	height: 33px;
	position: absolute;
	right: 0;
	top: 11px;
	width: 33px;
}

.messaging {
	padding: 0 0 50px 0;
}

.msg_history {
	height: 516px;
	overflow-y: auto;
}

</style>

<div class="jp_tittle_main_wrapper">
    <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xs-7 col-sm-6 col-lg-8">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading"  id="newmsg">
                            <h2>Messages</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="#">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Company</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Inbox</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container" >
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12" style="padding-top: 50px;">
        <div class="messaging">
            <div class="inbox_msg">
                <div class="inbox_people">
                <div class="headind_srch">
                    <div class="recent_heading">

					<button type="button" class="btn btn-info" onClick="window.location.href=window.location.href"><i class="fa fa-refresh"></i> </button>
              

                    </div>
                    <div class="srch_bar">
                    <div class="stylish-input-group">
						<h4>Recent</h4>
                        </div>
                    </div>
                </div>
                <div class="inbox_chat scroll">
                <?php if(empty($_GET['chatid'])){ $chatid = 0;}else{ $chatid = $_GET['chatid']; } ?>
                <?php foreach ($chats as $chat): ?>
                    <div class="chat_list <?php if($chat['chatid'] == $chatid){ ?>active_chat <?php } ?>">
                    <div class="chat_people">
                        <div class="chat_img"> <img src="<?php echo base_url().'assets/uploads/'.$chat['picture'];?>" alt="sunil"> </div>
                        <div class="chat_ib">
                        <h5><a href="<?php echo base_url().'company/inbox?chatid='.$chat['chatid'];?>#newmsg" ><?php echo $chat['name']; ?> </a><span class="chat_date">Dec 25</span></h5>
                        <p><?php echo $chat['email']; ?></p>
                        </div>
                    </div>
                    </div>
                <?php endforeach; ?>
                </div>
                </div>
		
                <div class="mesgs">
                <div class="msg_history" id="msgdiv">
                    <?php if($chatid != 0){ ?>
                    <?php foreach ($msgs as $msg): ?>
                        <?php if($msg['msgby'] == "2"){ ?>
							<div class="outgoing_msg">
                            <div class="sent_msg">
                                <p><?php echo $msg['text']; ?></p>
                                <span class="time_date"> <?php echo $msg['time']; ?>  </span>
                            </div>
                        </div>
        
                        <?php } else{ ?>
						<div class="incoming_msg">
                            <div class="incoming_msg_img"> <img src="<?php echo base_url().'assets/uploads/'.$msg['picture'];?>" alt="sunil"> </div>
                            <div class="received_msg">
                                <div class="received_withd_msg">
                                    <p><?php echo $msg['text']; ?></p>
                                    <span class="time_date"> <?php echo $msg['time']; ?> </span>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    <?php endforeach; ?>
                        <?php }else {
                            echo ' <center> <div class="alert alert-info"> <strong>Info!</strong> Inbox Empty. </div> </center>';
                        } ?>
						
                </div>
                <?php echo form_open_multipart('company/addmsg'); ?>
                <div class="type_msg">
                    <div class="input_msg_write">
                    <input type="hidden" name="chatid" value="<?php echo $chatid ?>" />
                    <input type="text" name="text" class="write_msg" placeholder="Type a message" />
                    <button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
                </div>
            </div>
        </div>
     </div>
     </div>
</div>

<script>


var objDiv = document.getElementById("msgdiv");
objDiv.scrollTop = objDiv.scrollHeight;
</script>