

<div class="jp_main_footer_img_wrapper">
        <div class="jp_newsletter_img_overlay_wrapper"></div>
        <div class="jp_newsletter_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="jp_newsletter_text">
                            <img src="<?php echo base_url(); ?>assets/images/content/news_logo.png" class="img-responsive" alt="news_logo" />
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="jp_newsletter_field">
                            <i class="fa fa-envelope-o"></i><input type="text" placeholder="Enter Your Email"><button type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jp Newsletter Wrapper End -->
        <!-- jp footer Wrapper Start -->
        <div class="jp_footer_main_wrapper">
            <div class="container">
                <div class="row">
              
                    <div class="jp_footer_three_sec_main_wrapper">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="jp_footer_first_cont_wrapper">
                                <div class="jp_footer_first_cont">
                                    <h2>Who We Are</h2>
                                    <p><?php echo $settings['about_us']; ?></p>
                                  
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="jp_footer_candidate_wrapper jp_footer_candidate_wrapper2">
                                <div class="jp_footer_candidate">
                                    <h2>For candidate</h2>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>jobs"><i class="fa fa-caret-right" aria-hidden="true"></i> All Jobs</a></li>
                                        <li><a href="<?php echo base_url(); ?>users/"><i class="fa fa-caret-right" aria-hidden="true"></i> User Dashboard</a></li>
                                        <li><a href="<?php echo base_url(); ?>jobs/myjobs"><i class="fa fa-caret-right" aria-hidden="true"></i> Users Applications</a></li>
                                        <li><a href="<?php echo base_url(); ?>users"><i class="fa fa-caret-right" aria-hidden="true"></i> My Account</a></li>
                                        <li><a href="<?php echo base_url(); ?>users/"><i class="fa fa-caret-right" aria-hidden="true"></i> View Resume</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="jp_footer_candidate_wrapper jp_footer_candidate_wrapper3">
                                <div class="jp_footer_candidate">
                                    <h2>For Employers</h2>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>company"><i class="fa fa-caret-right" aria-hidden="true"></i> Browse candidates</a></li>
                                        <li><a href="<?php echo base_url(); ?>company"><i class="fa fa-caret-right" aria-hidden="true"></i> Employer Dashboard</a></li>
                                        <li><a href="<?php echo base_url(); ?>company"><i class="fa fa-caret-right" aria-hidden="true"></i> Add Job</a></li>
                                        <li><a href="<?php echo base_url(); ?>company"><i class="fa fa-caret-right" aria-hidden="true"></i> Job Page</a></li>
                                        <li><a href="<?php echo base_url(); ?>company"><i class="fa fa-caret-right" aria-hidden="true"></i> Job Packages</a></li>
   
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="jp_footer_candidate_wrapper jp_footer_candidate_wrapper4">
                                <div class="jp_footer_candidate">
                                    <h2>Information</h2>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Home</a></li>
                                        <li><a href="<?php echo base_url(); ?>pages/services"><i class="fa fa-caret-right" aria-hidden="true"></i> Services</a></li>
                                        <li><a href="<?php echo base_url(); ?>pages/career"><i class="fa fa-caret-right" aria-hidden="true"></i> Career Tips</a></li>
                                        <li><a href="<?php echo base_url(); ?>blogs/index"><i class="fa fa-caret-right" aria-hidden="true"></i> Blog</a></li>
                                        <li><a href="<?php echo base_url(); ?>pages/contact"><i class="fa fa-caret-right" aria-hidden="true"></i> Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_bottom_footer_Wrapper">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="jp_bottom_footer_left_cont">
                                        <p>© 2019-20 Shugulika. All Rights Reserved.</p>
                                    </div>
                                    <div class="jp_bottom_top_scrollbar_wrapper">
                                        <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="jp_bottom_footer_right_cont">
                                        <ul>
                                            <li><a href="<?php echo $settings['facebook']; ?>"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="<?php echo $settings['twitter']; ?>"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="<?php echo $settings['linkdin']; ?>"><i class="fa fa-pinterest-p"></i></a></li>
        
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


       <!--Start of Tawk.to Script-->
       <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5d6a4b4b77aa790be331b8be/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    <!-- jp footer Wrapper End -->
    <!--main js file start-->
    <script src="<?php echo base_url().'assets/js/jquery_min.js' ?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js' ?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.menu-aim.js' ?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.countTo.js' ?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.inview.min.js' ?>"></script>
    <script src="<?php echo base_url().'assets/js/owl.carousel.js' ?>"></script>
    <script src="<?php echo base_url().'assets/js/modernizr.js' ?>"></script>
    <script src="<?php echo base_url().'assets/js/custom.js' ?>"></script>
    <!--main js file end-->


    <script src="<?php echo base_url(); ?>assets/js/jquery.simpleLoadMore.js"></script>
    <!-- <script src="jquery.simpleLoadMore.js"></script> -->
    <script>
        $('#loadmoreblog').simpleLoadMore({
        item: 'div',
        count: 16
        });
    </script>


<script>
        $('#commentsloadmore').simpleLoadMore({
        item: 'div',
        count: 35
        });
    </script>

</body>

</html>