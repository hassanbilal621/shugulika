<script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>


function getCity(val) {
	$.ajax({
	type: "GET",
	url: "<?php echo base_url(); ?>users/getcity/"+val,
	data:'country_name='+val,
	success: function(data){
		$("#city-list").html(data);
	}
	});
}

</script>


<script>


function getCitys(val) {
	$.ajax({
	type: "GET",
	url: "<?php echo base_url(); ?>users/getcity/"+val,
	data:'country_name='+val,
	success: function(data){
		$("#city-lists").html(data);
	}
	});
}

</script>




<div class="jp_tittle_main_wrapper">
    <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>My Profile</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="#">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Users</a> <i class="fa fa-angle-right"></i></li>
                                    <li>My Profile</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp profile Wrapper Start -->
	<div class="jp_cp_profile_main_wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="jp_cp_left_side_wrapper">
						<div class="jp_cp_left_pro_wallpaper ">
							<div class="jp_add_resume_cont">
							<img src="<?php if($user["picture"]){ echo base_url(); ?>assets/uploads/<?php echo $user["picture"]; } else{ echo base_url()."assets/uploads/avatar.jpeg";}?>" style="border-radius: 50%; width:200px;" alt="Add your profile image">
							</div>
							<h2><?php echo $user["name"]?></h2>
							<p> Email : <?php echo $user["email"];?> </p>




						
							<!--
							<ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
							</ul> -->
						</div>
		
					</div>
                    <div class="jp_add_resume_wrapper jp_job_location_wrapper jp_cp_left_ad_res">
                        <div class="jp_add_resume_img_overlay"></div>
                        <div class="jp_add_resume_cont">
            
                            <h4>View your generated resume.</h4>
                            <ul>
                                <li><a href="<?php echo base_url();?>users/resume/view"><i class="fa fa-plus-circle"></i> &nbsp;View PDF</a></li>
                            </ul>
                        </div>
                    </div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

					<div class="row">
					<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
							<div class="jp_blog_single_comment_box_wrapper">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="jp_cp_right_side_inner_wrapper">
										<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
												<div class="jp_cp_right_side_inner_wrapper">
												
												<div class="jp_pricing_form_wrapper">
													<?php echo form_open_multipart('users/edit/'.$user["id"]); ?>
														<div class="row">
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-120">
															<div class="jp_pricing_inputs_wrapper">
																<h2 style="color:black;">Profile :</h2> 
																</div>
															</div>
															<br><br><br>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper">
																	<i class="fa fa-user"></i><input type="text" required name="name" value="<?php echo $user["name"];?>" placeholder="Name*">
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper jp_pricing_inputs2_wrapper">
																	<i class="fa fa-envelope"></i><input type="text" required name="email" value="<?php echo $user["email"];?>" placeholder="Email*">
																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top:15px; padding-bottom:15px;">
																<div class="jp_pricing_inputs_wrapper">
																	<input type="text" name="address" required value="<?php echo $user["address"];?>" >
																</div>
															</div>

															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top:15px; padding-bottom:15px;">
																<div class="jp_pricing_inputs_wrapper">
																	<input type="text" name="bio" required value="<?php echo $user["bio"];?>" >
																</div>
															</div>

															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper">
																	<i class="fa fa-user"></i><input type="text" name="fname" required value="<?php echo $user["fathername"];?>" placeholder="Father Name*">
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper jp_pricing_inputs2_wrapper">
																	<i class="fa fa-envelope"></i><input type="text" required name="phone" value="<?php echo $user["phone"];?>" placeholder="Phone*">
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-top:15px; padding-bottom:15px;">
																<div class="jp_pricing_inputs_wrapper">
																	<i class="fa fa-location"></i>
																	
																	
																	<select class="form-control" name="country" id="country-list" onChange="getCity(this.value);">

																		<option value="<?php echo $user["country"];?>" selected><?php echo $user["country"];?></option>
																			<?php foreach ($countries as $country): ?>

																			<?php if (empty($country['country_name'])) { }
																			else{        
																			?>
																				<option value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
																			<?php }?>


																			<?php endforeach; ?>
																	</select>
																<!-- <input type="text" required disabled name="country" value="<?php echo $user["country"]?>" placeholder="Country*"> -->
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-top:15px; padding-bottom:15px;">
																<div class="jp_pricing_inputs_wrapper jp_pricing_inputs2_wrapper">
																	<i class="fa fa-location"></i>

																	<select name="city" id="city-list" class="form-control">
																		<option value="<?php echo $user["city"];?>" selected><?php echo $user["city"];?></option>
																	</select>

																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="jp_pricing_form_btn_wrapper">
																	<ul><li><div class="custom-input">
																				<span><i class="fa fa-upload"></i> &nbsp;Upload Picture</span>
																				<input type="file" name="userfile" value="" id="resume">
																			</div>
																		</li>
																	</ul>
																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="jp_pricing_form_btn_wrapper">
																	<ul>
																		<li><button type="submit"><i class="fa fa-plus-circle"></i> Submit</button></li>
																	</ul>
																</div>
															</div>
														</div>
													<?php echo form_close(); ?>
													</div>
										
												</div>
											</div>
				
											</div>
								
										
											</div>



											<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
												<div class="jp_cp_right_side_inner_wrapper">
												
													<div class="jp_pricing_form_wrapper" style="margin-top: 30px; margin-bottom: 30px;">
														<div class="row">                                                                                                       
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-120">
																<div class="jp_pricing_inputs_wrapper">
																	<h2 style="color:black;">Add Your Resume File :  (File : <?php if(isset($user["cv_attach"])){echo $user["cv_attach"]; }else {echo "No File Found";}?>)</h2> 
																</div>
															</div>
														</div>
													</div>
													<!--end of /.panel-group-->
												</div>
											</div>

											<div class="jp_blog_comment_main_section_wrapper" style="padding-left: 0px;padding-right: 0px;">
										
												<?php echo form_open_multipart('users/add_document'); ?>
														<div class="jp_cp_rd_wrapper">
															<ul>
														
																<li>
																<a><center> <input type="file" name="userfile" value="" id="resume"></center></a>
													
																	
																</li>
																<li><button class="home_button" type="submit"><i class="fa fa-upload"></i> &nbsp;Upload File</button></li>
															
															</ul>
														</div>
													<?php echo form_close(); ?>


											
											
                                            </div>

									
											<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
												<div class="jp_cp_right_side_inner_wrapper">
												
													<div class="jp_pricing_form_wrapper" style="margin-top: 30px; margin-bottom: 30px;">
														<div class="row">                                                                                                       
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-120">
																<div class="jp_pricing_inputs_wrapper">
																	<h2 style="color:black;">Educations :</h2> 
																</div>
															</div>
														</div>
													</div>
													<!--end of /.panel-group-->
												</div>
											</div>
											<br>
											<br>
											<hr>
                                            <div class="jp_blog_comment_main_section_wrapper" style="padding-left: 0px;padding-right: 0px;">
												<?php foreach ($educations as $education): ?>
												<div class="row">    
													<div class="jp_blog_sin_com_cont_wrapper">
														<ul>
															<li><h4><?php echo $education['degree'];?> ‎ </h4></li>
														</ul>
														<div class="grid">
    														<div class="row">	
																<ul>
										
																	<li><i class="fa fa-calendar"></i>&nbsp;&nbsp; <?php echo date("jS \of F Y", strtotime($education['startdate'])); ?>  &nbsp; to &nbsp; <?php echo date("jS \of F Y", strtotime($education['enddate'])); ?> </li>
									
																	<li><i class="fa fa-trash"></i>&nbsp;&nbsp; <a href="<?php echo base_url().'users/deleducation/'.$education['id'];?>">Delete</a></li>
																</ul>
															</div>
														</div>
																
														<p><?php echo $education['desc'];?></p>
													</div>
												</div>
												<?php endforeach; ?>
											
											
                                            </div>
											<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
												<div class="jp_cp_right_side_inner_wrapper">
												
												<div class="jp_pricing_form_wrapper">
														<div class="row">
														<?php echo form_open('users/addeducation/'.$user["id"]); ?>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-120">
															<div class="jp_pricing_inputs_wrapper">
																<h2 style="color:black;">Add Education :</h2> 
																</div>
															</div>
															<br><br><br>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper">
																	<i class="fa fa-user"></i><input required  type="text" name="degree" placeholder="Degree Tittle*">
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper jp_pricing_inputs2_wrapper">
																	<i class="fa fa-envelope"></i><input required  type="text" name="type" placeholder="College / University*">
																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top:15px; padding-bottom:15px;">
																<div class="jp_pricing_inputs_wrapper">
																	<input type="text" type="text"  required  name="desc" placeholder="Description*">
																</div>
															</div>

															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper">
																	<i class="fa fa-user"></i><input  required  type="date" name="startdate" placeholder="Start Date*">
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper jp_pricing_inputs2_wrapper">
																	<i class="fa fa-envelope"></i><input  required  type="date" name="enddate" placeholder="End Date*">
																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="jp_pricing_form_btn_wrapper">
																	<ul>
																		<li><button type="submit"><i class="fa fa-plus-circle"></i> Submit</button></li>
																	</ul>
																</div>
															</div>
														<?php echo form_close(); ?>
														</div>
													</div>
													<!--end of /.panel-group-->
												</div>
											</div>


											<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
												<div class="jp_cp_right_side_inner_wrapper">
												
													<div class="jp_pricing_form_wrapper" style="margin-top: 30px; margin-bottom: 30px;">
														<div class="row">                                                                                                       
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-120">
																<div class="jp_pricing_inputs_wrapper">
																	<h2 style="color:black;">Experiences :</h2> 
																</div>
															</div>
														</div>
													</div>
													<!--end of /.panel-group-->
												</div>
											</div>
											<br>
											<br>
											<hr>
                                            <div class="jp_blog_comment_main_section_wrapper" style="padding-left: 0px;padding-right: 0px;">
												<?php foreach ($experiences as $experience): ?>
												<div class="row">    
													<div class="jp_blog_sin_com_cont_wrapper">
														<ul>
															<li><h4><?php echo $experience['tittle'];?> ‎ </h4></li>
														</ul>
														<div class="grid">
    														<div class="row">	
																<ul>
										
																	<li><i class="fa fa-calendar"></i>&nbsp;&nbsp; <?php echo date("jS \of F Y", strtotime($experience['startdate'])); ?>  &nbsp; to &nbsp; <?php echo date("jS \of F Y", strtotime($education['enddate'])); ?> </li>
									
																	<li><i class="fa fa-trash"></i>&nbsp;&nbsp; <a href="<?php echo base_url().'users/delexperience/'.$experience['id'];?>">Delete</a></li>
																</ul>
															</div>
														</div>
																
														<p><?php echo $experience['desc'];?></p>
													</div>
												</div>
												<?php endforeach; ?>
											
											
                                            </div>
											<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
												<div class="jp_cp_right_side_inner_wrapper">
												
												<div class="jp_pricing_form_wrapper">
														<div class="row">
														<?php echo form_open('users/addexperience/'.$user["id"]); ?>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-120">
															<div class="jp_pricing_inputs_wrapper">
																<h2 style="color:black;">Add Experiences :</h2> 
																</div>
															</div>
															<br><br><br>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper">
																	<i class="fa fa-user"></i><input type="text"  required  name="tittle" placeholder="Tittle*">
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper jp_pricing_inputs2_wrapper">
																	<i class="fa fa-envelope"></i><input type="text"  required  name="company" placeholder="Company Name*">
																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top:15px; padding-bottom:15px;">
																<div class="jp_pricing_inputs_wrapper">
																	<input type="text" type="text" name="desc"  required placeholder="Description*">
																</div>
															</div>

															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper">
																	<i class="fa fa-user"></i><input type="date" required  name="startdate" placeholder="Start Date*">
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper jp_pricing_inputs2_wrapper">
																	<i class="fa fa-envelope"></i><input type="date" required  name="enddate" placeholder="End Date*">
																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="jp_pricing_form_btn_wrapper">
																	<ul>
																		<li><button type="submit"><i class="fa fa-plus-circle"></i> Submit</button></li>
																	</ul>
																</div>
															</div>
														<?php echo form_close(); ?>
														</div>
													</div>
													<!--end of /.panel-group-->
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
												<div class="jp_cp_right_side_inner_wrapper">
												
													<div class="jp_pricing_form_wrapper" style="margin-top: 30px; margin-bottom: 30px;">
														<div class="row">                                                                                                       
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-120">
																<div class="jp_pricing_inputs_wrapper">
																	<h2 style="color:black;">Projects :</h2> 
																</div>
															</div>
														</div>
													</div>
													<!--end of /.panel-group-->
												</div>
											</div>
											<br>
											<br>
											<hr>
											<div class="jp_blog_comment_main_section_wrapper" style="padding-left: 0px;padding-right: 0px;">
												<div class="bs-example" data-example-id="thumbnails-with-custom-content">
													<div class="row">
														<?php foreach ($projects as $project): ?>
															
															<div class="col-sm-6 col-md-4">
																<a href="<?php echo base_url().'users/delproject/'.$project['id'];?>"><span class="pull-right clickable close-icon" data-effect="fadeOut"><i class="fa fa-times"></i></span></a>
																<div class="thumbnail"> <img alt="100%x200" data-src="holder.js/100%x200" style="height: 200px; width: 100%; display: block;" src="<?php echo base_url();?>assets/uploads/<?php echo $project['img'];?>" data-holder-rendered="true">
																	<div class="caption">
																		<h3><?php echo $project['tittle'];?></h3>
																		<p><?php echo $project['desc'];?></p>
																	</div>
																</div>
															</div>    
									
														<?php endforeach; ?>
													</div>
												</div>
                                            </div>
											<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
												<div class="jp_cp_right_side_inner_wrapper">
												
												<div class="jp_pricing_form_wrapper">
														<div class="row">
														<?php echo form_open_multipart('users/addproject/'.$user["id"]); ?>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-120">
															<div class="jp_pricing_inputs_wrapper">
																<h2 style="color:black;">Add Projects :</h2> 
																</div>
															</div>
															<br><br><br>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper">
																	<i class="fa fa-user"></i><input type="text" required  name="tittle" placeholder="Tittle*">
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper jp_pricing_inputs2_wrapper">
																	<i class="fa fa-download"></i><input type="file"  required  name="userfile" placeholder="Image*">
																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top:15px; padding-bottom:15px;">
																<div class="jp_pricing_inputs_wrapper">
																	<input type="text" type="text" required  name="desc" placeholder="Description*">
																</div>
															</div>

															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<div class="jp_pricing_inputs_wrapper">
																	<i class="fa fa-user"></i><input type="date" required  name="date" placeholder="Start Date*">
																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="jp_pricing_form_btn_wrapper">
																	<ul>
																		<li><button type="submit"><i class="fa fa-plus-circle"></i> Submit</button></li>
																	</ul>
																</div>
															</div>
														<?php echo form_close(); ?>
														</div>
													</div>
													<!--end of /.panel-group-->
												</div>
											</div>
											
                                        </div>
                                    </div>
                                </div>
								
							</div>
							
			
						</div>

					
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- jp profile Wrapper End -->