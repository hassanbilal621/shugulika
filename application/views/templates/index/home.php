<!-- echo $this->user_model->get_empname($job['user_id']); -->


<script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>


function getCity(val) {
	$.ajax({
	type: "GET",
	url: "<?php echo base_url(); ?>users/getcity/"+val,
	data:'country_name='+val,
	success: function(data){
		$("#city-list").html(data);
	}
	});
}


</script>
       
<style>

    .jp_form_btn_wrapper li button{
        background: none;
        color: inherit;
        border: none;
        padding: 0;
        font: inherit;
        cursor: pointer;
        outline: inherit;
        
        float:left;
        width:100%;
        height:50px;
        -webkit-border-radius:15px;
        -moz-border-radius:15px;
        border-radius:15px;
        background:#23c0e9;
        color:#ffffff;
        line-height:50px;
        text-align:center;
        font-weight:bold;
        font-size:16px;
        -webkit-transition: all 0.5s;
        -o-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -moz-transition: all 0.5s;
        transition: all 0.5s;
    }
    .jp_form_btn_wrapper li button:hover{
        background:#ffffff;
        color:#000000;
        -webkit-transition: all 0.5s;
        -o-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -moz-transition: all 0.5s;
        transition: all 0.5s;
    }
    .jp_form_btn_wrapper li button i{
        padding-right:5px;
    }

</style>
       
        <div class="jp_banner_heading_cont_wrapper">
            <div class="container">
                <?php echo form_open_multipart('jobs'); ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_job_heading_wrapper">
                            <div class="jp_job_heading">
                                <h1><span>FIND</span> YOUR IDEAL JOB</h1>
                                <p>CHOOSE A JOB YOU LOVE AND YOU WILL NEVER HAVE TO WORK A DAY IN YOUR LIFE</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <!-- <div class="jp_header_form_wrapper" style="margin-top:40px; padding-bottom:0px;">                         
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="jp_form_location_wrapper">
                                    <i class="fa fa-dot-circle-o first_icon"></i><select>
                                <option>Select Location</option>
                                <option>Select Location</option>
                                <option>Select Location</option>
                                <option>Select Location</option>
                                <option>Select Location</option>
                            </select><i class="fa fa-angle-down second_icon"></i>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="jp_form_exper_wrapper">
                                    <i class="fa fa-dot-circle-o first_icon"></i><select>
                                <option>Experience</option>
                                <option>Experience</option>
                                <option>Experience</option>
                                <option>Experience</option>
                                <option>Experience</option>
                            </select><i class="fa fa-angle-down second_icon"></i>
                                </div>
                            </div>

                                
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="jp_form_location_wrapper">
                                    <i class="fa fa-dot-circle-o first_icon"></i><select>
                                <option>Select Location</option>
                                <option>Select Location</option>
                                <option>Select Location</option>
                                <option>Select Location</option>
                                <option>Select Location</option>
                            </select><i class="fa fa-angle-down second_icon"></i>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="jp_form_exper_wrapper">
                                    <i class="fa fa-dot-circle-o first_icon"></i><select>
                                <option>Experience</option>
                                <option>Experience</option>
                                <option>Experience</option>
                                <option>Experience</option>
                                <option>Experience</option>
                            </select><i class="fa fa-angle-down second_icon"></i>
                                </div>
                            </div>
                            
                            
                    
                        </div> -->
                        <div class="jp_header_form_wrapper" style="padding-bottom:30px; margin-top: 0px !important;">
                            
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="jp_form_location_wrapper">
                                    <i class="fa fa-dot-circle-o first_icon"></i>
                                    <select name="country" id="country-list" onChange="getCity(this.value);">

                                        <option value="nocountry" selected>Select Country</option>
                                        <?php foreach ($countries as $country): ?>

                                        <?php if (empty($country['country_name'])) { }
                                        else{        
                                        ?>
                                            <option value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
                                        <?php }?>

                                                
                                    <?php endforeach; ?>
                                        
                                    </select><i class="fa fa-angle-down second_icon"></i>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="jp_form_exper_wrapper">
                                    <i class="fa fa-dot-circle-o first_icon"></i>
                                    <select name="city" id="city-list">
                                            <option value="">Select City</option>
                                    </select>
                                    <i class="fa fa-angle-down second_icon"></i>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="jp_form_exper_wrapper">
                                    <input type="text" name="keyword" placeholder="Keyword e.g. (Job Title, Description, Tags)"/>
                                </div>
                            </div>

                        
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="jp_form_btn_wrapper">
                                    <ul>
                                        <input type="hidden" value="1" name="searchjob" />
                                        <li><button type="submit"><i class="fa fa-search"></i> Search</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_banner_main_jobs_wrapper">
                            <div class="jp_banner_main_jobs">
                                <ul>
                                    <li><i class="fa fa-tags"></i> Trending Keywords :</li>
                                    <li><a href="#">ui designer,</a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="jp_banner_jobs_categories_wrapper">
            <div class="container">
                <div class="jp_top_jobs_category_wrapper jp_job_cate_left_border jp_job_cate_left_border_bottom">
                    <div class="jp_top_jobs_category">
                        <i class="fa fa-code"></i>
                        <h3><a href="#">Developer</a></h3>
                        <!-- <p>(240 jobs)</p> -->
                    </div>
                </div>
                <div class="jp_top_jobs_category_wrapper jp_job_cate_left_border_bottom">
                    <div class="jp_top_jobs_category">
                        <i class="fa fa-laptop"></i>
                        <h3><a href="#">Technology</a></h3>
                        <!-- <p>(504 jobs)</p> -->
                    </div>
                </div>
                <div class="jp_top_jobs_category_wrapper jp_job_cate_left_border_bottom">
                    <div class="jp_top_jobs_category">
                        <i class="fa fa-bar-chart"></i>
                        <h3><a href="#">Accounting</a></h3>
                        <!-- <p>(2250 jobs)</p> -->
                    </div>
                </div>
                <div class="jp_top_jobs_category_wrapper jp_job_cate_left_border_res">
                    <div class="jp_top_jobs_category">
                        <i class="fa fa-medkit"></i>
                        <h3><a href="#">Medical</a></h3>
                        <!-- <p>(202 jobs)</p> -->
                    </div>
                </div>
                <div class="jp_top_jobs_category_wrapper">
                    <div class="jp_top_jobs_category">
                        <i class="fa fa-university"></i>
                        <h3><a href="#">Government</a></h3>
                        <!-- <p>(1457 jobs)</p> -->
                    </div>
                </div>
                <div class="jp_top_jobs_category_wrapper">
                    <div class="jp_top_jobs_category">
                        <i class="fa fa-th-large"></i>
                        <h3><a href="<?php echo base_url(); ?>jobs/">All Jobs</a></h3>
                        <!-- <p>(2000+ jobs)</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Wrapper End -->
    <!-- jp tittle slider Wrapper Start -->
    <!-- <div class="jp_tittle_slider_main_wrapper" style="float:left; width:100%; margin-top:30px;">
        <div class="container">
            <div class="jp_tittle_name_wrapper">
                <div class="jp_tittle_name">
                    <h3>Tranding</h3>
                    <h4>Jobs</h4>
                </div>
            </div>
            <div class="jp_tittle_slider_wrapper">
                <div class="jp_tittle_slider_content_wrapper">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="jp_tittle_slides_one">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?php echo base_url(); ?>assets/images/content/tittle_img1.png" alt="tittle_img" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4>Graphic Designer (UI / UX)</h4>
                                    <p>Webstrot Pvt. Ltdsss.</p>
                                </div>
                            </div>
                            <div class="jp_tittle_slides_one jp_tittle_slides_two">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?php echo base_url(); ?>assets/images/content/tittle_img2.png" alt="tittle_img" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4>Graphic Designer (UI / UX)</h4>
                                    <p>Webstrot Pvt. Ltd.</p>
                                </div>
                            </div>
                            <div class="jp_tittle_slides_one jp_tittle_slides_third">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?php echo base_url(); ?>assets/images/content/tittle_img3.png" alt="tittle_img" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4>Graphic Designer (UI / UX)</h4>
                                    <p>Webstrot Pvt. Ltd.</p>
                                </div>
                            </div>
                            <div class="jp_tittle_slides_one jp_tittle_slides_third">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?php echo base_url(); ?>assets/images/content/tittle_img3.png" alt="tittle_img" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4>Graphic Designer (UI / UX)</h4>
                                    <p>Webstrot Pvt. Ltd.</p>
                                </div>
                            </div>

                            <div class="jp_tittle_slides_one jp_tittle_slides_third">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?php echo base_url(); ?>assets/images/content/tittle_img3.png" alt="tittle_img" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4>Graphic Designer (UI / UX)</h4>
                                    <p>Webstrot Pvt. Ltd.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="jp_tittle_slides_one">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?php echo base_url(); ?>assets/images/content/tittle_img2.png" alt="tittle_img" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4>Graphic Designer (UI / UX)</h4>
                                    <p>Webstrot Pvt. Ltd.</p>
                                </div>
                            </div>
                            <div class="jp_tittle_slides_one jp_tittle_slides_two">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?php echo base_url(); ?>assets/images/content/tittle_img3.png" alt="tittle_img" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4>Graphic Designer (UI / UX)</h4>
                                    <p>Webstrot Pvt. Ltd.</p>
                                </div>
                            </div>
                            <div class="jp_tittle_slides_one jp_tittle_slides_third">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?php echo base_url(); ?>assets/images/content/tittle_img1.png" alt="tittle_img" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4>Graphic Designer (UI / UX)</h4>
                                    <p>Webstrot Pvt. Ltd.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="jp_tittle_slides_one">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?php echo base_url(); ?>assets/images/content/tittle_img3.png" alt="tittle_img" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4>Graphic Designer (UI / UX)</h4>
                                    <p>Webstrot Pvt. Ltd.</p>
                                </div>
                            </div>
                            <div class="jp_tittle_slides_one jp_tittle_slides_two">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?php echo base_url(); ?>assets/images/content/tittle_img1.png" alt="tittle_img" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4>Graphic Designer (UI / UX)</h4>
                                    <p>Webstrot Pvt. Ltd.</p>
                                </div>
                            </div>
                            <div class="jp_tittle_slides_one jp_tittle_slides_third">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?php echo base_url(); ?>assets/images/content/tittle_img2.png" alt="tittle_img" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4>Graphic Designer (UI / UX)</h4>
                                    <p>Webstrot Pvt. Ltd.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- jp tittle slider Wrapper End -->
    <!-- jp first sidebar Wrapper Start -->
    <div class="jp_first_sidebar_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_hiring_slider_main_wrapper">
                                <div class="jp_hiring_heading_wrapper">
                                    <h2>Top Hiring Companies</h2>
                                </div>
                                <div class="jp_hiring_slider_wrapper">
                                    <div class="owl-carousel owl-theme">

                                        <?php foreach($companies as $company): ?>

                                        <div class="item">
                                            <div class="jp_hiring_content_main_wrapper">
                                                <div class="jp_hiring_content_wrapper">
                                                    <img src="<?php echo base_url(); ?>assets/images/content/hiring_img1.png" alt="hiring_img" />
                                                    <h4><?php echo $company['name']; ?></h4>
                                                    <p>(<?php echo $company['country']; ?>)</p>
                                                    <ul>
                                                        <li><a href="#">4 Opening</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="cc_featured_product_main_wrapper">
                                <div class="jp_hiring_heading_wrapper jp_job_post_heading_wrapper">
                                    <h2>Recent Jobs</h2>
                                </div>
            
                            </div>
                            <div class="">
                                <div id="grid" class="tab-pane fade in active">
                                    <div class="ss_featured_products">

                                    <br>

                                    <?php 
                                        if(!$jobs)
                                        {
                                            echo "No jobs have been posted";
                                        }
                                        ?>
                                        <?php foreach ($jobs as $job): ?>

                                            <?php 
                                            ?>

                                            <div class="jp_job_post_main_wrapper_cont jp_job_post_grid_main_wrapper_cont">
                                                <div class="jp_job_post_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                
                                                                <img style="width:120px !important; height:120px !important;" src="<?php echo base_url().'assets/uploads/'.$job['img']; ?>" alt="post_img">
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <h4><?php echo $job['title'];?></h4>
                                                                <p><a href="<?php echo base_url()."company/view/". $job['company_id']; ?>"> <?php echo $job['companyname']; ?> </a></p>
                                                        
                                                                <ul>
                                                                    <li><i class="fa fa-dollar"></i>&nbsp; <?php echo $job['salary']; ?> .</li>
                                                                    <li><i class="fa fa-cc-date"></i>&nbsp; <?php echo date('F j, Y',strtotime($job['date_posted'])); ?></li>
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp; <?php echo strtoupper($job['jobcountry']);?> &nbsp;-&nbsp; <?php echo strtoupper($job['jobcity']);?></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="jp_job_post_right_btn_wrapper">
                                                                <ul>
                                                                    <li></li>
                                                                    <li><a href="<?php echo base_url()."jobs/".$job['jobnum']; ?>">View</a></li>
                                                                    <li></li>
                                                              
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-tags"></i>Keywords :</li>
                                                        <li><a href="#"> <?php echo $job['keywords'];?>  </a></li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <?php endforeach; ?>

        
                                    </div>
                                </div>
                            </div>
                        </div>
           
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="jp_first_right_sidebar_main_wrapper">
                        <div class="row">
  
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="jp_add_resume_wrapper">
                                    <div class="jp_add_resume_img_overlay"></div>
                                    <div class="jp_add_resume_cont">
                        
                                        <h4>Get Best Matched Jobs On your Email. Add Resume NOW!</h4>
                                        <ul>
                                            <li><a href="<?php echo base_url(); ?>users/login"><i class="fa fa-plus-circle"></i> &nbsp;Create Resume</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="jp_rightside_job_categories_wrapper">
                                    <div class="jp_rightside_job_categories_heading">
                                        <h4>Jobs by Category</h4>
                                    </div>
                                    <div class="jp_rightside_job_categories_content">
                                        <ul>
                                            <?php foreach ($categories as $category): ?>
                                            <li><i class="fa fa-caret-right"></i> <a href="<?php echo base_url().'jobs/jobcategory/'.$category['name'];?>"><?php echo $category['name'];?> </a></li>
                                            <?php endforeach; ?>
                                            <li></li>
                                        
                                        </ul>
                                    </div>
                                </div>
                            </div>
                  
                             
                              
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp first sidebar Wrapper End -->
    <!-- jp counter Wrapper Start -->
    <div class="jp_counter_main_wrapper">

        <div class="container">
            <div class="gc_counter_cont_wrapper">
                <div class="count-description">
                    <span class="timer">2540</span><i class="fa fa-plus"></i>
                    <h5 class="con1">Jobs Available</h5>
                </div>
            </div>
            <div class="gc_counter_cont_wrapper2">
                <div class="count-description">
                    <span class="timer">7325</span><i class="fa fa-plus"></i>
                    <h5 class="con2">Members</h5>
                </div>
            </div>
            <div class="gc_counter_cont_wrapper3">
                <div class="count-description">
                    <span class="timer">1924</span><i class="fa fa-plus"></i>
                    <h5 class="con3">Resumes</h5>
                </div>
            </div>
            <div class="gc_counter_cont_wrapper4">
                <div class="count-description">
                    <span class="timer">4275</span><i class="fa fa-plus"></i>
                    <h5 class="con4">Company</h5>
                </div>
            </div>
        </div>
        <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_register_section_main_wrapper">
                                <div class="jp_regis_left_side_box_wrapper">
                                    <div class="jp_regis_left_side_box">
                                        <img src="<?php echo base_url(); ?>assets/images/content/regis_icon.png" alt="icon" />
                                        <h4>I’m an EMPLOYER</h4>
                                        <p>Signed in companies are able to post new<br> job offers, searching for candidate...</p>
                                        <ul>
                                            <li><a href="<?php echo base_url(); ?>users/register"><i class="fa fa-plus-circle"></i> &nbsp;REGISTER AS EMPLOYER</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="jp_regis_right_side_box_wrapper">
                                    <div class="jp_regis_right_img_overlay"></div>
                                    <div class="jp_regis_right_side_box">
                                        <img src="<?php echo base_url(); ?>assets/images/content/regis_icon2.png" alt="icon" />
                                        <h4>I’m an candidate</h4>
                                        <p>Signed in as candidate are able to get new<br> jobs .</p>
                                        <ul>
                                            <li><a href="<?php echo base_url(); ?>users/register?ascompany"><i class="fa fa-plus-circle"></i> &nbsp;REGISTER AS CANDIDATE</a></li>
                                        </ul>
                                    </div>
                                    <div class="jp_regis_center_tag_wrapper">
                                        <p>OR</p>
                                    </div>
                                </div>

                            </div>
                  </div>
             </div>
    </div>
    <!-- jp counter Wrapper End -->
    <!-- jp Best deals Wrapper Start -->
    <!-- jp Best deals Wrapper End -->
    <!-- jp Client Wrapper Start -->
    <div class="jp_client_slider_main_wrapper" style="padding-bottom:50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_first_client_slider_wrapper">
                        <div class="jp_first_client_slider_img_overlay"></div>
                        <div class="jp_client_heading_wrapper">
                            <h2>What Clients Say?</h2>
                        </div>
                        <div class="jp_client_slider_wrapper">
                            <div class="owl-carousel owl-theme">
                                <div class="item">
                                    <div class="jp_client_slide_show_wrapper">
                                        <div class="jp_client_slider_img_wrapper">
                                            <img src="<?php echo base_url(); ?>assets/images/content/client_slider_img.jpg" alt="client_img" />
                                        </div>
                                        <div class="jp_client_slider_cont_wrapper">
                                            <p>“Sollicitudin, lorem quis bibendum en auctor, aks consequat ipsum, nec a sagittis sem nibh id elit. Duis sed odo nibh vulputate Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin”</p>
                                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i><span>~ Jeniffer Doe &nbsp;<b>(Ui Designer)</b></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="jp_client_slide_show_wrapper">
                                        <div class="jp_client_slider_img_wrapper">
                                            <img src="<?php echo base_url(); ?>assets/images/content/client_slider_img.jpg" alt="client_img" />
                                        </div>
                                        <div class="jp_client_slider_cont_wrapper">
                                            <p>“Sollicitudin, lorem quis bibendum en auctor, aks consequat ipsum, nec a sagittis sem nibh id elit. Duis sed odo nibh vulputate Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin”</p>
                                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i><span>~ Jeniffer Doe &nbsp;<b>(Ui Designer)</b></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="jp_client_slide_show_wrapper">
                                        <div class="jp_client_slider_img_wrapper">
                                            <img src="<?php echo base_url(); ?>assets/images/content/client_slider_img.jpg" alt="client_img" />
                                        </div>
                                        <div class="jp_client_slider_cont_wrapper">
                                            <p>“Sollicitudin, lorem quis bibendum en auctor, aks consequat ipsum, nec a sagittis sem nibh id elit. Duis sed odo nibh vulputate Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin”</p>
                                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i><span>~ Jeniffer Doe &nbsp;<b>(Ui Designer)</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="jp_best_deal_main_wrapper">
        <div class="container">
            <div class="row">

              
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-notification"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Job Notifications</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-wallet"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Essay Pay Money</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-people"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Happy Support</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-notification"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Job Notifications</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-wallet"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Essay Pay Money</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-people"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Happy Support</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- jp Client Wrapper End -->
    <!-- jp pricing Wrapper Start -->
    <!-- <div class="jp_pricing_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="pricing_box1_wrapper pricing_border_box1_wrapper">
                        <div class="box1_heading_wrapper">
                            <h4>Basic Plan</h4>
                        </div>
                        <div class="price_box1_wrapper">
                            <div class="price_box1">
                                <h1>$<span>29</span></h1>
                            </div>
                        </div>
                        <div class="pricing_cont_wrapper">
                            <div class="pricing_cont">
                                <ul>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;5 Jobs Posting</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;2 Featured jobs</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;1 Renew Jobs</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;10 Days Duration</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;Email Alert</li>
                                </ul>
                            </div>
                        </div>
                        <div class="pricing_btn_wrapper">
                            <div class="pricing_btn1">
                                <ul>
                                    <li><a href="#"><i class="fa fa-plus-circle"></i> Get Started</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="jp_pricing_label_wrapper">
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="pricing_box1_wrapper pricing_border_box2_wrapper">
                        <div class="box1_heading_wrapper box2_heading_wrapper">
                            <h4>Premium Plan</h4>
                        </div>
                        <div class="price_box1_wrapper">
                            <div class="price_box2">
                                <h1>$<span>49</span></h1>
                            </div>
                        </div>
                        <div class="pricing_cont_wrapper">
                            <div class="pricing_cont">
                                <ul>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;5 Jobs Posting</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;2 Featured jobs</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;1 Renew Jobs</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;10 Days Duration</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;Email Alert</li>
                                </ul>
                            </div>
                        </div>
                        <div class="pricing_btn_wrapper">
                            <div class="pricing_btn2">
                                <ul>
                                    <li><a href="#"><i class="fa fa-plus-circle"></i> Get Started</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="jp_pricing_label_wrapper">
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="pricing_box1_wrapper pricing_border_box3_wrapper">
                        <div class="box1_heading_wrapper box3_heading_wrapper">
                            <h4>Advanced Plan</h4>
                        </div>
                        <div class="price_box1_wrapper">
                            <div class="price_box3">
                                <h1>$<span>79</span></h1>
                            </div>
                        </div>
                        <div class="pricing_cont_wrapper">
                            <div class="pricing_cont">
                                <ul>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;5 Jobs Posting</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;2 Featured jobs</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;1 Renew Jobs</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;10 Days Duration</li>
                                    <li><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;Email Alert</li>
                                </ul>
                            </div>
                        </div>
                        <div class="pricing_btn_wrapper">
                            <div class="pricing_btn3">
                                <ul>
                                    <li><a href="#"><i class="fa fa-plus-circle"></i> Get Started</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="jp_pricing_label_wrapper">
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     -->