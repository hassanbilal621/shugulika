<br>
<br><br>
<div class="jp_best_deal_main_wrapper">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-magnifying-glass"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Search a Jobs</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-users"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Apply a Job</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-shield"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Job Security</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-notification"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Job Notifications</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-wallet"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Essay Pay Money</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-people"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Happy Support</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>