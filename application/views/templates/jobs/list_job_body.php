<style>
      .load-more__btn {
        margin: 0.5rem 0;
        display: inline-block;
        text-decoration: none;
        padding: 15px;
        border-bottom: 3px solid #2C3E50;
        border-radius: 3px;
        background-color: #f7f7f7;
      }

</style>

<?php 
$searchedjobs= 0;

foreach ($jobs as $job): 
$searchedjobs++;
endforeach; ?>


<div class="jp_listing_sidebar_main_wrapper">
<div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_listing_heading_wrapper">
                        <h2>We found <span><?php echo $searchedjobs ?> Jobs</span> Matches for you.</h2>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Jobs by Category</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <ul id="loadmorecategories">
      
                                        <?php foreach ($categories as $category): ?>
                                            <p>
                                            <li><i class="fa fa-caret-right"></i> <a href="<?php echo base_url().'jobs/jobcategory/'.$category['name'];?>" ><?php echo $category['name'];?> </a></li> </p>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="margin-top:30px;">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tab-content">
                                <div id="list" class="tab-pane fade active in">
                                    <div class="row">
                                        <?php if(!$jobs){ ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                                            <div class="pager_wrapper gc_blog_pagination">
                                                <ul class="pagination">
                                                    <li><p>
                                                        <?php
                                                            echo "No jobs have been posted";
                                
                                                        ?><p>
                                                        </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php } ?>
                                
                                        <ul id="loadmorejob">
                                        <?php foreach ($jobs as $job): ?>

                                        <?php 
                                        ?>
                                      
                                        <li>
                                            <div id="loadcontent" class="jp_job_post_main_wrapper_cont jp_job_post_grid_main_wrapper_cont">
                                                <div class="jp_job_post_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                
                                                                <img src="<?php echo base_url().'assets/uploads/'.$job['img']; ?>" style="width:120px !important; height:120px !important;" alt="post_imgs">
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <h4><?php echo $job['title'];?></h4>
                                                                <p><a href="<?php echo base_url().'company/view/'.$job['company_id']; ?>"><?php echo $job['companyname'];?></a></p>
                                                                <ul>
                                                                    <li><i class="fa fa-cc-paypal"></i>&nbsp; <?php echo $job['salary'];?></li>
                                                                    <li><i class="fa fa-cc-date"></i>&nbsp; <?php echo date('F j, Y',strtotime($job['date_posted'])); ?></li>
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp; <?php echo strtoupper($job['jobcountry']);?> &nbsp;-&nbsp; <?php echo strtoupper($job['jobcity']);?></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="jp_job_post_right_btn_wrapper">
                                                                <ul>
                                                                    <li>
                                                                    <!-- <a href="#"><i class="fa fa-heart-o"></i></a> -->
                                                                    </li>
                                                                    <li><a href="<?php echo base_url()."jobs/".$job['jobid']; ?>">View</a></li>
                                                                    <li></li>
                                                                
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                    
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-tags"></i>Keywords :</li>
                                                        <li><a href="#"> <?php echo $job['category'];?></a></li>
                                
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                     
                                        <?php endforeach; ?>
                                        </ul>

                                        
                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
</div>        

<script src="<?php echo base_url(); ?>assets/js/jquery.simpleLoadMore.js"></script>
  <!-- <script src="jquery.simpleLoadMore.js"></script> -->
  <script>
    $('#loadmorejob').simpleLoadMore({
      item: 'div',
      count: 36
    });
  </script>


<!-- <script>
    $('#loadmorecategories').simpleLoadMore({
      item: 'p',
      count: 5
    });
  </script> -->