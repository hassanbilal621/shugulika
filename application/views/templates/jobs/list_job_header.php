<script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>


function getCity(val) {
	$.ajax({
	type: "GET",
	url: "<?php echo base_url(); ?>users/getcitys/"+val,
	data:'country_name='+val,
	success: function(data){
		$("#city-list").html(data);
	}
	});
}

</script>


<div class="jp_bottom_footer_Wrapper_header_img_wrapper">
    <div class="jp_slide_img_overlay"></div>
    <div class="jp_banner_heading_cont_wrapper">
    <div class="container">
            <?php echo form_open_multipart('jobs'); ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_job_heading_wrapper">
                        <div class="jp_job_heading">
                            <h1><span>3,000+</span> Browse Jobs</h1>
                            <p>Find Jobs, Employment & Career Opportunities</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- <div class="jp_header_form_wrapper" style="margin-top:40px; padding-bottom:0px;">                         
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="jp_form_location_wrapper">
                                <i class="fa fa-dot-circle-o first_icon"></i><select>
                            <option>Select Location</option>
                            <option>Select Location</option>
                            <option>Select Location</option>
                            <option>Select Location</option>
                            <option>Select Location</option>
                        </select><i class="fa fa-angle-down second_icon"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="jp_form_exper_wrapper">
                                <i class="fa fa-dot-circle-o first_icon"></i><select>
                            <option>Experience</option>
                            <option>Experience</option>
                            <option>Experience</option>
                            <option>Experience</option>
                            <option>Experience</option>
                        </select><i class="fa fa-angle-down second_icon"></i>
                            </div>
                        </div>

                            
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="jp_form_location_wrapper">
                                <i class="fa fa-dot-circle-o first_icon"></i><select>
                            <option>Select Location</option>
                            <option>Select Location</option>
                            <option>Select Location</option>
                            <option>Select Location</option>
                            <option>Select Location</option>
                        </select><i class="fa fa-angle-down second_icon"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="jp_form_exper_wrapper">
                                <i class="fa fa-dot-circle-o first_icon"></i><select>
                            <option>Experience</option>
                            <option>Experience</option>
                            <option>Experience</option>
                            <option>Experience</option>
                            <option>Experience</option>
                        </select><i class="fa fa-angle-down second_icon"></i>
                            </div>
                        </div>
                        
                        
                
                    </div> -->
                    <div class="jp_header_form_wrapper" style="padding-bottom:30px; margin-top: 0px !important;">
                        
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="jp_form_location_wrapper">
                                <i class="fa fa-dot-circle-o first_icon"></i>
                                <select name="country" id="country-list" onChange="getCity(this.value);">

                                <option value="nocountry" selected>Select Country</option>
                                    <?php foreach ($countries as $country): ?>

                                    <?php if (empty($country['country_name'])) { }
                                    else{        
                                    ?>
                                        <option value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
                                    <?php }?>

                                            
                                <?php endforeach; ?>
                                    
                                </select>
                            <i class="fa fa-angle-down second_icon"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="jp_form_exper_wrapper">
                                <i class="fa fa-dot-circle-o first_icon"></i>
                                <select name="city" id="city-list">
                                        <option value="">Select City</option>
                                </select>
                                <i class="fa fa-angle-down second_icon"></i>
                            </div>
                        </div>
              
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="jp_form_exper_wrapper">
                                    <input type="text" name="keyword" placeholder="Keyword e.g. (Job Title, Description, Tags)"/>
                                </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <div class="jp_form_btn_wrapper">
                                <ul>
                                    <input type="hidden" value="1" name="searchjob" />
                                    <li><button type="submit"><i class="fa fa-search"></i> Search</button></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_banner_main_jobs_wrapper">
                        <div class="jp_banner_main_jobs">
                            <ul>
                                <li><i class="fa fa-tags"></i> Trending Keywords :</li>
                                <li><a href="#">Engineer, Developer , Graphic Designer , Seo Expert ....</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>
</div>