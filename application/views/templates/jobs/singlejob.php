        
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="padding-top:60px !important; padding-bottom:60px !important; ">
                <div class="jp_listing_left_sidebar_wrapper">
                    <div class="jp_job_des">
                        <h2>Job Description</h2>
                        <p>"<?php echo $job["description"]?></p>
                    </div>
    
                    <div class="jp_job_res jp_job_qua">
                        <h2>Minimum qualifications</h2>
                        <p><?php echo $job["qualification"]?>.</p>
                    </div>
                    <div class="jp_job_apply">
                        <h2>How To Apply</h2>
                        <p><?php echo $job["howtoapply"]?>.</p>
                    </div>
                    <!-- <div class="jp_job_map">
                        <h2>Loacation</h2>
                        <div id="map" style="width: 100%; float: left; height: 300px; position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-err-container"><div class="gm-err-content"><div class="gm-err-icon"><img src="https://maps.gstatic.com/mapfiles/api-3/images/icon_error.png" draggable="false" style="-moz-user-select: none;"></div><div class="gm-err-title">Oops! Something went wrong.</div><div class="gm-err-message">This page didn't load Google Maps correctly. See the JavaScript console for technical details.</div></div></div></div></div>
                    </div> -->
                </div>
                <!-- <div class="jp_listing_left_bottom_sidebar_wrapper">
                    <div class="jp_listing_left_bottom_sidebar_social_wrapper">
                        <ul class="hidden-xs">
                            <li>SHARE :</li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li class="hidden-xs"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li class="hidden-xs"><a href="#"><i class="fa fa-vimeo"></i></a></li>
                        </ul>
                    </div>
                </div> -->
                <div class="jp_listing_left_bottom_sidebar_key_wrapper">
                    <ul>
                        <li><i class="fa fa-tags"></i>Keywords :</li>
                        <li><?php echo $job["keywords"]?></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:60px !important;">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_rightside_job_categories_wrapper jp_rightside_listing_single_wrapper">
                            <div class="jp_rightside_job_categories_heading">
                                <h4>Job Overview</h4>
                            </div>
                            <div class="jp_jop_overview_img_wrapper">
                                <div class="jp_jop_overview_img">
                                    <img src="<?php echo base_url().'assets/uploads/'.$job['img']; ?>"  style="width:300px !important; height:300px !important;" alt="post_img">
                                </div>
                            </div>
                            <div class="jp_job_listing_single_post_right_cont">
                                <div class="jp_job_listing_single_post_right_cont_wrapper">
                                    <h4><?php echo $job["name"]?></h4>
                                    <p><?php echo $job["companyname"]?>.</p>
                                </div>
                            </div>
        
                            <div class="jp_listing_overview_list_outside_main_wrapper">
                                <div class="jp_listing_overview_list_main_wrapper">
                                    <div class="jp_listing_list_icon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <div class="jp_listing_list_icon_cont_wrapper">
                                        <ul>
                                            <li>Date Posted:</li>
                                            <li><?php echo $job["date_posted"]?></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                    <div class="jp_listing_list_icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <div class="jp_listing_list_icon_cont_wrapper">
                                        <ul>
                                            <li>Location:</li>
                                            <li><?php echo strtoupper($job["jobaddress"]);?></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                    <div class="jp_listing_list_icon">
                                        <i class="fa fa-info-circle"></i>
                                    </div>
                                    <div class="jp_listing_list_icon_cont_wrapper">
                                        <ul>
                                            <li>Job Title:</li>
                                            <li><?php echo $job["title"]?></li>
                                        </ul>
                                    </div>
                                </div>
 
                                <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                    <div class="jp_listing_list_icon">
                                        <i class="fa fa-money"></i>
                                    </div>
                                    <div class="jp_listing_list_icon_cont_wrapper">
                                        <ul>
                                            <li>Salary:</li>
                                            <li>$ <?php echo $job["salary"]?></li>
                                        </ul>
                                    </div>
                                </div>
   
                                <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                    <div class="jp_listing_list_icon">
                                        <i class="fa fa-star"></i>
                                    </div>
                                    <div class="jp_listing_list_icon_cont_wrapper">
                                        <ul>
                                            <li>Country / City:</li>
                                            <li><?php echo strtoupper($job["jobcountry"]);?> / <?php echo strtoupper($job["jobcity"]);?></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php if($this->session->flashdata('application_exists')){ ?>
                                <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                   
                                    <div class="jp_listing_list_icon_cont_wrapper">
                                        <ul>                  		
                                            <?php echo '<p class="alert alert-success">'.$this->session->flashdata('application_exists').'</p>'; ?>
                                        </ul>
                                    </div>
                                </div>
                                <?php } else{?>
                         
                            
                                <div class="jp_listing_right_bar_btn_wrapper">
                                    <div class="jp_listing_right_bar_btn">
                                        <ul>
                                            <li><a href="<?php echo base_url().'jobs/applynow/'.$job["jobid"]; ?>"><i class="fa fa-plus-circle"></i> &nbsp;Apply NOw!</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>