<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from demo.ravelweb.com/material-cv/home.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Apr 2019 01:09:59 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="author" content="Mohammad Asif">

    <title>Decent Material CV | Resume</title>
    <!-- Fav Icon -->
    <link rel="icon" href="<?php echo base_url(); ?>assets/ncv/images/fav-icon.png">
    <!-- Apple Touch Icon -->
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/ncv/images/apple-touch-icon.png">


    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>


    <!-- Google Material Icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Font Awesome Icon -->
    <link href="<?php echo base_url(); ?>assets/ncv/stylesheets/font-awesome.min.css" rel="stylesheet">
    <!-- IonIcons Icon -->
    <link href="<?php echo base_url(); ?>assets/ncv/stylesheets/ionicons.min.css" rel="stylesheet">


    <!-- Animation -->
    <!-- Animation On Scroll -->


    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/ncv/stylesheets/bootstrap.css" rel="stylesheet">
    <!-- Materialize -->
    <link href="<?php echo base_url(); ?>assets/ncv/stylesheets/materialize.css" rel="stylesheet">
    <!-- Swiper Slider -->
    <link href="<?php echo base_url(); ?>assets/ncv/stylesheets/swiper.css" rel="stylesheet">
	<link id="color-switcher" href="<?php echo base_url(); ?>assets/ncv/stylesheets/style-grey.css" rel="stylesheet">


    <style>


body{
    background: -webkit-linear-gradient(left, #3931af, #00c6ff);
}

</style>

</head>

<body>

<!--==========================================
                 HEADER
===========================================-->


<!--==========================================
                   ABOUT
===========================================-->
<section id="about" class="section">
    <div class="container">
        <div class="row">
            <div class="col col-md-12">

                <!-- Profile Picture [Square] -->


          
                <!-- Social Links -->
                <div id="intro-div" class="card content-wrapper">
                
                    <table style="width:100%;">
                        <tr>
                            <td align="center">
                            <div id="profile" align="center" class="center-block">
                                <img src="<?php echo base_url(); ?>assets/ncv/images/profile-pic.png">
                            </div>
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                    <thead>
                        <tr>
                        <td></td>
                        <th scope="col">Name :</th>
                        <td scope="col">Hassan Jahangir</td>
                        <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                        <th>Otto :</th>
                        <td>@mdo</td>
                        <td></td>
                        </tr>
                        <tr>
                        <td></td>
                        <th>Thornton :</th>
                        <td>@fat</td>
                        <td></td>
                        </tr>
                        <tr>
                        <td></td>
                        <th>Larry the Bird :</th>
                        <td>@twitter</td>
                        <td></td>
                        </tr>
                    </tbody>
                    </table>

                    <!-- Some Intro About You -->
                    <p class="text-center">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi venenatis et tortor ac tincidunt.
                        In euismod iaculis lobortis. Vestibulum posuere molestie ipsum vel sollicitudin. Vestibulum
                        venenatis pharetra mi, ut vestibulum elit ultricies a. Vestibulum at mollis ex, ac consectetur
                        massa. Donec nunc dui, laoreet a nibh et, semper tincidunt nunc. Donec ac posuere tellus.
                        Pellentesque tempus suscipit velit sit amet bibendum.
                    </p>

                    <div class="row">
                        <!-- Download & Contact Button -->
                        <div class="col col-xs-12 col-sm-12 col-md-12 text-center">
                            <!-- Your CV File -->
                            <!-- <a href="<?php echo base_url(); ?>assets/ncv/cv/cv-file.docx" download="cv-file.docx" class="btn waves-effect waves-light btn-primary custom-btn">Download CV</a>
                            <a href="<?php echo base_url(); ?>assets/ncv/#contact" class="btn waves-effect waves-light btn-primary custom-btn">Contact Me</a> -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!--==========================================
                   EXPERIENCE
===========================================-->
<pagebreak>
<section id="experience" class="section">

    <div class="container">
        <div class="row">
            <div class="col col-md-12 col-sm-12 col-xs-12">

                <!-- Timeline -->
                <div class="timeline center-block">
                    <ul>
                        <li class="card" data-aos="fade-up">
                            <i class="title-icon fa fa-circle"></i>
                            <div class="timeline-content">

                                <!-- Heading -->
                                <div class="timeline-header">
                                    <h3 class="text-capitalize" >   <h4 class="text-uppercase text-center">Experience</h4></h3>
                                </div>
                                
                                <!-- Organization and Period -->
                                <p class="sub-heading">Lorem Ipsum Technology - South Africa</p>
                                <p class="sub-heading">January 2012 - December 2015</p>

                                <!-- Job Summary -->
                                <p class="content-p">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Atque, facilis quo. Maiores magnam modi ab libero praesentium
                                    blanditiis consequatur aspernatur accusantium maxime molestiae sunt ipsa.
                                </p>
                            </div>
                        </li>


                    </ul>
                </div>
                <!-- End of Timeline -->
            </div>
        </div>
    </div>
</section>

<!--==========================================
                   EDUCATION
===========================================-->
<section id="education" class="section">
    <h4 class="text-uppercase text-center">Education</h4>
    <div class="container">
        <div class="row">
            <div class="col col-md-12 col-sm-12 col-xs-12">

                <!-- Timeline -->
                <div class="timeline center-block">
                    <ul>
                        <li class="card" data-aos="fade-up">
                            <i class="title-icon fa fa-circle"></i>
                            <div class="timeline-content">

                                <!-- Heading -->
                                <div class="timeline-header">
                                    <h3 class="text-capitalize">Master of Computer Science & engineering</h3>
                                </div>

                                <!-- Institution, Period & result -->
                                <p class="sub-heading">Harvard University - United States</p>
                                <p class="sub-heading">January 2012 - December 2015</p>
                                <p class="sub-heading">Result: 4.0 out of 4.0</p>

                                <!-- Education Summary -->
                                <p class="content-p">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Atque, facilis quo. Maiores magnam modi ab libero praesentium
                                    blanditiis consequatur aspernatur accusantium maxime molestiae sunt ipsa.
                                </p>
                            </div>
                        </li>

                        <li class="card" data-aos="fade-up">
                            <i class="title-icon fa fa-circle"></i>
                            <div class="timeline-content">

                                <!-- Heading -->
                                <div class="timeline-header">
                                    <h3 class="text-capitalize">Bachelor Of Computer Science & Engineering</h3>
                                </div>

                                <!-- Institution, Period & result -->
                                <p class="sub-heading">Harvard University - United States</p>
                                <p class="sub-heading">January 2012 - December 2015</p>
                                <p class="sub-heading">Result: 4.0 out of 4.0</p>

                                <!-- Education Summary -->
                                <p class="content-p">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Atque, facilis quo. Maiores magnam modi ab libero praesentium
                                    blanditiis consequatur aspernatur accusantium maxime molestiae sunt ipsa.
                                </p>
                            </div>
                        </li>

                        <li class="card" data-aos="fade-up">
                            <i class="title-icon fa fa-circle"></i>
                            <div class="timeline-content">

                                <!-- Heading -->
                                <div class="timeline-header">
                                    <h3 class="text-capitalize">Master of Computer Science & engineering</h3>
                                </div>

                                <!-- Institution, Period & result -->
                                <p class="sub-heading">Harvard University - United States</p>
                                <p class="sub-heading">January 2012 - December 2015</p>
                                <p class="sub-heading">Result: 4.0 out of 4.0</p>

                                <!-- Education Summary -->
                                <p class="content-p">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Atque, facilis quo. Maiores magnam modi ab libero praesentium
                                    blanditiis consequatur aspernatur accusantium maxime molestiae sunt ipsa.
                                </p>
                            </div>
                        </li>

                        <li class="card" data-aos="fade-up">
                            <i class="title-icon fa fa-circle"></i>
                            <div class="timeline-content">

                                <!-- Heading -->
                                <div class="timeline-header">
                                    <h3 class="text-capitalize">Bachelor Of Computer Science & Engineering</h3>
                                </div>

                                <!-- Institution, Period & result -->
                                <p class="sub-heading">Harvard University - United States</p>
                                <p class="sub-heading">January 2012 - December 2015</p>
                                <p class="sub-heading">Result: 4.0 out of 4.0</p>

                                <!-- Education Summary -->
                                <p class="content-p">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Atque, facilis quo. Maiores magnam modi ab libero praesentium
                                    blanditiis consequatur aspernatur accusantium maxime molestiae sunt ipsa.
                                </p>

                
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- End of Timeline -->
            </div>
        </div>
    </div>
</section>

<!--==========================================
                   PORTFOLIOS
===========================================-->
<!--==========================================
                   EDUCATION
===========================================-->
<section id="education" class="section">
    <h4 class="text-uppercase text-center">Portfolios</h4>
    <div class="container">
        <div class="row">
            <div class="col col-md-12 col-sm-12 col-xs-12">

                <!-- Timeline -->
                <div class="timeline center-block">
                    <ul>

                        <li class="card">
                            <div class="card" style="">
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                        <img class="media-object" src="..." alt="...">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Media heading</h4>
                                        ...
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- End of Timeline -->
            </div>
        </div>
    </div>
</section>

<!--==========================================
                   INTEREST
===========================================-->
<section id="interest" class="section">
    <h4 class="text-uppercase text-center">Interest</h4>
    <div class="container">
        <div class="row">
            <div class="col col-md-12 col-sm-12 col-xs-12">
                <div class="card interest">
                    <div class="row">
                        <div class="col col-md-12 col-sm-12 col-xs-12">

                            <!-- Some brief about your interest -->
                            <p class="text-center">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi venenatis et tortor ac tincidunt.
                                In euismod iaculis lobortis. Vestibulum posuere molestie ipsum vel sollicitudin.
                                Vestibulum venenatis pharetrami. Lorem ipsum dolor sit amet.
                            </p>
                        </div>

                        <!-- Interest Topic Icon and Name -->

                        <!-- ./Interest Topic Icon and Name -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






<!--==========================================
                    FOOTER
===========================================-->
<footer>
    <div class="container">
        <p class="text-center">
            © DECENT Material CV | Resume 2016. Designed by
            <a href="https://themeforest.net/user/ravelweb" target="_blank">
                <strong>Mohammad Asif | Ravelweb</strong>
            </a>
        </p>
    </div>
</footer>




</body>

</html>