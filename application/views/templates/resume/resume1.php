<!DOCTYPE html>
<html>
<head>
    <title>Profile Page</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/cv/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/cv/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/cv/fontawesome/css/all.css">
    <style>
.row {
margin: 10px 0px 0px 0px !important;
padding: 0px !important;
}

.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
	border:0;
	padding:0;
	margin-left:-0.00001;
}
    

    </style>
</head>
<body>
    <div class="main-wrapper">
        <div class="container">
            <div class="row header">
                <div class="col-lg-4 col-md-4 img">
                    <img src="<?php if($user["picture"]){ echo base_url(); ?>assets/uploads/<?php echo $user["picture"]; } else{ echo base_url()."assets/uploads/avatar.jpeg";}?>" width="150px">
                </div>
                <div class="col-lg-8 info col-md-8">
                    <h1 style="color:white; font-size: 25px;"><?php echo $user['name']; ?></h1>
                    <h3 style="font-size: 20px; padding-bottom:10px; padding-top:10px;">POSITION</h3>
                    <span></span>
                    <p style="padding-left:40px; padding-right:40px; font-size: 15px; padding-bottom:10px;"><?php echo $user['bio']; ?></p>
                    <table align="center" style="font-size: 25px; color:white;">
                        <tr>
                            <td style="padding-bottom:15px;">
                                <div class="col-sm-12 col-md-6 col-lg-3 my-5">
                                <div class="card border-0">
                                    <div class="card-body text-center">
                                        <i class="fa fa-map-marker fa-5x mb-3" aria-hidden="true"></i>
                                        <h6 class="text-uppercase mb-5" >Father Name :</h6>
                                        <address style="font-size: 15px;"><?php echo $user['fathername']; ?></address>
                                    </div>
                                    </div>
                                </div>
                            </td>
                            <td style="padding-left:40px;">
                            </td>
                            <td style="padding-bottom:15px;">
                                <div class="col-sm-12 col-md-6 col-lg-3 my-5">
                                <div class="card border-0">
                                    <div class="card-body text-center">
                                        <i class="fa fa-globe fa-5x mb-3" aria-hidden="true"></i>
                                        <h6 class="text-uppercase mb-5">Country & City :</h6>
                                        <p style="font-size: 15px;"><?php echo $user['country']; ?> - <?php echo $user['city']; ?></p>
                                    </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom:15px;">
                                <div class="col-sm-12 col-md-6 col-lg-3 my-5">
                                <div class="card border-0">
                                    <div class="card-body text-center">
                                        <i class="fa fa-phone fa-5x mb-3" aria-hidden="true"></i>
                                        <h6 class="text-uppercase mb-5">Number :</h6>
                                        <p style="font-size: 15px;"><?php echo $user['phone']; ?> </p>
                                    </div>
                                    </div>
                                </div>
                            </td>
                            <td style="padding-left:40px;">
                            </td>
                            <td>
                                <div class="col-sm-12 col-md-6 col-lg-3 my-5">
                                <div class="card border-0">
                                    <div class="card-body text-center">
                                        <i class="fa fa-map-marker fa-5x mb-3" aria-hidden="true"></i>
                                        <h6 class="text-uppercase mb-5">Email :</h6>
                                    <address style="font-size: 15px;"><?php echo $user['email']; ?></address>
                                    </div>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>             
                                <div class="col-sm-12 col-md-6 col-lg-3 my-5">
                                <div class="card border-0">
                                    <div class="card-body text-center">
                                        <i class="fa fa-map-marker fa-5x mb-3" aria-hidden="true"></i>
                                        <h6 class="text-uppercase mb-5">Address :</h6>
                                    <address style="font-size: 15px;"><?php echo $user['address']; ?></address>
                                    </div>
                                    </div>
                                </div>
            
                            </td>
                            <td>
                  
                            </td>
                            <td>
                            </td>
                        </tr>
    
    
                    </table>
                </div>
            </div>


            <div class="row profile-description">
                <div class="col-lg-6 col-md-6">
                    <h1><span style="font-size: 20px;">Education</span></h1>
                    <br>
                    <?php foreach($educations as $education): ?>
                    <h3 style="font-size: 15px;">
                    
                    <?php $datecreate=date_create($education['startdate']); echo date_format($datecreate,"d/F/Y"); ?> - <?php $datecreate=date_create($education['enddate']); echo date_format($datecreate,"d/F/Y"); ?></h3>

                    <h4 style="font-size: 12px;"><?php echo $education['degree']; ?></h4>
                    <p style="font-size: 10px;"><?php echo $education['desc']; ?></p>
                
                    <?php endforeach; ?>


                </div>
            
                <div class="col-lg-6 col-md-6">
              
                    <h1><span style="font-size: 20px;">Experiences</span></h1>
                    <br>
                    <?php foreach($experiences as $experience): ?>
                    <h3 style="font-size: 15px;"><?php echo $experience['startdate']; ?> - <?php echo $education['enddate']; ?></h3>
                    <h4 style="font-size: 12px;"><?php echo $experience['company']; ?> </h4>
                    <p style="font-size: 10px;"><?php echo $experience['desc']; ?></p>
              

                    <?php endforeach; ?>

                    <br>
                    <h1><span style="font-size: 20px;">Projects</span></h1>    
                </div>
            </div>


            <div class="jp_blog_comment_main_section_wrapper" style="padding-left: 0px;padding-right: 0px;">
                <div class="bs-example" data-example-id="thumbnails-with-custom-content">
                    <div class="row">

                        <?php foreach($projects as $project): ?>                                                       
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail"> <img alt="100%x200" data-src="holder.js/100%x200" style="height: 200px; width: 100%; display: block;" src="<?php echo base_url(); ?>assets/uploads/<?php echo $project['img']; ?>" data-holder-rendered="true">
                                <div class="caption">
                                    <h3 style="font-size: 15px;"><?php echo $project['tittle']; ?>  </h3>
                                    <p style="font-size: 10px;"><?php echo $project['desc']; ?></p>
                                </div>
                            </div>
                        </div>    
                        <?php endforeach; ?>
      
    
                    </div>
                </div>
            </div>
          
        </div>
    </div>
	<br>
	<br>
</body>
</html>