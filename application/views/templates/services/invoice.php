<style>
    .color-invoice{
    background-color: #ffffff;
        border: 1px solid #d7d7d7;
        padding-top:100px;
        padding-bottom:100px;
    }
</style>
<div class="jp_tittle_main_wrapper">
    <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xs-7 col-sm-6 col-lg-8">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Invoices</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="#">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Users</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Invoice</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br>  <br>
    </div>
    <div class="container">
        <div class="row color-invoice">
        <div class="col-md-12">
            #Sr. No: 78660
            <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-7">
                <h1>INVOICE</h1>
                <br />
                <strong>Email : </strong> info@HtmlSnipp.com
                <br />
                <strong>Call : </strong> +1-6655-44-453
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5">

                <h2>   Html Snipp LLC</h2> 789/89 , Lane Set , New York,
                <br> Pin- 90-89-78-00,
                <br> United States.

            </div>
            </div>

            <hr />
            <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <strong>ITEM DESCRIPTION & DETAILS :</strong>
            </div>
            </div>
            <hr />
            <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>S. No.</th>
                        <th>Perticulars</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Website Design</td>
                  
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Website Development</td>
         
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Customization</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Plugin Setup</td>
            
                    </tr>
                    </tbody>
                </table>
                </div>
                <hr>
                <div>
                <h4>  Total : 22000 USD </h4>
                </div>
                <hr>
                <div>
                <h4>  Taxes : 4400 USD ( 20 % on Total Bill ) </h4>
                </div>
                <hr>
                <div>
                <h3>  Bill Amount : 26400 USD </h3>
                </div>
                <hr />
            </div>
            </div>
            <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <strong> Important: </strong>
                <ol>
                <li>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.

                </li>
                <li>
                    Nulla eros eros, laoreet non pretium sit amet, efficitur eu magna.
                </li>
                <li>
                    Curabitur efficitur vitae massa quis molestie. Ut quis porttitor justo, sed euismod tortor.
                </li>
                </ol>
            </div>
            </div>
            <hr />
            <div class="row">
            <div class="container">
                    <div class="row">
                        <!-- You can make it whatever width you want. I'm making it full width
                            on <= small devices and 4/12 page width on >= medium devices -->
                        <div class="col-xs-12 col-md-4">
                        
                        
                            <!-- CREDIT CARD FORM STARTS HERE -->
                            <div class="panel panel-default credit-card-box">
                                <div class="panel-heading display-table" >
                                    <div class="row" >
                                        <div class="display-td" >                            
                                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                                        </div>
                                    </div>                    
                                </div>
                                <div class="panel-body">
                                    <form role="form" id="payment-form" method="POST" action="javascript:void(0);">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="cardNumber">CARD NUMBER</label>
                                                    <div class="input-group">
                                                        <input 
                                                            type="tel"
                                                            class="form-control"
                                                            name="cardNumber"
                                                            placeholder="Valid Card Number"
                                                            autocomplete="cc-number"
                                                            required autofocus 
                                                        />
                                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                    </div>
                                                </div>                            
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-7 col-md-7">
                                                <div class="form-group">
                                                    <label for="cardExpiry"><span class="hidden-xs">EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
                                                    <input 
                                                        type="tel" 
                                                        class="form-control" 
                                                        name="cardExpiry"
                                                        placeholder="MM / YY"
                                                        autocomplete="cc-exp"
                                                        required 
                                                    />
                                                </div>
                                            </div>
                                            <div class="col-xs-5 col-md-5 pull-right">
                                                <div class="form-group">
                                                    <label for="cardCVC">CV CODE</label>
                                                    <input 
                                                        type="tel" 
                                                        class="form-control"
                                                        name="cardCVC"
                                                        placeholder="CVC"
                                                        autocomplete="cc-csc"
                                                        required
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="couponCode">COUPON CODE</label>
                                                    <input type="text" class="form-control" name="couponCode" />
                                                </div>
                                            </div>                        
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <button class="subscribe btn btn-success btn-lg btn-block" type="button">Pay Now</button>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;">
                                            <div class="col-xs-12">
                                                <p class="payment-errors"></p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>            
                            <!-- CREDIT CARD FORM ENDS HERE -->
                            
                            
                        </div>            
                        
                        <div class="col-xs-12 col-md-8" style="font-size: 12pt; line-height: 2em;">
                            <p><h1>Features:</h1>
                                <ul>
                                    <li>As-you-type, input formatting</li>
                                    <li>Form field validation (also as you type)</li>
                                    <li>Graceful error feedback for declined card, etc</li>
                                    <li>AJAX form submission w/ visual feedback</li>
                                    <li>Creates a Stripe credit card token</li>
                                </ul>
                            </p>
                            <p>Be sure to replace the dummy API key with a valid Stripe API key.</p>
                        
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <hr>
           
        </div>
        </div>
    </div>
    <div class="row">
    <br>
    </div>
</div>