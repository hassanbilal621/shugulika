




<div class="jp_tittle_main_wrapper">
    <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Services</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="#">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Pages</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Services</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="jp_best_deal_main_wrapper">
        <div class="container">
            <div class="row">

              
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-notification"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Job Notifications</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-wallet"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Essay Pay Money</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-people"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Happy Support</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-notification"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Job Notifications</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-wallet"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Essay Pay Money</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="jp_best_deal_main_cont_wrapper jp_best_deal_main_cont_wrapper2">
                        <div class="jp_best_deal_icon_sec">
                            <i class="flaticon-people"></i>
                        </div>
                        <div class="jp_best_deal_cont_sec">
                            <h4><a href="#">Happy Support</a></h4>
                            <p>Proin gravida nibh Aenean sollicitudin...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="jp_pricing_info_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_pricing_cont_heading">
                        <h2>Additional Information :</h2>
                    </div>
                    <div class="jp_pricing_cont_wrapper">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus auctor lacinia pellentesque. Vivamus et tellus in urna faucibus porttitor. Sed auctor ut nunc in pulvinar. Fusce lacinia, velit vitae pharetra porttitor, nunc libero
                            itdin odio, quis iaculis tortor ligula feugiat ex. Nam ut cursus mi. Nullam eu erat in justo euismod ultrices ut id enim. Mrbi non tempor ante, eget molestie mauris. Cras gravida, lacus nec sollicitn euismod, ipsum nisl tempor
                            leo, in volutat sapien ex ac erat. In sit amet dolor ut erat fermentum tincidut. Maecenas sd pque ex.<br><br>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem
                            quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>